package cn.org.awcp.unit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.unit.service.PunPositionService;
import cn.org.awcp.unit.vo.PunPositionVO;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;

/**
 * 职位管理
 */
@Controller
@RequestMapping("/punPositionController")
public class PunPositionController {
	/**
	 * 日志对象
	 */
	private static final Log logger = LogFactory.getLog(PunPositionController.class);

	@Resource(name = "punPositionServiceImpl")
	private PunPositionService punPositionService;

	/**
	 * 职位列表页面
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "/pageList", method = RequestMethod.GET)
	public ModelAndView selectPagedByExample(
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/unit/punGroup-Positions");
		PageList<PunPositionVO> list = punPositionService.selectPagedByExample("queryList", null, currentPage, pageSize,null);
		mv.addObject("vos", list);
		return mv;
	}
	
	/**
	 * 保存职位
	 * @param punPosition
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ReturnResult save(PunPositionVO punPosition) {
		ReturnResult result = ReturnResult.get();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", punPosition.getName());
		List<PunPositionVO> vos = punPositionService.queryResult("queryList", params);
		if (null != vos && vos.size() > 0) {
			result.setStatus(StatusCode.FAIL).setMessage("岗位" + punPosition.getName() + "已存在");
		} else {
			punPosition.setShortName(punPosition.getName());
			punPositionService.save(punPosition);
			result.setStatus(StatusCode.SUCCESS);
		}
		return result;
	}
	
	/**
	 * 修改职位
	 * @param punPosition
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ReturnResult update(PunPositionVO punPosition) {
		ReturnResult result = ReturnResult.get();
		logger.debug(punPosition.getGrade() + "");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", punPosition.getName());
		List<PunPositionVO> vos = punPositionService.queryResult("queryList", params);
		long count = vos.stream().filter((item) -> {
			return !item.getPositionId().equals(punPosition.getPositionId());
		}).count();
		if(count > 0) {
			result.setStatus(StatusCode.FAIL).setMessage("岗位" + punPosition.getName() + "已存在");
		} else {
			punPosition.setShortName(punPosition.getName());
			punPositionService.update(punPosition, "update");
			result.setStatus(StatusCode.SUCCESS);
		}
		return result;
	}
	
	/**
	 * 删除职位
	 * @param positionId
	 * @return
	 */
	@RequestMapping(value = "/remove")
	public ModelAndView remove(long[] positionId) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/punPositionController/pageList.do");
		try {
			if (null != positionId && positionId.length > 0) {
				for (Long id : positionId) {
					PunPositionVO p = new PunPositionVO();
					p.setPositionId(id);
					punPositionService.remove(p);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	/**
	 * 获取所有职位
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/pageListByAjax", method = RequestMethod.GET)
	public List<PunPositionVO> selectPagedByExampleByAjax() {
		ModelAndView mv = new ModelAndView();
		Map<String,Object> queryParam = new HashMap<String,Object>();
		PageList<PunPositionVO> list = punPositionService.selectPagedByExample("queryList", queryParam, 1, 9999, null);
		mv.addObject("punPositiontList", list);
		return list;
	}

}