package cn.org.awcp.common.util;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 常用工具方法
 * @author yqtao
 *
 */
public class CommonUtil {

	/**
	 * 将字符串包裹单引号
	 * @param str
	 * @param separator
	 * @return
	 */
	public static String wrapQuot(String str, String separator) {
		if(StringUtils.isBlank(str)) {
			return "";
		} else {
			StringBuffer sb = new StringBuffer();
			Arrays.stream(str.split(separator)).forEach((s) -> {
				sb.append("'").append(s).append("',");
			});
			if(sb.length() > 1) {
				sb.deleteCharAt(sb.length() - 1);
			}
			return sb.toString();
		}
	}
	
	/**
	 * 包裹单引号
	 * @param list
	 * @return
	 */
	public static String wrapQuot(List<String> list) {
		if(list == null || list.isEmpty()) {
			return "";
		} else {
			StringBuffer sb = new StringBuffer();
			list.stream().forEach((s) -> {
				sb.append("'").append(s).append("',");
			});
			if(sb.length() > 1) {
				sb.deleteCharAt(sb.length() - 1);
			}
			return sb.toString();
		}
	}
}
