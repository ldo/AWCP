package cn.org.awcp.metadesigner.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.metadesigner.application.MetaModelItemService;
import cn.org.awcp.metadesigner.application.MetaModelService;
import cn.org.awcp.metadesigner.vo.MetaModelItemsVO;
import cn.org.awcp.metadesigner.vo.MetaModelVO;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.exception.PlatformException;

/**
 * 元数据字段管理
 *
 */
@Controller("metaModelItemsController")
@RequestMapping(value = "/metaModelItems")
public class MetaModelItemsController {
	/**
	 * 日志对象
	 */
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private MetaModelService metaModelServiceImpl;

	@Autowired
	private MetaModelItemService metaModelItemsServiceImpl;

	/**
	 * 根据元数据查询其字段
	 * @param modelId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/queryResultByParams")
	public String queryResult(@RequestParam(value = "id") String modelId, Model model) {
		MetaModelVO vo = metaModelServiceImpl.get(modelId);
		model.addAttribute("vo", vo);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("modelId", modelId);
		PageList<MetaModelItemsVO> pl = metaModelItemsServiceImpl.queryResult("queryList", map, 1, 999, "usePrimaryKey.desc");
		model.addAttribute("list", pl);
		model.addAttribute("modelId", modelId);
		return "metadesigner/metaModelItem/metaModelItem_list";
	}
	
	/**
	 * 条件加分页查询 用于搜索
	 * @param model
	 * @param vo
	 * @return
	 */
	@RequestMapping(value = "queryPageResult")
	public String queryPageResult(Model model, MetaModelItemsVO vo) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (vo != null) {
			map.put("itemName", vo.getItemName());
			map.put("itemCode", vo.getItemCode());
			map.put("itemType", vo.getItemType());
			map.put("defaultValue", vo.getDefaultValue());
			map.put("modelId", vo.getModelId());
		}
		String queryStr = "queryList";
		PageList<MetaModelItemsVO> pl = metaModelItemsServiceImpl.queryResult(queryStr, map, 1, 999, "usePrimaryKey.desc");
		model.addAttribute("list", pl);
		model.addAttribute("modelId", vo.getModelId());
		return "metadesigner/metaModelItem/metaModelItem_list";
	}
	
	/**
	 * 页面跳转 点击增加
	 * 
	 * @param model
	 * @param modelId
	 * @return
	 */
	@RequestMapping(value = "/add_toggle")
	public String addToggle(Model model, String modelId) {
		model.addAttribute("modelId", modelId);
		model.addAttribute("type", "no");
		List<MetaModelItemsVO> ls = metaModelItemsServiceImpl.queryResult("queryResult", modelId);
		ls.stream().forEach((mmi) -> {
			if (mmi.getUsePrimaryKey() != null && mmi.getUsePrimaryKey() == 1) {
				model.addAttribute("type", "yes");
			}
		});
		return "metadesigner/metaModelItem/metaModelItem_add";
	}
	
	/**
	 * 保存数据源字段
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/save")
	public ReturnResult save(@ModelAttribute MetaModelItemsVO vo) {
		ReturnResult ret = ReturnResult.get();
		// 1.判断数据库是否已经存在这一属性
		List<MetaModelItemsVO> ls = metaModelItemsServiceImpl.queryColumn("queryList", vo.getModelId(),vo.getItemCode());
		if (ls.isEmpty()) {
			vo.setItemValid(0);
			vo.setItemName(vo.getItemCode());
			metaModelItemsServiceImpl.save(vo);
		} else {
			throw new PlatformException("该元数据已经有该字段");
		}
		return ret;
	}

	/**
	 * 页面跳转 修改页面
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/update_toggle")
	public String updateToggle(Model model, String id) {
		MetaModelItemsVO vo = metaModelItemsServiceImpl.get(id);
		model.addAttribute("vo", vo);
		model.addAttribute("type", "no");
		List<MetaModelItemsVO> ls = metaModelItemsServiceImpl.queryResult("queryResult", vo.getModelId());
		ls.stream().forEach((mmi) -> {
			if (mmi.getUsePrimaryKey() != null && mmi.getUsePrimaryKey() == 1) {
				model.addAttribute("type", "yes");
			}
		});
		return "metadesigner/metaModelItem/metaModelItem_edit";
	}
	
	/**
	 * 修改
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update")
	public ReturnResult update(@ModelAttribute MetaModelItemsVO vo) {
		ReturnResult ret = ReturnResult.get();
		List<MetaModelItemsVO> ls = metaModelItemsServiceImpl.queryColumn("queryList", vo.getModelId(), vo.getItemCode());
		long count = ls.stream().filter((item) -> {
			return !item.getId().equals(vo.getId());
		}).count();
		if (count == 0) {
			vo.setItemValid(0);
			vo.setItemName(vo.getItemCode());
			metaModelItemsServiceImpl.update("update", vo);		
		} else {
			throw new PlatformException("该元数据已经有该字段");
		}
		return ret;
	}
	
	/**
	 * 删除元数据字段
	 * @param vo
	 * @return
	 */
	@RequestMapping(value = "/remove")
	public String remove(String[] id,String modelId) {
		Arrays.stream(id).forEach((itemId) -> {
			metaModelItemsServiceImpl.remove(metaModelItemsServiceImpl.get(itemId));
		});
		return "redirect:queryResultByParams.do?id=" + modelId;
	}

}
