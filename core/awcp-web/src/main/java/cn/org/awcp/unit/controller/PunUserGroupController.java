package cn.org.awcp.unit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.Criteria;
import cn.org.awcp.unit.service.PunUserBaseInfoService;
import cn.org.awcp.unit.service.PunUserGroupService;
import cn.org.awcp.unit.vo.PunUserBaseInfoVO;
import cn.org.awcp.unit.vo.PunUserGroupVO;

/**
 * 用户部门Controller
 */
@Controller
@RequestMapping("/punUserGroupController")
public class PunUserGroupController {
	/**
	 * 日志对象
	 */
	protected final Log logger = LogFactory.getLog(getClass());

	@Resource(name = "punUserGroupServiceImpl")
	private PunUserGroupService punUserGroupService; // 用户部门Service

	@Autowired
	@Qualifier("punUserBaseInfoServiceImpl")
	PunUserBaseInfoService userService; // 用户Service

	@Resource(name = "jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * 根据部门获取用户
	 * @param groupId
	 * @param name
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "/getUsers", method = RequestMethod.GET)
	public ModelAndView getUsers(Long groupId,String name,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/unit/punGroup-usersView");
		if (groupId == null) {
			String sql = "select group_id from p_un_group where PARENT_GROUP_ID=0";
			groupId = jdbcTemplate.queryForObject(sql, Long.class);
		}			
		BaseExample example = new BaseExample();
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("p_un_group.GROUP_ID", groupId);
		if(StringUtils.isNotBlank(name)) {
			criteria.andLike("p_un_user_base_info.NAME", "%" + name + "%");
		}
		PageList<PunUserBaseInfoVO> list = userService.selectByExample_UserList(example, currentPage, pageSize, null);
		mv.addObject("groupId",groupId);
		mv.addObject("name",name);
		mv.addObject("userList", list);
		return mv;
	}

	/**
	 * 根据部门ID获取该部门的所有用户
	 * 
	 * @param groupId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserListByAjax", method = RequestMethod.GET)
	public PageList<PunUserBaseInfoVO> getUserListByAjax(Long groupId) {
		PageList<PunUserBaseInfoVO> list = punUserGroupService.queryUserListByGroupId(groupId, 0, 9999, null);
		return list;
	}

	/**
	 * 根据条件查询用户
	 * 
	 * @param groupId
	 *            部门ID
	 * @param role
	 *            角色ID
	 * @param job
	 *            职位ID
	 * @param name
	 *            姓名
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserListByWhere", method = RequestMethod.POST)
	public List<PunUserBaseInfoVO> getUserListByWhere(Long groupId, String role, String job, String name,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "9999") int pageSize) {
		String sortString = "USER_ID.desc";
		Map<String, Object> params = new HashMap<String, Object>();
		String sqlMap = "";
		if (role != null) {
			params.put("roleId", role);
			sqlMap = "queryUserRelateRole";
		}
		if (job != null) {
			params.put("position_id", job);
			sqlMap = "queryUserRelatePosition";
		}
		if (name != null) {
			params.put("name", name);
			List<PunUserBaseInfoVO> userVOs = userService.queryResult("eqQueryList", params);
			return userVOs;
		}
		List<PunUserBaseInfoVO> userVOs = userService.queryPagedResult(sqlMap, params, currentPage, pageSize,sortString);
		return userVOs;
	}

	/**
	 * 移动部门
	 * @param groupId
	 * @param userIds
	 * @param oldGroupId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/userGroupEdit", method = RequestMethod.POST)
	public Map<String, Object> userGroupEdit(Long groupId, String[] userIds, Long oldGroupId) {
		for (String s : userIds) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("groupId", oldGroupId);
			params.put("userId", s);
			List<PunUserGroupVO> userGroupVOs = punUserGroupService.queryResult("eqQueryList", params);
			if (null != userGroupVOs && userGroupVOs.size() > 0) {
				for (PunUserGroupVO pvo : userGroupVOs) {
					pvo.setGroupId(groupId);
					punUserGroupService.save(pvo);
				}
			}
		}
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("result", "success");
		return res;
	}

}