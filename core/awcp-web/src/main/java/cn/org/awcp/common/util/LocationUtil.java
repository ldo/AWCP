package cn.org.awcp.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.wechat.util.RequestUtil;

/**
 * 地理位置工具类
 * @author yqtao
 *
 */
public class LocationUtil {

	//地球半径(米)
	private final static double EARTH_RADIUS = 6371393; 
	
	//百度地图api秘钥
	private final static String KEY = "GiX75jzdX7yDyWiaK5oB7eWQZz6YVvEt";
	
	/**
	 * 通过地址获取百度地图的经纬度
	 * @param address	地点
	 * @param city		地点所在的城市
	 * @return
	 */
	public static String getLocation(String address, String city){
		String url = "http://api.map.baidu.com/geocoder/v2/?output=json&address=" + address + "&city=" + city + "&ak=" + KEY;
		try {
			return RequestUtil.doGet(url);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PlatformException("通过地址获取百度地图的经纬度失败");
		}
	}
	
	/**
	 * 将WGS84坐标转换为百度地图经纬度坐标
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	public static Object getBaiduLocation(String longitude,String latitude){
    	String url = "http://api.map.baidu.com/geoconv/v1/?coords=" + longitude + "," + latitude + "&from=1&to=5&ak=" + KEY;
    	try {
			String json = RequestUtil.doGet(url);
			JSONObject jsonObj = JSON.parseObject(json);
			return jsonObj.getJSONArray("result").get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PlatformException("转换坐标失败");
		}
    }	

	/**
	 * 获取百度地图2个点之间的距离
	 * @param lat_a		A点纬度
	 * @param lng_a		A点经度
	 * @param lat_b		B点纬度
	 * @param lng_b		B点经度
	 * @return
	 */
    public static String getDistance(double lat_a, double lng_a, double lat_b, double lng_b){
        double pk = 180 / 3.14169;
        double a1 = lat_a / pk;
        double a2 = lng_a / pk;
        double b1 = lat_b / pk;
        double b2 = lng_b / pk;
        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);
        return String.format("%.2f", EARTH_RADIUS * tt);
    }

}
