package cn.org.awcp.formdesigner.core.constants;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public class FormDesignGlobal {
	public static final String PAGE_ACT_CODE_PREFIX = "";
	public static final String DOCUMENT_VIEW = "document/view.do?id=&dynamicPageId=";

	public static final Map<String, String> COMPOENT_TYPE_NAME = new HashMap<String, String>();
	public static final Map<String, String> COMPOENT_TYPE_URL = new HashMap<String, String>();
	public static final Map<String, String> LAYOUT_TYPE_SHOW = new HashMap<String, String>();
	
	static {
		COMPOENT_TYPE_NAME.put("1001", "单行文本框");		// 计算值、隐藏、只读、禁用
		COMPOENT_TYPE_NAME.put("1002", "日期文本框");		// 计算值、隐藏、只读、禁用
		COMPOENT_TYPE_NAME.put("1003", "多选复选框");		// 计算值、隐藏、只读、禁用、选项
		COMPOENT_TYPE_NAME.put("1004", "radio单选按钮");	// 计算值、隐藏、只读、禁用、选项
		COMPOENT_TYPE_NAME.put("1005", "多行输入框");		// 计算值、隐藏、只读、禁用
		COMPOENT_TYPE_NAME.put("1006", "下拉选项框");		// 计算值、隐藏、只读、禁用、选项
		COMPOENT_TYPE_NAME.put("1008", "列框");			// 计算值、隐藏
		COMPOENT_TYPE_NAME.put("1009", "标签");			// 计算值、隐藏
		COMPOENT_TYPE_NAME.put("1010", "隐藏框");			// 计算值、禁用
		COMPOENT_TYPE_NAME.put("1011", "文件上传框");		// 计算值、隐藏、只读、禁用
		COMPOENT_TYPE_NAME.put("1012", "包含组件框");		// 计算值、隐藏、只读、禁用
		COMPOENT_TYPE_NAME.put("1016", "图片上传框");		// 计算值、隐藏、只读、禁用
		COMPOENT_TYPE_NAME.put("1017", "表格组件");		// 计算值、隐藏、禁用
		COMPOENT_TYPE_NAME.put("1019", "富文本框");		// 计算值、隐藏、禁用
		COMPOENT_TYPE_NAME.put("1020", "用户选择框");		// 计算值、隐藏、禁用
		COMPOENT_TYPE_NAME.put("1033", "级联下拉框");		// 计算值、隐藏、禁用
		COMPOENT_TYPE_NAME.put("1036", "搜索条件");		// 计算值、隐藏、禁用
		COMPOENT_TYPE_NAME.put("1043", "流程意见组件");		// 计算值、隐藏、禁用

		COMPOENT_TYPE_URL.put("1001", "formdesigner/page/component/input/input.jsp");
		COMPOENT_TYPE_URL.put("1002", "formdesigner/page/component/datetime/datetime.jsp");
		COMPOENT_TYPE_URL.put("1003", "formdesigner/page/component/checkbox/checkbox.jsp");
		COMPOENT_TYPE_URL.put("1004", "formdesigner/page/component/radio/radio.jsp");
		COMPOENT_TYPE_URL.put("1005", "formdesigner/page/component/textarea/textarea.jsp");
		COMPOENT_TYPE_URL.put("1006", "formdesigner/page/component/select/select.jsp");
		COMPOENT_TYPE_URL.put("1008", "formdesigner/page/component/column/column.jsp");
		COMPOENT_TYPE_URL.put("1009", "formdesigner/page/component/label/label.jsp");
		COMPOENT_TYPE_URL.put("1010", "formdesigner/page/component/hidden/hidden.jsp");
		COMPOENT_TYPE_URL.put("1011", "formdesigner/page/component/file/file.jsp");
		COMPOENT_TYPE_URL.put("1012", "formdesigner/page/component/containPage/containPage.jsp");
		COMPOENT_TYPE_URL.put("1016", "formdesigner/page/component/image/image.jsp");
		COMPOENT_TYPE_URL.put("1017", "formdesigner/page/component/dataGrid/dataGrid.jsp");
		COMPOENT_TYPE_URL.put("1019", "formdesigner/page/component/kindEditor/kindEditor.jsp");
		COMPOENT_TYPE_URL.put("1020", "formdesigner/page/component/userSelect/userSelect.jsp");
		COMPOENT_TYPE_URL.put("1033", "formdesigner/page/component/multilevelLinkage/multilevelLinkage.jsp");
		COMPOENT_TYPE_URL.put("1036", "formdesigner/page/component/searchComponent/addSearch.jsp");
		COMPOENT_TYPE_URL.put("1043","formdesigner/page/component/suggestion/suggestion.jsp");
		
		LAYOUT_TYPE_SHOW.put("1", "列");
		LAYOUT_TYPE_SHOW.put("2", "行");
	}

	public static boolean isValuedNoneContain(JSONObject o) {
		int componentType = Integer.valueOf(o.getString("componentType"));
		if (componentType == 1001 || componentType == 1002 || componentType == 1003 || componentType == 1004 || 
				componentType == 1005 || componentType == 1006 || componentType == 1010 || componentType == 1011 ||
				componentType == 1016 || componentType == 1017 || componentType == 1019 || componentType == 1020) {
			return true;
		}
		return false;
	}
	
}
