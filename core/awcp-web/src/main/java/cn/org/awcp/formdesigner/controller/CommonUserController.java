package cn.org.awcp.formdesigner.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;

import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.extend.formdesigner.DocumentUtils;
import cn.org.awcp.formdesigner.application.service.FormdesignerService;
import cn.org.awcp.formdesigner.application.vo.DynamicPageVO;
import cn.org.awcp.unit.service.PunGroupService;
import cn.org.awcp.unit.service.PunUserBaseInfoService;
import cn.org.awcp.unit.vo.PunGroupVO;
import cn.org.awcp.unit.vo.PunUserBaseInfoVO;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.exception.PlatformException;

/**
 * 常用用户管理，页面绑定管理
 * @author yqtao
 *
 */
@Controller
@RequestMapping("/common/user")
public class CommonUserController {

	@Autowired
	@Qualifier("punGroupServiceImpl")
	PunGroupService groupService;

	@Autowired
	@Qualifier("punUserBaseInfoServiceImpl")
	PunUserBaseInfoService userService;// 用户Service

	@Autowired
	private FormdesignerService formdesignerServiceImpl;

	@Autowired
	private SzcloudJdbcTemplate jdbcTemplate;
	
	/**
	 * 加入常用联系人
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/userCount", method = RequestMethod.POST)
	public String userCount(String user) {
		PunUserBaseInfoVO userInfo = (PunUserBaseInfoVO) SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER);
		String[] userIdArr = user.split(",");
		for (String userId : userIdArr) {
			String sql = "select id,user_id,user,click_number from p_un_common_user where user_id=? and user=?";
			List<Map<String, Object>> mapResult = jdbcTemplate.queryForList(sql, userInfo.getUserId(), userId);
			//存在增加记录否则修改记录
			if (mapResult != null && mapResult.size() > 0) {
				Map<String, Object> map = mapResult.get(0);
				int click_number = Integer.parseInt(String.valueOf(map.get("click_number"))) + 1;
				sql = "update p_un_common_user set click_number=? where id=?";
				jdbcTemplate.update(sql, click_number, map.get("id"));
			} else {
				sql = "insert into p_un_common_user(id,user_id,user,click_number) values(?,?,?,1)";
				jdbcTemplate.update(sql, UUID.randomUUID().toString(), userInfo.getUserId(), userId);
			}
		}
		return "success";
	}
	
	/**
	 * 查询当前用户下常用联系人
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCommonUser")
	public List<PunUserBaseInfoVO> queryByUser() {
		PunUserBaseInfoVO userInfo = (PunUserBaseInfoVO) SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER);
		String sql = "select distinct user from p_un_common_user where user_id=? "
				+ "and user in (select USER_ID from p_un_user_base_info) order by click_number desc limit 15";
		List<Long> list = jdbcTemplate.queryForList(sql, Long.class, userInfo.getUserId());
		return list.stream().map((userId) -> {
			return DocumentUtils.getIntance().getUserById(userId);
		}).filter((vo) -> vo!=null).collect(Collectors.toList());
	}

	/**
	 * 加入常用部门
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/groupCount", method = RequestMethod.POST)
	public String groupCount(String group) {
		PunUserBaseInfoVO userInfo = (PunUserBaseInfoVO) SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER);
		Long userId = userInfo.getUserId();
		String[] groupIdArr = group.split(",");
		for (String groupId : groupIdArr) {
			String sql = "select count(*) from p_un_common_group where user_id=? and group_id=?";
			int count = jdbcTemplate.queryForObject(sql, Integer.class, userId, groupId);
			if(count > 0) {
				sql = "update p_un_common_group set click_number=click_number+1 where user_id=? and group_id=?";
				jdbcTemplate.update(sql, userId, groupId);
			} else {
				sql = "insert into p_un_common_group(id,group_id,user_id,click_number) values(?,?,?,1)";
				jdbcTemplate.update(sql, UUID.randomUUID().toString(), groupId, userId);
			}
		}
		return "success";
	}

	/**
	 * 查询当前用户下常用部门
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryByGroup")
	public List<PunGroupVO> queryByGroup() {
		PunUserBaseInfoVO userInfo = (PunUserBaseInfoVO) SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER);
		String sql = "select distinct group_id from p_un_common_group where user_id=? "
				+ "and group_id in (select GROUP_ID from p_un_group) order by click_number desc limit 15";
		List<Long> list = jdbcTemplate.queryForList(sql, Long.class, userInfo.getUserId());
		return list.stream().map((groupId) -> {
			return groupService.findById(groupId);
		}).filter((vo) -> vo!=null).collect(Collectors.toList());
	}
	
	/**
	 * 查询当前用户下级
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/querySubUserByUser")
	public List<PunUserBaseInfoVO> querySubUserByUser() {
		PunUserBaseInfoVO userInfo = (PunUserBaseInfoVO) SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER);
		String sql = "select USER_ID from p_un_user_group where leader = " + userInfo.getUserId();
		List<Long> list = jdbcTemplate.queryForList(sql, Long.class);
		return list.stream().map((userId) -> {
			return DocumentUtils.getIntance().getUserById(userId);
		}).filter((vo) -> {
			return vo!=null;
		}).collect(Collectors.toList());
	}

	/**
	 * 通过sql查询用户
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/querySqlUser")
	public List<PunUserBaseInfoVO> querySqlUser(String sql, String userName) {
		if (StringUtils.isNotBlank(userName)) {
			sql = "select USER_ID from p_un_user_base_info where USER_ID in (" + sql + ") and NAME like '%" + userName + "%'";
		}
		List<Long> list = jdbcTemplate.queryForList(sql, Long.class);
		return list.stream().map((userId) -> {
			return DocumentUtils.getIntance().getUserById(userId);
		}).filter((vo) -> {
			return vo!=null;
		}).collect(Collectors.toList());
	}

	/**
	 * 获取节点的接收人
	 * 
	 * @param WorkID
	 * @param NodeID
	 * @param FK_Flow
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryReceiver")
	public List<PunUserBaseInfoVO> queryReceiver(String WorkID, String NodeID, String FK_Flow) {
		String sql = "select FK_Emp from wf_generworkerlist where fk_Node=? and workID=? and fk_flow=?";
		List<String> list = jdbcTemplate.queryForList(sql, String.class, NodeID, WorkID, FK_Flow);
		return list.stream().map((fkEmp) -> {
			List<PunUserBaseInfoVO> tempList = userService.selectByIDCard(fkEmp);
			if(tempList.isEmpty()) {
				return null;
			} else {
				return tempList.get(0);
			}
		}).filter((vo) -> {
			return vo!=null;
		}).collect(Collectors.toList());
	}

	/**
	 * 动态页面绑定列表
	 * @return
	 */
	@RequestMapping(value = "/list-bind")
	public ModelAndView list(String name,String currentPage,String pageSize) {
		ModelAndView mv = new ModelAndView();
		String sql = "select id,PAGEID_PC,PAGEID_PC_LIST,PAGEID_APP,PAGEID_APP_LIST from (select id,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_PC) as PAGEID_PC,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_PC_LIST) as PAGEID_PC_LIST,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_APP) as PAGEID_APP,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_APP_LIST) as PAGEID_APP_LIST "
				+ "from p_un_page_binding) t ";
		if (StringUtils.isNotBlank(name)) {
			sql += "where PAGEID_PC like '%" + name + "%' "
					+ "or PAGEID_PC_LIST like '%" + name + "%' "
					+ "or PAGEID_APP like '%" + name + "%' "
					+ "or PAGEID_APP_LIST like '%" + name + "%'";
		}
		int count = jdbcTemplate.queryForObject("select count(*) from (" + sql + ") temp", Integer.class);
        int ps = Integer.parseInt(StringUtils.isNumeric(pageSize) ? pageSize : "10");
        int cp = Integer.parseInt(StringUtils.isNumeric(currentPage) ? currentPage : "1");
		sql += " limit " + (cp - 1) * ps + "," + ps;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		PageList<Map<String,Object>> pl = new PageList<>(list, new Paginator(cp, ps, count));
		mv.addObject("name", name);
		mv.addObject("vos", pl);
		mv.setViewName("formdesigner/page/dynamicPage-bind-list");
		return mv;
	}

	/**
	 * 动态页面绑定保存
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/binding")
	public ReturnResult binding(String[] id) {
		ReturnResult ret = ReturnResult.get();
		Map<String, Object> map = new HashMap<String, Object>();
		Arrays.stream(id).forEach((pageId)->{
			// 是否重复
			String sql = "select count(*) from p_un_page_binding "
					+ "where PAGEID_PC=? or PAGEID_PC_LIST=? or PAGEID_APP=? or PAGEID_APP_LIST=?";
			int count = jdbcTemplate.queryForObject(sql, Integer.class, pageId, pageId, pageId, pageId);
			DynamicPageVO vo = formdesignerServiceImpl.findById(pageId);
			if (count != 0) {
				throw new PlatformException("页面" + vo.getName() + "已经绑定过");
			}
			if (vo.getPageType() == 1002 && !"APP".equalsIgnoreCase(vo.getDescription())) {
				map.put("PAGEID_PC", pageId);
			}
			if (vo.getPageType() == 1003 && !"APP".equalsIgnoreCase(vo.getDescription())) {
				map.put("PAGEID_PC_LIST", pageId);
			}
			if (vo.getPageType() == 1002 && "APP".equalsIgnoreCase(vo.getDescription())) {
				map.put("PAGEID_APP", pageId);
			}
			if (vo.getPageType() == 1003 && "APP".equalsIgnoreCase(vo.getDescription())) {
				map.put("PAGEID_APP_LIST", pageId);
			}
		});
		if (map.size() > 0) {
			String sql = "insert into p_un_page_binding(id,PAGEID_PC,PAGEID_PC_LIST,PAGEID_APP,PAGEID_APP_LIST) values(?,?,?,?,?)";
			jdbcTemplate.update(sql, UUID.randomUUID().toString(), 
					map.get("PAGEID_PC")==null?"":map.get("PAGEID_PC"), 
					map.get("PAGEID_PC_LIST")==null?"":map.get("PAGEID_PC_LIST"),
					map.get("PAGEID_APP")==null?"":map.get("PAGEID_APP"),
					map.get("PAGEID_APP_LIST")==null?"":map.get("PAGEID_APP_LIST"));
		}
		return ret.setData("1");
	}
	
	/**
	 * 删除动态页面绑定
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(String[] id) {
		Arrays.stream(id).forEach((s)->{
			jdbcTemplate.update("delete from p_un_page_binding where id=?", s);
		});
		return new ModelAndView("redirect:/common/user/list-bind.do");
	}

	/**
	 * 动态页面绑定详情
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get")
	public Map<String, Object> get(String id) {
		String sql = "select id,PAGEID_PC,PAGEID_PC_LIST,PAGEID_APP,PAGEID_APP_LIST,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_PC) as PAGEID_PC_Name,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_PC_LIST) as PAGEID_PC_LIST_Name,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_APP) as PAGEID_APP_Name,"
				+ "(select name from p_fm_dynamicpage where ID=PAGEID_APP_LIST) as PAGEID_APP_LIST_Name "
				+ "from p_un_page_binding where id=?";
		Map<String, Object> map = jdbcTemplate.queryForMap(sql, id);		
		return map;
	}

	/**
	 * 动态页面绑定修改
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update")
	public ReturnResult update(String id, String pc, String pc_list, String app, String app_list) {
		ReturnResult ret = ReturnResult.get();
		String[] arr = {pc, pc_list, app, app_list};
		long total = Arrays.stream(arr).distinct().count();
		if(total != 4) {
			throw new PlatformException("有重复页面");
		}
		Arrays.stream(arr).forEach((pageId)->{
			String sql = "select count(*) from p_un_page_binding "
					+ "where (PAGEID_PC=? or PAGEID_PC_LIST=? or PAGEID_APP=? or PAGEID_APP_LIST=?) and id<>?";
			int count = jdbcTemplate.queryForObject(sql, Integer.class, pageId, pageId, pageId, pageId, id);
			DynamicPageVO vo = formdesignerServiceImpl.findById(pageId);
			if (count != 0) {
				throw new PlatformException("页面" + vo.getName() + "已经绑定过");
			}
		});
		String sql = "update p_un_page_binding set PAGEID_PC=?,PAGEID_PC_LIST=?,PAGEID_APP=?,PAGEID_APP_LIST=? where id=?";
		jdbcTemplate.update(sql, pc, pc_list, app, app_list, id);
		return ret;
	}

}
