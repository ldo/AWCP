package cn.org.awcp.unit.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.org.awcp.core.utils.constants.OperTypeEnum;
import cn.org.awcp.unit.service.PunMenuService;
import cn.org.awcp.unit.service.PunRoleAccessService;
import cn.org.awcp.unit.service.PunRoleInfoService;
import cn.org.awcp.unit.vo.PunAJAXStatusVO;
import cn.org.awcp.unit.vo.PunRoleAccessVO;

/**
 * 角色菜单资源关联Controller
 *
 */
@Controller
@RequestMapping("/unit")
public class PunRoleAccessController {
	
	@Autowired
	@Qualifier("punRoleAccessServiceImpl")
	private  PunRoleAccessService roleAccService;
	
	@Autowired
	@Qualifier("punRoleInfoServiceImpl")
	private PunRoleInfoService roleService;// 角色Service
	
	@Autowired
	@Qualifier("punMenuServiceImpl")
	private PunMenuService menuService;// 菜单service
	
	/**
	 * 保存菜单权限
	 * 1.当传一个菜单结点，且传入的父节点ID不为空，则将该结点下所有的子节点赋权限； 
	 * 2.如果传入的父节点Id为空，则对该结点赋权限
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/punRoleAccessAJAXSave")
	public PunAJAXStatusVO punRoleAccessAJAXSave(PunRoleAccessVO vo) {
		PunAJAXStatusVO respStatus = new PunAJAXStatusVO();
		try {
			if (vo != null && vo.getRoleId() != null && vo.getResourceId() != null) {
				roleAccService.assignMenuAccessRight(vo.getRoleId(), vo.getResourceId(), vo.getOperType());
				respStatus.setStatus(0);
			} else {
				respStatus.setStatus(1);
			}
		} catch (Exception e) {
			respStatus.setStatus(1);
			respStatus.setMessage(e.getMessage());
		}
		return respStatus;
	}

	/**
	 * 删除菜单权限 
	 * 1.当传一个菜单结点，且传入的父节点ID不为空，则将删除该结点下所有的子节点权限；
	 * 2.如果传入的父节点Id为空，则删除该结点赋权限
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/punRoleAccessAJAXDelete")
	public PunAJAXStatusVO punRoleAccessAJAXDelete(PunRoleAccessVO vo) {
		PunAJAXStatusVO respStatus = new PunAJAXStatusVO();
		try {
			roleAccService.removeMenuAccessRight(vo.getRoleId(), vo.getResourceId());
			respStatus.setStatus(0);
		} catch (Exception e) {
			respStatus.setStatus(1);
			respStatus.setMessage(e.getMessage());
		}
		return respStatus;
	}

	/**
	 * 保存资源权限
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/punResoAccessAJAXSave")
	public PunAJAXStatusVO punResoAccessAJAXSave(PunRoleAccessVO vo) {
		PunAJAXStatusVO respStatus = new PunAJAXStatusVO();
		try {
			if (vo != null && vo.getRoleId() != null && vo.getResourceId() != null) {
				vo.setOperType(OperTypeEnum.OPER_ALL.getkey());
				roleAccService.addOrUpdateRole(vo);
				respStatus.setStatus(0);
			} else {
				respStatus.setStatus(1);
			}
		} catch (Exception e) {
			respStatus.setStatus(1);
			respStatus.setMessage(e.getMessage());
		}
		return respStatus;
	}

	/**
	 * 删除资源权限
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/punResoAccessAJAXDelete")
	public PunAJAXStatusVO punResoAccessAJAXDelete(PunRoleAccessVO vo) {
		PunAJAXStatusVO respStatus = new PunAJAXStatusVO();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("roleId", vo.getRoleId());
			params.put("resourceId", vo.getResourceId());
			roleAccService.resoAuthorDel(params);
			respStatus.setStatus(0);
		} catch (Exception e) {
			respStatus.setStatus(1);
			respStatus.setMessage(e.getMessage());
		}
		return respStatus;
	}

}
