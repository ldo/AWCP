package cn.org.awcp.venson.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.Springfactory;

public class DatabaseStorage {
	protected static final Log logger = LogFactory.getLog(DatabaseStorage.class);
    public static final String KEY_VALUE = "value";
    public static final String KEY_TIMEOUT = "timeout";
    public static final String KEY_SAVE_TIME = "saveTime";
    private static SzcloudJdbcTemplate jdbcTemplate;
    
    static {
    	DruidDataSource dataSource = Springfactory.getBean("masterDataSource");
    	jdbcTemplate = new SzcloudJdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
        jdbcTemplate.afterPropertiesSet();
        init();
    }
    
    //初始化数据库记录
    private static void init() {
    	String sql = "select ID,sequence,version from awcp_sequence";
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
        if(list.isEmpty()) {
        	String sequence = JSON.toJSONString(new JSONObject());      
        	sql = "insert into awcp_sequence(sequence,version) select ?,1 from dual where (select count(*) from awcp_sequence)=0";
        	jdbcTemplate.update(sql, sequence);
        }
    }
    
    /**
     * 将对象写入store
     *
     * @param key   键
     * @param value 值
     */
    public static void set(JSONObject store, String key, Object value) {
        set(store, key, value, -1);
    }

    /**
     * 将对象写入store
     *
     * @param key     键
     * @param value   值
     * @param timeout 过期时间(秒)，值为<=0时则为永久
     */
    public static void set(JSONObject store, String key, Object value, int timeout) {
        Map<String, Object> map = new HashMap<>(3);
        map.put(KEY_VALUE, value);
        map.put(KEY_TIMEOUT, timeout);
        //不设置过期时间
        if (timeout > 0) {
            map.put(KEY_SAVE_TIME, System.currentTimeMillis());
        }
        // 新增对象
        store.put(key, map);
    }

    /**
     * 将store保存到数据库
     * @param store
     * @param version
     * @return
     */
    public static boolean saveToDatabase(JSONObject store,Long version) {
    	String sequence = JSON.toJSONString(store);
        String sql = "update awcp_sequence set sequence=?,version=version+1 where version=?";
        int count = jdbcTemplate.update(sql, sequence, version);
        return count==1;
    }

    /**
     * 获取对象
     *
     * @param key   键
     * @param clazz 泛型
     * @return T
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static <T> T get(JSONObject store, String key, Class<T> clazz) {
        Object obj = store.get(key);
        if (obj == null) {
            return null;
        }
        if (!(obj instanceof Map)||((Map)obj).get(KEY_TIMEOUT)==null) {
            //移除缓存
            remove(store, key);
            return null;
        }
        Map map = (Map) obj;
        T value = (T) map.get(KEY_VALUE);
        int timeout = (int) map.get(KEY_TIMEOUT);
        //如果是<=0则直接返回
        if (timeout <= 0) {
            return value;
        } else {
            //判断是否过期
            long saveTime = (long) map.get(KEY_SAVE_TIME) + timeout * 1000;
            if (saveTime > System.currentTimeMillis()) {
                return value;
            } else {
                //移除缓存
                remove(store, key);
                return null;
            }
        }
    }

    /**
     * 获取对象
     *
     * @param key 键
     * @return Object
     */
    public static Object get(JSONObject store,String key) {
        return get(store, key, Object.class);
    }

    /**
     * 移除对象
     *
     * @param key 键
     * @return Object
     */
    public static Object remove(JSONObject store,String key) {
        if (isEmpty(store)) {
            return null;
        }
        Object data = store.remove(key);
        return data;
    }

    /**
     * 是否包含键值为key的映射
     *
     * @param key 键
     * @return boolean
     */
    public static boolean hasKey(JSONObject store, String key) {
        if (store.containsKey(key)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 是否是空的对象
     */
    public static boolean isEmpty(JSONObject store) {
        return store.isEmpty();
    }

    /**
     * 清空sequence
     */
    public static void clear() {
        String sql = "update awcp_sequence set sequence=?,version=1";
        String sequence = JSON.toJSONString(new JSONObject());     
        jdbcTemplate.update(sql, sequence);
    }

    /**
     * 获取数据库记录
     * @return
     */
    public static Map<String,Object> getStorage() {
    	String sql = "select ID,sequence,version from awcp_sequence";
    	Map<String,Object> map = jdbcTemplate.queryForMap(sql);
    	return map;
    }
}
