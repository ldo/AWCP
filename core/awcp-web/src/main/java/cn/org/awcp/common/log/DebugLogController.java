package cn.org.awcp.common.log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cn.org.awcp.core.utils.DateUtils;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.exception.PlatformException;

/**
 * 后台展示日志
 * @author yqtao
 *
 */
@Controller
@RequestMapping("/debug")
public class DebugLogController {

	private String logDir;
	
	/**
	 * 下载日志
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/downloadLog")
	public String downloadLog(HttpServletRequest request,HttpServletResponse response) {
		String logDir = getLogDir();
		String today = DateUtils.format(new Date(), "yyyy-MM-dd");
		String fileName = request.getParameter("file");
		String filePath = logDir + "/" + today + "/" + fileName;
		InputStream is = null;
		try {
			is = new FileInputStream(filePath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		if (is != null) {
			int i;
			try {
				response.setContentType("application/x-msdownload;");
				response.setHeader("Content-disposition",
						"attachment; filename=" + ControllerHelper.processFileName(fileName));
				OutputStream out = response.getOutputStream(); // 读取文件流
				i = 0;
				byte[] buffer = new byte[4096];
				while ((i = is.read(buffer)) != -1) { // 写文件流
					out.write(buffer, 0, i);
				}
				out.flush();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 清空日志
	 */
	@ResponseBody
	@RequestMapping("/clear")
	public ReturnResult deleteLog(HttpServletRequest request) {
		ReturnResult ret = new ReturnResult();
		String logDir = getLogDir();
		String today = DateUtils.format(new Date(), "yyyy-MM-dd");
		String fileName = request.getParameter("file");
		String filePath = logDir + "/" + today + "/" + fileName;
		File file = new File(filePath);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write("");
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	/**
	 * 获取日志
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/getLogs")
	public List<String> getLogs() throws Exception{
		List<String> ret = new ArrayList<String>();		
		String today = DateUtils.format(new Date(), "yyyy-MM-dd");
		String logDir = getLogDir();
		String todayLogDir = logDir + "/" + today + "/";
		String[] logFiles = {"error-log.log","script-log.log","sql-log.log"};
		for(String logFile : logFiles) {
			ret.add(randomAccessReadFile(todayLogDir + logFile));
		}
		return ret;
	}
	
	private String getLogDir() {
		if(StringUtils.isNotBlank(logDir)) {
			return logDir;
		}
		try {
			String path = DebugLogController.class.getClassLoader().getResource("conf/logback.xml").getFile();
			File file = new File(path);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();   
			DocumentBuilder builder = factory.newDocumentBuilder();   
			Document doc = builder.parse(file); 
			NodeList nodes = doc.getElementsByTagName("property");
			Node node = nodes.item(0);
			logDir = node.getAttributes().item(1).getNodeValue();
			return logDir;
		} catch(Exception e) {
			throw new PlatformException("无法获取到日志目录");			
		}		
	}
	
	private String randomAccessReadFile(String filePath) {
		RandomAccessFile rf = null;
		StringBuffer sb = new StringBuffer();
		try {
			rf = new RandomAccessFile(filePath, "r"); // r:可读,w：可写
			long len = rf.length(); // 文件长度
			if (len == 0) {
				return "";
			}
			long nextend = len - 1; // 指针是从0到length-1
			String line = null;
			int c = -1;
			int i = 300000;
			rf.seek(nextend); // seek到最后一个字符
			while (nextend >= 0 && i >= 0) {
				c = rf.read();
				// 只有行与行之间才有\r\n，这表示读到每一行上一行的末尾的\n，而执行完read后， 指针指到了这一行的开头字符
				if (c == '\n') {
					line = rf.readLine();
					if (line != null) {
						// RandomAccessFile的readLine方法读取文本为ISO-8859-1，需要转化为utf-8
						line = new String(line.getBytes("ISO-8859-1"), "utf-8");
						if (line.length() > 4 && StringUtils.isNumeric(line.substring(0, 2))
								&& !line.contains("ERROR")) {
							sb.insert(0, "<pre>" + line + "</pre>");
						} else {
							sb.insert(0, "<pre class='text-danger'>" + line + "</pre>");
						}
					}
				}
				// 这一句必须在这个位置，如果在nextend--后，那么导致进0循环后去seek-1索引，报异常
				// 如果在read()以前，那么导致进入0循环时，因为read指针到1，第一行少读取一个字符
				rf.seek(nextend);
				if (nextend == 0) {// 当文件指针退至文件开始处，输出第一行
					line = rf.readLine();
					if (line != null) {
						// RandomAccessFile的readLine方法读取文本为ISO-8859-1，需要转化为utf-8
						line = new String(line.getBytes("ISO-8859-1"), "utf-8");
						if (line.length() > 4 && StringUtils.isNumeric(line.substring(0, 2))
								&& !line.contains("ERROR")) {
							sb.insert(0, "<pre>" + line + "</pre>");
						} else {
							sb.insert(0, "<pre class='text-danger' >" + line + "</pre>");
						}
					}
				}
				i--;
				nextend--;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(rf);
		}
		return sb.toString();
	}

}
