package cn.org.awcp.miniapp.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.miniapp.service.MiniAppService;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.controller.base.ReturnResult;

/**
 * 微信小程序Controller
 * @author yqtao
 *
 */
@Controller
public class MiniAppController {

	@Autowired
	private MiniAppService miniAppService;
	
	/**
	 * 获取用户的openid
	 * @param code		获取openid的code
	 * @param appid		微信小程序appid
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/anon/miniapp/getOpenid")
	public ReturnResult getOpenid(String code, String appid) {
		ReturnResult ret = ReturnResult.get();
		ret.setData(miniAppService.getOpenid(code, appid));
		return ret;
	}
	
	/**
	 * 微信小程序登录
	 * @param code		获取openid的code
	 * @param appid		微信小程序appid
	 * @param nickName	微信用户的昵称
	 * @param imgs		微信用户的头像
	 * @param createAQR 是否生成二维码，createAQR=Y时则生成
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/anon/miniapp/login")
	public ReturnResult login(String code, String appid, String nickName, String imgs, String createAQR) {
		ReturnResult ret = ReturnResult.get();
		//通过code获取openid
		String openid = miniAppService.getOpenid(code, appid);
		//创建用户
		if("Y".equals(createAQR)) {
			String imgName = openid + ".jpg";
			String path = "pages/index/index?shareOpenid=" + openid;
			miniAppService.createUser(openid, nickName, imgs, imgName, path, appid);
		} else {
			miniAppService.createUser(openid, nickName, imgs);
		}	
		//登录并返回登录系统凭证
		ControllerHelper.toLogin(openid, true);
		Session session = SessionUtils.getCurrentSession();		
		Serializable sid = session.getId();
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("sid", sid);
		result.put("openid", openid);
		ret.setData(result);
		return ret;
	}
	
}
