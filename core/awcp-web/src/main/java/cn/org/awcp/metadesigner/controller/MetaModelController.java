package cn.org.awcp.metadesigner.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.Criteria;
import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.metadesigner.application.DataSourceManageService;
import cn.org.awcp.metadesigner.application.MetaModelItemService;
import cn.org.awcp.metadesigner.application.MetaModelService;
import cn.org.awcp.metadesigner.application.SysSourceRelationService;
import cn.org.awcp.metadesigner.util.CreateTables;
import cn.org.awcp.metadesigner.vo.MetaModelItemsVO;
import cn.org.awcp.metadesigner.vo.MetaModelVO;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;
import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.venson.util.BeanUtil;

/**
 * 元数据管理
 * 
 */
@Controller
@RequestMapping("/metaModel")
public class MetaModelController extends BaseController {
    /**
     * 日志对象
     */
    private Log logger = LogFactory.getLog(getClass());

    @Autowired
    private MetaModelService metaModelServiceImpl;

    @Autowired
    private MetaModelItemService metaModelItemsServiceImpl;

    @Autowired
    private CreateTables createTables;

    @Autowired
    @Qualifier("dataSourceManageServiceImpl")
    DataSourceManageService dataSourceService;

    @Autowired
    @Qualifier("sysSourceRelationServiceImpl")
    SysSourceRelationService sysSourceRelationService;
  
    @Autowired
    private SzcloudJdbcTemplate jdbcTemplate;

    /**
     * 元数据列表页面
     * @param vo
     * @param model
     * @param currentPage
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/selectPage")
    public String selectPageByExample(@ModelAttribute MetaModelVO vo, Model model,
                                      @RequestParam(required = false, defaultValue = "1") int currentPage,
                                      @RequestParam(required = false, defaultValue = "25") int pageSize) {
    	if (currentPage == 0) {
            currentPage = 1;
        }
        BaseExample be = new BaseExample();    
        Criteria c = be.createCriteria();   
        String tableName = vo.getTableName();
        if (StringUtils.isNotBlank(tableName)) {
            c.andLike("tableName", "%" + tableName + "%");
        }
        String modelName = vo.getModelName();
        if (StringUtils.isNotBlank(modelName)) {
            c.andLike("modelName", "%" + modelName + "%");
        }
        String modelCode = vo.getModelCode();
        if (StringUtils.isNotBlank(modelCode)) {
            c.andLike("modelCode", "%" + modelCode + "%");
        }       
        Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
        if (obj instanceof PunSystemVO) {
            PunSystemVO system = (PunSystemVO) obj;
            c.andEqualTo("SYSTEM_ID", system.getSysId());
        }
        PageList<MetaModelVO> pl = metaModelServiceImpl.selectPagedByExample(be, currentPage, pageSize,"seq desc");
        model.addAttribute("list", pl);
        return "metadesigner/metaModel/findAll";
    }
    
    /**
     * 跳转到新增页面
     * @param model
     * @return
     */
    @RequestMapping("toggle")
    public String toggle(Model model) {       
        return "metadesigner/metaModel/metaModel_add";
    }
    
    /**
     * 保存
     * @param vo
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/save")
    public ReturnResult save(@ModelAttribute MetaModelVO vo, Model model) {
    	ReturnResult ret = ReturnResult.get();
        // 判读元数据是否存在 存在不增加 不存在则增加
    	validate(vo.getModelCode(), vo.getTableName(), vo.getModelName(), "");
        vo.setSystemId(ControllerHelper.getSystemId());
        vo.setModelSynchronization(false);
        vo.setModelValid(false);
        metaModelServiceImpl.save(vo);
        return ret;
    }
    
    /**
     * 数据库新增元数据
     * @param tableNames
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/addByDb")
    public String addByDb(String[] tableNames) {
        JSONObject rtn = new JSONObject();
        try {
            for (int i = 0; i < tableNames.length; i++) {
                MetaModelVO modelVO = metaModelServiceImpl.queryByModelCode(tableNames[i]);
                modelVO = BeanUtil.instance(modelVO, MetaModelVO.class);
                if (StringUtils.isBlank(modelVO.getTableName()) || StringUtils.isBlank(modelVO.getId())) {
                    modelVO.setModelCode(tableNames[i]);
                    modelVO.setModelName(tableNames[i]);
                    modelVO.setTableName(tableNames[i]);
                    modelVO.setSystemId(ControllerHelper.getSystemId());
                    String id = metaModelServiceImpl.save(modelVO);
                    modelVO.setId(id);
                } else {
                    metaModelItemsServiceImpl.removeByFk(modelVO.getId());
                }
                syncMeta(modelVO);
            }
            rtn.put("result", "1");
        } catch (Exception e) {
            rtn.put("result", "-1");
            logger.info("ERROR", e);
        }     
        return rtn.toJSONString();
    }

    /**
	 * 根据表名生成元数据
	 * @param request
	 * @return
	 */
	@Transactional
    @ResponseBody
	@RequestMapping(value="/generateModelMeta")
	public ReturnResult generateModelMeta(HttpServletRequest request) {
		ReturnResult ret = new ReturnResult();
		String table = request.getParameter("table");	
		MetaModelVO modelVO = metaModelServiceImpl.queryByModelCode(table);
        modelVO = BeanUtil.instance(modelVO, MetaModelVO.class);
        if (StringUtils.isBlank(modelVO.getTableName()) || StringUtils.isBlank(modelVO.getId())) {
            modelVO.setModelCode(table);
            modelVO.setModelName(table);
            modelVO.setTableName(table);
            modelVO.setSystemId(ControllerHelper.getSystemId());
            String id = metaModelServiceImpl.save(modelVO);
            modelVO.setId(id);
        } else {
            metaModelItemsServiceImpl.removeByFk(modelVO.getId());
        }
        syncMeta(modelVO);
        ret.setData(metaModelItemsServiceImpl.queryResult("queryResult", modelVO.getId()));	
		return ret;
	}
    
    /**
     * 查询一条数据 用于修改按钮
     * @param id
     * @param pw
     * @return
     */
    @RequestMapping(value = "/get")
    public String get(String id, Model model) {
        MetaModelVO mmo = new MetaModelVO();
        try {
            mmo = metaModelServiceImpl.get(id);
            model.addAttribute("vo", mmo);
        } catch (Exception e) {
            logger.info("ERROR", e);
            return null;
        }
        return "metadesigner/metaModel/metaModel_add";
    }
    
    /**
     * 修改
     * @param vo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/update")
    public ReturnResult update(@ModelAttribute MetaModelVO vo) {
    	ReturnResult ret = ReturnResult.get();
    	validate(vo.getModelCode(), vo.getTableName(), vo.getModelName(), vo.getId());
        vo.setModelSynchronization(false);
        vo.setModelValid(false);
        // 查看表名是否修改
        MetaModelVO vvo = metaModelServiceImpl.get(vo.getId());
        if (!vo.getTableName().equals(vvo.getTableName())) {
            // 查看这张表是否存在
            if (metaModelServiceImpl.tableIsExist(vvo.getTableName())) {
                // 修改表名
                StringBuffer sb = new StringBuffer("ALTER TABLE ").append(vvo.getTableName()).append(" RENAME TO ")
                        .append(vo.getTableName());
                metaModelServiceImpl.excuteSql(sb.toString());
            }
        }
        metaModelServiceImpl.update("update", vo);
        return ret;
    }
    
    /**
     * 分页查询
     *
     * @param vo
     * @param currentPage
     * @param model
     * @param session
     * @return
     */
    @RequestMapping(value = "/queryResult")
    public String queryResult(@ModelAttribute MetaModelVO vo,
                              @RequestParam(required = false, defaultValue = "1") int currentPage, Model model) {
        int pageSize = 20;
        String queryStr = "queryList";
        Map<String, Object> map = new HashMap<String, Object>();
        if (currentPage == 0) {
            currentPage = 1;
        }
        map.put("modelClassId", vo.getModelClassId());
        map.put("modelName", vo.getModelName());
        map.put("modelCode", vo.getModelCode());
        map.put("tableName", vo.getTableName());
        map.put("projectName", vo.getProjectName());
        PageList<MetaModelVO> pl = metaModelServiceImpl.queryResult(queryStr, map, currentPage, pageSize, null);
        model.addAttribute("list", pl);
        model.addAttribute("currentPage", currentPage);
        return "metadesigner/metaModel/findAll";
    }
    
    /**
     * 数据库同步到元数据
     * @param id
     * @return
     */
    @RequestMapping(value = "/synchronizedMeta")
    @ResponseBody
    public ReturnResult synchronizedMeta(@RequestParam(value = "id") String id) {
    	ReturnResult ret = ReturnResult.get();
    	metaModelItemsServiceImpl.removeByFk(id);
    	MetaModelVO mmv = metaModelServiceImpl.get(id);
    	syncMeta(mmv);
       	return ret;
    }

    //同步表到元数据
    private void syncMeta(MetaModelVO mmv) {
    	try {
    		List<Map<String, Object>> list = jdbcTemplate.queryForList("SHOW FULL FIELDS FROM " + mmv.getTableName());
            for (Map<String, Object> map : list) {
                MetaModelItemsVO item = new MetaModelItemsVO();
                String Field = (String) map.get("Field");
                String Type = (String) map.get("Type");
                String key = (String) map.get("Key");
                String Null = (String) map.get("Null");
                String Default = (String) map.get("Default");
                String Comment = (String) map.get("Comment");
                if ("PRI".equals(key)) {
                    item.setUsePrimaryKey(1);
                    item.setUseIndex(-1);
                } else if ("UNI".equals(key)) {
                    item.setUseIndex(1);
                } else if ("MUL".equals(key)) {
                    item.setUseIndex(2);
                } else {
                    item.setUseIndex(0);
                }
                int index = Type.indexOf("(");
                if (index != -1) {
                    String length = Type.substring(index + 1, Type.length() - 1);
                    item.setItemLength(length);
                    Type = Type.replace("(" + length + ")", "");
                }
                if ("NO".equals(Null)) {
                    item.setUseNull(1);
                } else {
                    item.setUseNull(0);
                }
                item.setRemark(Comment);
                item.setModelId(mmv.getId());
                item.setItemName(Field);
                item.setItemCode(Field);
                item.setItemType(Type);
                item.setItemValid(1);
                item.setDefaultValue(Default);
                metaModelItemsServiceImpl.save(item);
            }
    	} catch(Exception e) {
    		e.printStackTrace();
    		throw new PlatformException("数据库表" + mmv.getTableName() + "同步元数据失败，可能该表在数据库不存在。");
    	}       
    }

    /**
     * 只删除元数据
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/removeModel")
    public String removeModel(String[] id, Model model) {
        try {
            // 查询对应的关系
            for (int i = 0; i < id.length; i++) {
                if (StringUtils.isNotBlank(id[i])) {
                    // 查询元数据
                    MetaModelVO mmo = metaModelServiceImpl.get(id[i]);
                    metaModelItemsServiceImpl.removeByFk(id[i]);
                    metaModelServiceImpl.remove(mmo);
                }
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        return "redirect:queryResult.do";
    }

    /**
     * 删除元数据并删除数据库表
     *
     * @param classId
     * @param model
     * @return
     */
    @RequestMapping(value = "/remove")
    public String remove(String[] id, Model model) {
        try {
            for (String i : id) {
                if (StringUtils.isNotBlank(i)) {
                    // 查询元数据
                    MetaModelVO mmo = metaModelServiceImpl.get(i);
                    if (mmo != null) {
                        metaModelItemsServiceImpl.removeByFk(i);
                        metaModelServiceImpl.remove(mmo);
                        String sql = "drop table " + mmo.getTableName();
                        try {
                            jdbcTemplate.execute(sql);
                        } catch (Exception e) {
                        	
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        return "redirect:queryResult.do";
    }

    /**
     * 创建数据库表
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/createTable", method = RequestMethod.POST)
    @ResponseBody
    public ReturnResult createTable(@RequestParam(value = "id") String id) {
        ReturnResult result = ReturnResult.get();
        MetaModelVO vo = metaModelServiceImpl.get(id);
        // 判断表是否存在
        try {
            jdbcTemplate.beginTranstaion();
            if (metaModelServiceImpl.tableIsExist(vo.getTableName())) {
                jdbcTemplate.update("drop table " + vo.getTableName());
            }
            List<MetaModelItemsVO> pl = metaModelItemsServiceImpl.queryResult("queryResult", vo.getId());
            String sql = createTables.getSql(vo, pl);
            jdbcTemplate.update(sql);
            vo.setModelSynchronization(true);
            metaModelServiceImpl.update("update", vo);
            jdbcTemplate.commit();
            result.setStatus(StatusCode.SUCCESS);
        } catch (Exception e) {
            jdbcTemplate.rollback();
            logger.info("ERROR", e);
            result.setStatus(StatusCode.FAIL).setMessage(e.getMessage());
        }

        return result;
    }

    /**
     * 根据modelCode,modelName 查询元数据，用于动态页面的数据源新增
     * @param modelCode
     * @param modelName
     * @param page
     * @param rows
     * @return
     * @throws SQLException
     */
    @ResponseBody
    @RequestMapping("/queryPageByModel")
    public List<Map<String, Object>> queryPageByModel(String modelCode, String modelName,
    		@RequestParam(required = false, defaultValue = "1") int page) throws SQLException {
        StringBuffer b = new StringBuffer("select modelClassId,modelCode,modelName,modelDesc,tableName,projectName,modelType,"
                + "modelSynchronization,modelValid,id,system_id from fw_mm_metaModel where 1=1");
        Object[] objs = null;
        if (modelCode != null && "".equals(modelCode)) {
            b.append(" and modelCode like %");
            b.append(modelCode).append("%");
        }
        if (modelName != null && "".equals(modelName)) {
            b.append(" and modelName like %");
            b.append(modelName).append("%");
        }
        Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
        if (obj instanceof PunSystemVO) {
            PunSystemVO system = (PunSystemVO) obj;
            b.append(" and SYSTEM_ID=").append(system.getSysId());
        }
        List<Map<String, Object>> list = jdbcTemplate.queryForList(b.toString(), objs);
        return list;
    }

    /**
     * 通过元数据获取其明细，用于动态页面的数据源新增
     * @param modelCode
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "queryModelItemByModel")
    public List<MetaModelItemsVO> queryModelItemByModel(String modelCode) {
        try {
            MetaModelVO vo = metaModelServiceImpl.queryByModelCode(modelCode);
            List<MetaModelItemsVO> ls = metaModelItemsServiceImpl.queryResult("queryResult", vo.getId());
            return ls;
        } catch (Exception e) {
            return null;
        }
    }

    //校验元数据是否已经存在
    private void validate(String modelCode, String tableName, String modelName, String id) {
    	List<MetaModelVO> ls = new ArrayList<MetaModelVO>();
        if (StringUtils.isBlank(id)) {
            ls = metaModelServiceImpl.queryMetaModel("queryMetaModel", modelCode, tableName, modelName);
        } else {
            MetaModelVO mm = metaModelServiceImpl.get(id);
            ls = metaModelServiceImpl.queryMetaModel("queryMetaModel", modelCode, tableName, modelName);
            ls = ls.stream().filter((mmv) -> !mmv.getId().equals(mm.getId())).collect(Collectors.toList());
        }
        if(!ls.isEmpty()) {
        	throw new PlatformException("已经有了该数据源");
        }
    }

}
