package cn.org.awcp.unit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.org.awcp.unit.service.PunMenuService;
import cn.org.awcp.unit.service.PunSystemService;
import cn.org.awcp.unit.utils.JsonFactory;
import cn.org.awcp.unit.utils.ResourceTreeUtils;
import cn.org.awcp.unit.vo.PunMenuVO;
import cn.org.awcp.unit.vo.PunResourceTreeNode;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ReturnResult;

/**
 * 系统管理
 *
 */
@Controller
@RequestMapping("/dev")
public class PunDevUserSystemController {
	/**
	 * 日志对象
	 */
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	@Qualifier("punSystemServiceImpl")
	private PunSystemService sysService;
	
	@Autowired
	@Qualifier("punMenuServiceImpl")
	private PunMenuService menuService;
	
	/**
	 * 根据ID查找
	 * @return
	 */
	@RequestMapping(value = "punSystemGet")
	public ModelAndView punSystemGet() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("unit/dev/punDevSystem-edit");
		try {
			PunSystemVO vo = sysService.findAll().get(0);
			mv.addObject("vo", vo);
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "异常");
		}
		return mv;
	}	
	
	/**
	 * 保存系统
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveSystem")
	public ReturnResult saveSystem(PunSystemVO vo) {
		ReturnResult ret = ReturnResult.get();
		sysService.addOrUpdate(vo);
		return ret;
	}

	/**
	 * 根据SystemId返回系统的菜单树(json)
	 * @return
	 */
	@RequestMapping(value = "punSysMenuEdit", method = RequestMethod.GET)
	public ModelAndView punSysMenuEdit() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("unit/dev/punSysMenu-edit");
		try {
			List<PunMenuVO> resoVOs = menuService.findAll();
			ResourceTreeUtils rtu = new ResourceTreeUtils();
			List<PunMenuVO[]> list = rtu.generateTreeView(resoVOs);
			Map<Integer, List<PunResourceTreeNode>> resultMap = new HashMap<Integer, List<PunResourceTreeNode>>();
			Integer index = 1;
			for (PunMenuVO[] prvo : list) {
				// 以目录根节点的index为key，以manyNodeTree为value
				List<PunResourceTreeNode> manyNodeTree = ResourceTreeUtils.getPlainZNodes(prvo);
				resultMap.put(index, manyNodeTree);
				index++;
			}
			JsonFactory factory = new JsonFactory();
			mv.addObject("menuJson", factory.encode2Json(resultMap));
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "异常");
		}
		return mv;
	}

}
