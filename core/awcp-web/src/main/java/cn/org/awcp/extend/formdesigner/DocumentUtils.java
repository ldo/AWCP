package cn.org.awcp.extend.formdesigner;

import cn.org.awcp.core.utils.Springfactory;
import cn.org.awcp.extend.workflow.WorkFlowExtend;
import cn.org.awcp.formdesigner.application.service.FormdesignerService;
import cn.org.awcp.formdesigner.application.vo.DocumentVO;
import cn.org.awcp.formdesigner.application.vo.DynamicPageVO;
import cn.org.awcp.formdesigner.utils.BaseUtils;
import cn.org.awcp.unit.message.PunNotification;
import cn.org.awcp.venson.common.I18nKey;
import cn.org.awcp.venson.controller.base.ControllerContext;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.venson.util.BeanUtil;
import cn.org.awcp.workflow.service.IWorkFlowService;

import com.alibaba.fastjson.JSON;

import BP.WF.GenerWorkFlowAttr;
import BP.WF.WFState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.WebApplicationContext;

/**
 * 文档操作
 */
public class DocumentUtils extends BaseUtils {

	private static DocumentUtils utils = new DocumentUtils();

	public static DocumentUtils getIntance() {
		return utils;
	}

	private DocumentUtils() {
	}

	/**
	 * 获取当前文档
	 * 
	 * @return
	 */
	public DocumentVO getCurrentDocument() {
		return ControllerContext.getDoc();
	}

	// *******************************钉钉发送OA消息-begin************************************************
	public boolean sendMessage(String url, String type, String json, String title, String ids, String agentId) {
		return sendMessage(url, type, JSON.parseObject(json, Map.class), title, ids, agentId, null, null);
	}

	public boolean sendMessage(String url, String type, String json, String title, String ids) {
		return sendMessage(url, type, JSON.parseObject(json, Map.class), title, ids, null, null, null);
	}

	public boolean sendMessage(String url, String type, Map<?, ?> content, String title, String ids) {
		return sendMessage(url, type, content, title, ids, null, null, null);
	}


	/**
	 * 钉钉OA消息发送
	 * 
	 * @param url
	 *            客户端点击消息时跳转到的H5地址
	 * @param type
	 *            0：用户，1：群组
	 * @param content
	 *            消息body键值对内容
	 * @param title
	 *            标题
	 * @param ids
	 *            推送用户或群组的ID，逗号分隔,
	 * @param agentId
	 *            微应用Id
	 * @param img
	 *            消息body的图片
	 * @param bodyContent
	 *            消息body的content
	 * @return
	 */
	public boolean sendMessage(String url, String type, Map<?, ?> content, String title, String ids, String agentId,
			String img, String bodyContent) {
		return true;
	}
	// *******************************钉钉发送OA消息-end************************************************

	/**
	 * 根据ID查询指定表并返回指定字段
	 * 
	 * @param table
	 *            要查询的表名
	 * @param field
	 *            要查询的字段
	 * @param idName
	 *            主键
	 * @param value
	 *            主键值
	 */
	public Object queryObjectById(String table, String field, String idName, Object value) {
		return this.excuteQueryForObject("select " + field + " from " + table + " where " + idName + "=?", value);
	}

	public String getLocal() {
		if (ControllerHelper.getLang() == Locale.SIMPLIFIED_CHINESE) {
			return "zh";
		} else {
			return "en";
		}
	}
	
	public Object getServletBean(String beanName) {
		HttpServletRequest request = ControllerContext.getRequest();
		WebApplicationContext context = (WebApplicationContext) 
				request.getAttribute("org.springframework.web.servlet.DispatcherServlet.CONTEXT");
		return context.getBean(beanName);
	}
	
	public Map<?,?> createMap(){
		return new HashMap<>();
	}
	
	public List<?> createList() {
		return new ArrayList<>();
	}
	
	public Map<String,Object> executeFlow(String FK_Flow, Integer FK_Node,String dynamicPageId,String FID,
			int actType,String masterDataSource,Map<String,String> otherParams){
		HttpServletRequest request = ControllerContext.getRequest();
		String workId = request.getParameter("WorkID");			
		return executeFlowWithWorkID(FK_Flow, FK_Node, workId, dynamicPageId, FID, actType, masterDataSource, otherParams);
	}
	
	public Map<String,Object> executeFlowWithWorkID(String FK_Flow, Integer FK_Node, String workId,String dynamicPageId,String FID,
			int actType,String masterDataSource,Map<String,String> otherParams){
		//初始化
		HttpServletRequest request = ControllerContext.getRequest();
		WorkFlowExtend workFlowExtend = Springfactory.getBean("workFlowExtend");
		IWorkFlowService iWorkFlowServiceImpl = Springfactory.getBean("iWorkFlowServiceImpl");
		Map<String, Object> resultMap = new HashMap<>(16);	
		String slectsUserIds = request.getParameter("slectsUserIds");        
        resultMap.put(GenerWorkFlowAttr.WFState, WFState.Blank);
        DocumentVO docVo;    
        Integer WorkID = StringUtils.isNumeric(workId)?Integer.parseInt(workId):null;
        try {
            // 如果不存在workId则是新建
            if (WorkID==null || WorkID==0) {
                docVo = new DocumentVO();
                docVo.setUpdate(false);
            } else {
                docVo = documentService.findDocByWorkItemId(FK_Flow, WorkID);
                docVo = BeanUtil.instance(docVo, DocumentVO.class);
                docVo.setWorkItemId(WorkID);
                docVo.setUpdate(true);
            }
            //设置参数
            docVo.setFlowTempleteId(FK_Flow);
            docVo.setEntryId(FK_Node);
            docVo.setWorkflowId(FK_Flow);
            docVo.setFid(FID);
            docVo.setActType(actType);
            docVo.setNextUser(slectsUserIds);
            //设置数据源值
            DynamicPageVO pageVo = setDataSorceFromRequestParameter(docVo, dynamicPageId, masterDataSource, otherParams);
            //开启事务
            ControllerContext.setDoc(docVo);
            ControllerContext.setPage(pageVo);
            //设置数据源ID
            if (StringUtils.isNotBlank(docVo.getRecordId()) && StringUtils.isNotBlank(masterDataSource)) {
                if (StringUtils.isBlank(getDataItem(masterDataSource, "ID"))) {
                    setDataItem(masterDataSource, "ID", docVo.getRecordId());
                }
            }
            workFlowExtend.beforeExecuteFlow(request, docVo);
            resultMap.putAll(iWorkFlowServiceImpl.execute(pageVo, docVo, masterDataSource));
            workFlowExtend.afterExecuteFlow(docVo, resultMap);
            //刷新自己桌面
            PunNotification.sendFlow(ControllerHelper.getUser().getUserIdCardNumber());
            resultMap.put("docId", docVo.getId());
            resultMap.put("WorkItemID", docVo.getWorkItemId());
            resultMap.put("EntryID", FK_Node);
            resultMap.put("dynamicPageId", dynamicPageId);
            resultMap.put("flowTempleteId", FK_Flow);
            resultMap.put("FID", FID);
            return resultMap;
        } catch (Exception e) {
            logger.info("ERROR", e);          
            if (e instanceof PlatformException) {
                throw (PlatformException) e;
            } else {
                throw new PlatformException(ControllerHelper.getMessage(I18nKey.wf_operation_failure));
            }
        }		
	}
	
    private DynamicPageVO setDataSorceFromRequestParameter(DocumentVO docVo,String dynamicPageId, 
    		String masterDataSource,Map<String,String> otherParams){
    	HttpServletRequest request = ControllerContext.getRequest();
		FormdesignerService formdesignerServiceImpl = Springfactory.getBean("formdesignerServiceImpl");
        //如果为空不执行因为手机端渲染没有用动态页面
        if (StringUtils.isNotBlank(dynamicPageId)) {
            DynamicPageVO pageVO = formdesignerServiceImpl.findById(dynamicPageId);
            if (pageVO != null) {
                docVo.setDynamicPageId(dynamicPageId);
                docVo.setDynamicPageName(pageVO.getName());
                //将request中的参数转成map
                Map<String, String[]> enumeration = request.getParameterMap();
                Map<String, String> params = new HashMap<>(enumeration.size());
                for (Map.Entry<String, String[]> entry : enumeration.entrySet()) {
                    params.put(entry.getKey(), StringUtils.join(entry.getValue(), ";"));
                }
                params.putAll(otherParams);
                //设置request参数
                docVo.setRequestParams(params);
                //将参数设置到数据源
                List<Map<String, String>> list = new ArrayList<Map<String,String>>();
                list.add(params);
                Map<String, List<Map<String, String>>> listParams = docVo.getListParams();
                listParams.put(masterDataSource, list);
                return pageVO;
            }
        }
        return null;
    }
    
    public int saveOrUpdateData(Map<String,Object> params,String tableName,boolean isUpdate) {
    	params.putAll(getParams());
    	if(params.isEmpty()) {
    		throw new PlatformException("没有传参数");
    	}
    	List<String> columns = getColumn(tableName);
    	for(String mapKey : params.keySet()) {
    		if(!"ID".equalsIgnoreCase(mapKey)) {
    			if(!columns.contains(mapKey)) {
    				params.remove(mapKey);
    			}
    		}
    	}
        StringBuffer sb = new StringBuffer();
        List<Object> args = new ArrayList<Object>();       
        if(isUpdate) {
        	sb.append("update ").append(tableName).append(" set ");
        	Object id = null;
        	for(String key : params.keySet()) {
        		if("ID".equalsIgnoreCase(key)) {
        			id = params.get(key);
        		} else {
        			Object value = params.get(key);
        			sb.append(key).append("=?,");
	         		args.add(value);
        		}       		
        	}
        	if(id == null) {
        		throw new PlatformException("没有传参数id");
        	} else {
        		if(sb.indexOf(",") != -1) {
            		sb.deleteCharAt(sb.length() - 1);
            	}
        		sb.append(" where ID=?");
        		args.add(id);
        	}
        } else {
        	sb.append("insert into ").append(tableName).append(" ("); 
            for(String key : params.keySet()) {
            	Object value = params.get(key);
            	sb.append(key).append(",");
        		args.add(value);
            }
            if(sb.indexOf(",") != -1) {
        		sb.deleteCharAt(sb.length() - 1);
        	}
            sb.append(") values (");
            for(int i=0;i<args.size();i++) {
            	if(i != args.size()-1) {
            		sb.append("?,");
            	} else {
            		sb.append("?)");
            	}
            }
        }    
        return jdbcTemplate.update(sb.toString(), args.toArray());
    }
    
    private List<String> getColumn(String tableName){
    	String sql = "select COLUMN_NAME from information_schema.COLUMNS where table_name=?";
    	return jdbcTemplate.queryForList(sql, String.class, tableName);
    }
    
    public Map<String,String> getParams(){
    	HttpServletRequest request = ControllerContext.getRequest();
    	Map<String, String[]> enumeration = request.getParameterMap();
        Map<String, String> params = new HashMap<>(enumeration.size());
        for (Map.Entry<String, String[]> entry : enumeration.entrySet()) {
            params.put(entry.getKey(), StringUtils.join(entry.getValue(), ";"));
        }
        return params;
    }

}
