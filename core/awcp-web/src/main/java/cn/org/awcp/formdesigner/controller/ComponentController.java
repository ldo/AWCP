package cn.org.awcp.formdesigner.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.utils.BeanUtils;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.formdesigner.application.service.StoreService;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.formdesigner.core.constants.FormDesignGlobal;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ControllerHelper;

/**
 * 组件管理Controller
 *
 */
@Controller
@RequestMapping("/component")
public class ComponentController extends BaseController {

	@Autowired
	@Qualifier("storeServiceImpl")
	private StoreService storeService;

	/**
	 * 校验名称是否唯一
	 * 
	 * @param dynamicPageId
	 * @param componentName
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/validateComponentNameInPage")
	public boolean checkComponentNameInPage(String dynamicPageId, String componentName, String componentId) {
		BaseExample baseExample = new BaseExample();
		baseExample.createCriteria().andEqualTo("DYNAMICPAGE_ID", dynamicPageId).andEqualTo("NAME", componentName);
		List<StoreVO> list = storeService.selectPagedByExample(baseExample, 1, Integer.MAX_VALUE, null);
		// 更新操作（componentId不为空）时，如果输入的名字在数据库中已经存在，而且查询出来的结果与提交的componentId匹配，则说明该name可以用，否则，该name不允许使用
		if (list == null || list.isEmpty()) {
			return true;
		} else {
			for (int i = 0; i < list.size(); i++) {
				if (!list.get(i).getId().equalsIgnoreCase(componentId)) {
					return false;
				}
			}
		}
		return true;
	}

	@RequestMapping("/toedit")
	public ModelAndView toEditComponent(String dynamicPageId, String componentType, String componentId) {
		ModelAndView mv = new ModelAndView();
		if (StringUtils.isBlank(componentId) && (dynamicPageId == null || StringUtils.isBlank(componentType))) {
			mv.setViewName("");// error
		} else {
			if (!StringUtils.isBlank(componentId)) {
				mv.addObject("componentId", componentId);
				StoreVO vo = storeService.findById(componentId);
				if (vo != null && StringUtils.isNotBlank(vo.getContent())) {
					JSONObject o = JSONObject.parseObject(vo.getContent());
					componentType = o.getString("componentType");
					mv.addObject("componentType", componentType);
				}
			} else {
				mv.addObject("dynamicPageId", dynamicPageId);
				mv.addObject("componentType", componentType);
			}
			if (StringUtils.isBlank(componentType)) {
				// error
			} else {
				String url = FormDesignGlobal.COMPOENT_TYPE_URL.get(componentType);
				if (StringUtils.isBlank(url)) {
					// error
				} else {
					mv.setViewName(url.replaceFirst("\\.jsp", ""));
				}
			}
		}
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			BaseExample baseExample = new BaseExample();
			baseExample.createCriteria().andEqualTo("SYSTEM_ID", system.getSysId()).andLike("code",
					"%" + StoreService.STYLE_CODE + "%");
			List<StoreVO> styles = storeService.selectPagedByExample(baseExample, 1, Integer.MAX_VALUE, null);
			mv.addObject("styles", styles);
		}
		mv.addObject("_ComponentTypeName", FormDesignGlobal.COMPOENT_TYPE_NAME);
		return mv;
	}

	/**
	 * 保存组件
	 * @param vo
	 * @param componentIds
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/save")
	public StoreVO save(StoreVO vo, @RequestParam(value = "componentIds", required = false) String componentIds) {
		vo.setContent(StringEscapeUtils.unescapeHtml4(vo.getContent()));
		if (componentIds == null || "".equals(componentIds)) {
			if (validate(vo)) {
				Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
				if (obj instanceof PunSystemVO) {
					PunSystemVO system = (PunSystemVO) obj;
					vo.setSystemId(system.getSysId());
				}
				vo.setCode(StoreService.COMPONENT_CODE + System.currentTimeMillis());
				JSONObject json = JSON.parseObject(vo.getContent()); // 将json中的order转为int类型（freeMarker
																		// sort_by('order')
																		// 必须）
				json.put("order", json.getInteger("order"));
				vo.setContent(json.toJSONString());
				String id = storeService.save(vo);// 保存Store并返回storeId
				vo.setId(id);
			}
		} else {
			String[] componentIds1 = componentIds.split(",");
			List<String> notAllowedKeys = new ArrayList<String>(
					Arrays.asList("name", "order", "layoutId", "layoutName"));
			HashMap<String,Object> temp = new HashMap<String,Object>();
			for (String componentId : componentIds1) {
				StoreVO voi = storeService.findById(componentId);
				JSONObject oi = JSONObject.parseObject(voi.getContent());
				JSONObject o = JSONObject.parseObject(vo.getContent());
				Iterator<String> it = o.keySet().iterator();
				boolean isChanged = false;
				while (it.hasNext()) {
					String key = it.next();
					if (!notAllowedKeys.contains(key)) {
						String valueString = o.getString(key);
						if ("\"\"".equals(valueString)) {
							oi.put(key, "");
							temp.put(key, "");
							isChanged = true;
						} else if (valueString != "" && !"".equals(valueString) && !valueString.equals(oi.get(key))) {
							oi.put(key, valueString);
							temp.put(key, valueString);
							isChanged = true;
						}
					}
				}
				if (isChanged) {
					voi.setContent(oi.toJSONString());
					storeService.save(voi);// 保存Store并返回storeId
				}
			}
		}
		return vo;
	}

	/**
	 * 根据组件ID获取组件详情
	 * @param storeId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getComponentById")
	public StoreVO getComponentById(@RequestParam(required = true) String storeId) {
		StoreVO store = storeService.findById(storeId);
		return store;
	}

	/**
	 * 重置序号
	 * @param pageId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/refreshComponentOrder")
	public String refreshLayoutOrder(@RequestParam(required = true) String pageId) {
		boolean flag = storeService.freshComponentOrder(pageId);
		if (flag) {
			return "1";
		} else {
			return "0";
		}
	}

	/**
	 * 根据页面ID获取组件
	 * @param dynamicPageId
	 * @param currentPage
	 * @param pageSize
	 * @param sortString
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getComponentListByPageId")
	public List<StoreVO> getComponentListByPageId(@RequestParam(required = true) String dynamicPageId,
			@RequestParam(required = false, defaultValue = "1") int currentPage,
			@RequestParam(required = false, defaultValue = "10") int pageSize,
			@RequestParam(required = false, defaultValue = " T_ORDER ASC") String sortString) {
		BaseExample baseExample = new BaseExample();
		baseExample.createCriteria().andEqualTo("DYNAMICPAGE_ID", dynamicPageId).andLike("CODE",
				StoreService.COMPONENT_CODE + "%");
		return storeService.selectPagedByExample(baseExample, currentPage, pageSize, sortString);
	}

	private boolean validate(StoreVO vo) {
		return true;
	}

	/**
	 * 异步ajax删除选中的页面动作数据,更改数据状态
	 * 
	 * @param selects
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "deleteByAjax")
	public String deleteComponentByAjax(@RequestParam(value = "_selects") String selects) {
		String[] ids = selects.split(",");
		if (storeService.delete(ids)) {
			return "1";
		} else {
			return "0";
		}
	}

	/**
	 * 批量拷贝组件
	 * @param _selects
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "copyComponentByAjax")
	public String copyComponentByAjax(@RequestParam(value = "_selects") String selects) {
		String[] ids = selects.split(",");
		for (int i = 0; i < ids.length; i++) {
			StoreVO s = storeService.findById(ids[i]);
			StoreVO copy = BeanUtils.getNewInstance(s, StoreVO.class);
			copy.setId(""); // 置Id为空，表示新增
			copy.setName(copy.getName() + "-copy");
			JSONObject copyJson = JSON.parseObject(copy.getContent());
			if (copyJson.containsKey("pageId")) { // 置pageId为空，表示新增
				copyJson.put("pageId", "");
			}
			copyJson.put("name", copy.getName());
			copy.setContent(copyJson.toJSONString());
			storeService.save(copy); // 保存
		}
		return "1";
	}

	/**
	 * 批量修改 数据源名称
	 * @param _selects
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "batchModifyDataAlias")
	public String batchModifyDataAlias(@RequestParam(value = "_selects") String selects, String dynamicPageId,String alias) {
		String[] ids = selects.split(",");
		for (int i = 0; i < ids.length; i++) {
			StoreVO s = storeService.findById(ids[i]);
			// 置Id为空，表示新增
			JSONObject json = JSON.parseObject(s.getContent());
			String dataItemCode = json.getString("dataItemCode");
			dataItemCode = dataItemCode.substring(dataItemCode.indexOf("."));
			dataItemCode = alias + dataItemCode;
			json.put("dataItemCode", dataItemCode);
			s.setContent(json.toJSONString());
			storeService.save(s); // 保存
		}
		return "1";
	}

	/**
	 * 批量修改 是否为空
	 * @param selects
	 * @param allowNull	1允许为空，0不允许为空
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "batchModifiAllowNull")
	public String batchModifiAllowNull(@RequestParam(value = "_selects") String selects, String allowNull) {
		String[] ids = selects.split(",");
		for (int i = 0; i < ids.length; i++) {
			StoreVO s = storeService.findById(ids[i]);
			JSONObject json = JSON.parseObject(s.getContent());
			json.put("required", allowNull);
			s.setContent(json.toJSONString());
			storeService.save(s);
		}
		return "1";
	}

	/**
	 * 快捷新增，根据数据源配置添加组件
	 * 
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/quickComByData")
	public String quickComByData(@RequestParam(value = "_dataSource") String dataSources,
			@RequestParam(value = "_labelTitle") String labelTitles,
			@RequestParam(value = "_layoutName") String layoutNames,
			@RequestParam(value = "_componentType") String componentTypes,
			@RequestParam(value = "_layoutId") String layoutIds,
			@RequestParam(value = "_labelLayoutId") String labelLayoutIds,
			@RequestParam(value = "_labelLayoutName") String labelLayoutNames,
			@RequestParam(value = "dynamicPageId") String dynamicPageId) {
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		Long systemId = null;
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			systemId = system.getSysId();
		}
		String[] dataSource = dataSources.split(",");
		String[] labelTitle = labelTitles.split(",");
		String[] layoutName = layoutNames.split(",");
		String[] componentType = componentTypes.split(",");
		String[] layoutId = layoutIds.split(",");
		String[] labelLayoutId = labelLayoutIds.split(",");
		String[] labelLayoutName = labelLayoutNames.split(",");
		for (int i = 0; i < dataSource.length; i++) {
			StoreVO s = new StoreVO();
			s.setSystemId(systemId);
			s.setCode(StoreService.COMPONENT_CODE + System.currentTimeMillis());
			s.setDynamicPageId(dynamicPageId);
			s.setName("quickData" + System.currentTimeMillis());
			s.setOrder(2); // 默认设为2
			JSONObject json = new JSONObject();
			json.put("dataItemCode", dataSource[i]); // 修改数据源
			json.put("componentType", componentType[i]);
			json.put("dynamicPageId", dynamicPageId);
			json.put("layoutId", layoutId[i]);
			json.put("layoutName", layoutName[i]);
			json.put("name", s.getName());
			json.put("pageId", "");
			json.put("order", s.getOrder());
			s.setContent(json.toJSONString());
			storeService.save(s); // 保存组件
			StoreVO labelStore = new StoreVO(); // 对应组件的文本组件
			labelStore.setSystemId(systemId);
			labelStore.setCode(StoreService.COMPONENT_CODE + System.currentTimeMillis());
			labelStore.setDynamicPageId(dynamicPageId);
			labelStore.setName("label_" + System.currentTimeMillis());
			labelStore.setOrder(1); // 默认设为1
			JSONObject labelJson = new JSONObject();
			labelJson.put("componentType", "1009");
			labelJson.put("dynamicPageId", dynamicPageId);
			labelJson.put("layoutId", labelLayoutId[i]); // 文本布局
			labelJson.put("layoutName", labelLayoutName[i]); // 文本布局名称
			labelJson.put("name", labelStore.getName());
			labelJson.put("title", labelTitle[i]);
			labelJson.put("pageId", "");
			labelJson.put("order", labelStore.getOrder());
			labelStore.setContent(labelJson.toJSONString());
			storeService.save(labelStore); // 保存相应的文本组件
		}
		return "1";
	}

	/**
	 * 校验页面是否签出
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/validateCheckOut")
	public String validateCheckOut(String id) {
		if (id == null) { // 新增页面，无需校验是否签出；
			return "1";
		}
		StoreVO vo = storeService.findById(id);
		if (vo.getIsCheckOut() != null && vo.getIsCheckOut() == 1) { // 已签出状态
			if (!vo.getCheckOutUser().equals(ControllerHelper.getUser().getName())) {
				String ret = "页面已被  " + vo.getCheckOutUser() + " 签出，你暂时无法修改";
				return ret;
			} else {
				return "1";
			}
		} else {
			String ret = "编辑修改前，请先签出";
			return ret;
		}
	}
}
