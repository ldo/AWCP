package cn.org.awcp.formdesigner.controller;

import static cn.org.awcp.venson.common.SC.DATA_SOURCE_COOKIE_KEY;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.org.awcp.common.db.DynamicDataSource;
import cn.org.awcp.common.util.CommonUtil;
import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.DateUtils;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;
import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.venson.util.BeanUtil;
import cn.org.awcp.venson.util.CookieUtil;

/**
 * 动态页面备份还原
 * @author yqtao
 *
 */
@RequestMapping(value="/fd")
@Controller
public class DynamicPageBackup {

	private static final String PAGE_SQL = "select * from p_fm_dynamicpage where id=?";
	private static final String STORE_SQL = "select * from p_fm_store where DYNAMICPAGE_ID=?";
	private static final String INSERT_BACKUP_INFO_SQL = "insert into p_fm_backup(ID,dynamicpageId,dynamicpageName,"
			+ "fileName,description,date) values(UUID(),?,?,?,?,?)";
		
	@Autowired
	private SzcloudJdbcTemplate jdbcTemplate;
	
	/**
	 * 获取所有备份过的动态页面
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getBackupPages")
	public List<Map<String,Object>> getBackupPages(){
		return jdbcTemplate.queryForList("select DISTINCT dynamicpageId,dynamicpageName from p_fm_backup");
	}
	
	/**
	 * 设置备份文件夹路径
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/setDirPath")
	public ReturnResult setDirPath(HttpServletRequest request){
		ReturnResult ret = new ReturnResult();
		String dirPath = request.getParameter("dirPath");
		File dir = new File(dirPath);
		if(!dir.exists()) {
			throw new PlatformException("没有找到该文件夹");
		}
		try {
			int count = jdbcTemplate.queryForObject("select count(*) from p_fm_backup_dir", Integer.class);
			String sql = "";
			if(count == 0) {
				sql = "insert into p_fm_backup_dir(path) values(?)";
			} else {
				sql = "update p_fm_backup_dir set path=?";
			}
			jdbcTemplate.update(sql, dirPath);
		} catch(Exception e) {
			throw new PlatformException("数据库执行错误。");
		}			
		return ret;
	}
	
	/**
	 * 获取备份文件夹路径
	 * @return
	 */
	@ResponseBody
  	@RequestMapping(value="/getBackupDir")
  	public Map<String,Object> getBackupDir() {
		Map<String,Object> ret = new HashMap<String,Object>();
  		String sql = "select path from p_fm_backup_dir";
  		String backupDir = "";
  		try {
  			backupDir = jdbcTemplate.queryForObject(sql, String.class);
  		} catch(Exception e) {
  			
  		}	
  		ret.put("backupDir", backupDir);
  		return ret;
  	}
	
	/**
	 * 获取页面备份数据
	 * @param currentPage
	 * @param pageSize
	 * @param dynamicpageId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getPageBackupList")
	public Map<String,Object> getPageBackupList(@RequestParam(required = false, defaultValue = "1") int currentPage,
            									@RequestParam(required = false, defaultValue = "10") int pageSize,
            									@RequestParam(required = false) String dynamicpageId){
		Map<String,Object> ret = new HashMap<String,Object>();
		int limit = pageSize;
		int offset = (currentPage - 1) * pageSize;
		String where = "where 1=1";
		if(StringUtils.isNotBlank(dynamicpageId)) {
			where += " and dynamicpageId='" + dynamicpageId + "'";
		}
		String sql = "select ID,dynamicpageId,dynamicpageName,fileName,description,date from p_fm_backup " + where ;
		int count = jdbcTemplate.queryForObject("select count(1) from (" + sql + ") t", Integer.class);			
		sql += " order by date desc limit " + offset + "," + limit;
		ret.put("rows", jdbcTemplate.queryForList(sql));
		ret.put("total", count);
		return ret;
	}
		
	/**
	 * 备份页面
	 * @param id	动态页面ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/backup")
	public ReturnResult backup(HttpServletRequest request){		
		ReturnResult ret = new ReturnResult();
		String id = request.getParameter("id");						//动态页面ID
		String description = request.getParameter("description");	//备份描述
		StringBuffer buffer = new StringBuffer();
		Map<String,Object> page = jdbcTemplate.queryForMap(PAGE_SQL,id);
        buffer.append(BeanUtil.getInsertSQL(page, "p_fm_dynamicpage"));                 
        List<Map<String, Object>> stores = jdbcTemplate.queryForList(STORE_SQL, id);
        for (Map<String, Object> store : stores) {
            buffer.append(BeanUtil.getInsertSQL(store, "p_fm_store"));
        }
           	
        String backupDir = getDir();		
        String dynamicpageName = (String) page.get("name");
        String thisPageDirPath = backupDir + "\\" + dynamicpageName;
        File dir = new File(thisPageDirPath);
        if(!dir.exists()) {
        	dir.mkdirs();
        }
       	String now = DateUtils.format(new Date(), "yyyy-MM-dd-HH-mm-ss"); 
       	String fileName = dynamicpageName + "_" + now + ".sql";
       	FileWriter out = null;
       	try {
       		out = new FileWriter(thisPageDirPath + "\\" + fileName);
       		out.write(buffer.toString());
       		jdbcTemplate.update(INSERT_BACKUP_INFO_SQL, id, dynamicpageName, fileName, description, new Date());
       		out.flush();
       	} catch(Exception e) {
       		e.printStackTrace();
       		throw new PlatformException("备份失败");
       	} finally {
       		IOUtils.closeQuietly(out);
       	}
		return ret;
	}
	
	/**
	 * 还原动态页面
	 * @param backupId	备份ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/restore")
	public ReturnResult restore(String backupId){
		ReturnResult ret = new ReturnResult();
		String sql = "select dynamicpageId,fileName,dynamicpageName from p_fm_backup where ID=?";
		Map<String,Object> map = jdbcTemplate.queryForMap(sql,backupId);
		String dynamicpageId = (String) map.get("dynamicpageId");
		String dynamicpageName = (String) map.get("dynamicpageName");
		String fileName = (String) map.get("fileName");
		String dir = getDir();
		File file = new File(dir + "\\" + dynamicpageName + "\\" + fileName);
		if(!file.exists()) {
			throw new PlatformException("没有找到备份文件");
		}
		BufferedReader in = null;
		try {
			jdbcTemplate.beginTranstaion();
			in = new BufferedReader(new FileReader(file));
			List<String> sqls = readFile(in);
			String[] sqlArr = new String[sqls.size()];	
			jdbcTemplate.update("delete from p_fm_store where DYNAMICPAGE_ID=?",dynamicpageId);
			jdbcTemplate.update("delete from p_fm_dynamicpage where id=?", dynamicpageId);
			jdbcTemplate.batchUpdate(sqls.toArray(sqlArr));		
			jdbcTemplate.commit();
		} catch(Exception e) {
			e.printStackTrace();
			jdbcTemplate.rollback();
			throw new PlatformException("还原动态页面出错");
		} finally {
			IOUtils.closeQuietly(in);
		}
		return ret;
	}
		
	//读取文件
    private List<String> readFile(BufferedReader in) throws IOException {
    	List<String> ret = new ArrayList<String>();	
    	String line = in.readLine();  		
		while(line != null) {
			ret.add(line);
			line = in.readLine();		
		}  
		in.close();
    	return ret;
    }
	
    //获取备份文件的文件夹
  	private String getDir() {
  		String sql = "select path from p_fm_backup_dir";
  		try {
  			String path = jdbcTemplate.queryForObject(sql, String.class);
  			if(!new File(path).exists()) {
  				throw new PlatformException("设置的备份文件夹不存在。");
  			}
  			String dataSourceName = CookieUtil.findCookie(DATA_SOURCE_COOKIE_KEY);
  			if(StringUtils.isBlank(dataSourceName)) {
  				dataSourceName = DynamicDataSource.MASTER_DATA_SOURCE;
  			}
  			String dir = path + "\\" + dataSourceName;  			
  			return dir;
  		} catch(Exception e) {
  			throw new PlatformException("没有设置备份文件夹路径，请到页面备份管理，设置路径。");
  		}		
  	}

  	/**
  	 * 删除备份
  	 * @param backupId	备份ID
  	 * @return
  	 */
  	@ResponseBody
  	@RequestMapping(value="/deleteBackup")
  	public ReturnResult deleteBackup(String backupIds) {
  		ReturnResult ret = new ReturnResult();  				
		try {
			String ids = CommonUtil.wrapQuot(backupIds, ",");		
			String sql = "select ID,fileName,dynamicpageName from p_fm_backup where ID in (" + ids + ")";
			List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
			StringBuffer msg = new StringBuffer();
			for(Map<String,Object> map : list) {
				String dynamicpageName = (String) map.get("dynamicpageName");
				String fileName = (String) map.get("fileName");
				String backupId = (String) map.get("ID");
				String dir = getDir();
				File file = new File(dir + "\\" + dynamicpageName + "\\" + fileName);
				if(file.exists()) {
					if(file.delete()) {
						jdbcTemplate.update("delete from p_fm_backup where ID=?", backupId);
					} else {
						msg.append("删除备份文件:" + fileName + "失败<br/>" );
					}
				} else {
					jdbcTemplate.update("delete from p_fm_backup where ID=?", backupId);
				}
			}
			if(msg.length() > 0) {
				ret.setStatus(StatusCode.FAIL).setMessage(msg.toString());
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new PlatformException("删除备份文件出错");
		}
		return ret;
  	}
}
