package cn.org.awcp.workflow.service;

import cn.org.awcp.venson.exception.PlatformException;

/**
 * 流程动作类型枚举类
 * @author venson
 * @version 20180301
 */
public enum FlowExecuteType {
	/**
     * 办结
     */
    SHOTDOWN(3000),
	
    /**
     * 撤销
     */
    UNDO(3001),
    
    /**
     * 拒绝
     */
    REJECT(3002),
    
    /**
     * 回撤
     */
    RETRACEMENT(3003),
    
    /**
     * 退回
     */
    RETURN(3004),
    
    /**
     * 发送
     */
    SEND(3005),
    
    /**
     * 跳转
     */
    SKIP(3006),
    
    /**
     * 传阅
     */
    AGREE(3007),
    
    /**
     * 加签
     */
    COUNTERSIGN(3008),
    
    /**
     * 保存
     */
    SAVE(3009);

    private int value;

    FlowExecuteType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static FlowExecuteType forValue(int value) {
        switch (value) {
	        case 3000:
	            return SHOTDOWN;
	        case 3001:
	            return UNDO;
	        case 3002:
	            return REJECT;
            case 3003:
                return RETRACEMENT;
            case 3004:
                return RETURN;
            case 3005:
                return SEND;
            case 3006:
                return SKIP;
            case 3007:
                return AGREE;
            case 3008:
                return COUNTERSIGN;
            case 3009:
                return SAVE;
            default:
                throw new PlatformException("未知的流程类型类型");
        }
    }
}
