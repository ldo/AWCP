package cn.org.awcp.metadesigner.controller;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.druid.pool.DruidDataSource;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.common.db.DynamicDataSource;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.Criteria;
import cn.org.awcp.core.utils.BeanUtils;
import cn.org.awcp.metadesigner.application.DataSourceManageService;
import cn.org.awcp.metadesigner.application.SysSourceRelationService;
import cn.org.awcp.metadesigner.vo.DataSourceManageVO;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;

/**
 * 数据源管理
 */
@Controller
@RequestMapping(value = "/dataSourceManage")
public class DataSourceManageController extends BaseController {

    @Autowired
    private DataSourceManageService dataSourceManageServiceImpl;
  
    @Autowired
    @Qualifier("sysSourceRelationServiceImpl")
    private SysSourceRelationService sysSourceRelationService;

    @Autowired
    private DynamicDataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    /**
     * 显示该单位下的所有数据源
     */
    @RequestMapping(value = "/selectPage")
    public String selectPagedByExample(@RequestParam(required = false, defaultValue = "1") int currentPage,
                                       @RequestParam(required = false, defaultValue = "10") int pageSize, 
                                       @ModelAttribute DataSourceManageVO vo, Model model) {
        currentPage = currentPage <= 0 ? 1 : currentPage;
        pageSize = pageSize <= 0 ? 10 : pageSize;
        BaseExample be = new BaseExample();
        Criteria c = be.createCriteria();
        String name = vo.getName();
        if (name != null && !"".equals(name)) {
            c.andLike("name", "%" + name + "%");
        }
        String url = vo.getSourceUrl();
        if (url != null && !"".equals(url)) {
            c.andLike("sourceUrl", "%" + url + "%");
        }
        PageList<DataSourceManageVO> pl = dataSourceManageServiceImpl.selectPagedByExample(be, currentPage, pageSize, "name desc");
        model.addAttribute("list", pl);
        model.addAttribute("currentPage", currentPage);
        return "metadesigner/dataSource/dataSource_list";
    }

    /**
     * 查询一条数据源
     * @param id
     * @return
     */
    @RequestMapping(value = "/get",method = RequestMethod.GET)
    public String get(String id, Model model) {
        if (StringUtils.isNotBlank(id)) {
            model.addAttribute("vo", this.dataSourceManageServiceImpl.get(id));
        }
        return "metadesigner/dataSource/dataSourceRemote_edit";
    }
    
    /**
     * 删除数据源
     * @param id
     * @return
     */
    @RequestMapping(value = "/remove")
    public String remove(String[] id) {
    	Arrays.stream(id).forEach((dataSourceId)->{
    		DataSourceManageVO dsmv = dataSourceManageServiceImpl.get(dataSourceId);
            dataSourceManageServiceImpl.delete(dsmv);
            if(dsmv!=null){
                dataSource.remove(dsmv.getName());
            }
    	});
        return "redirect:selectPage.do";
    }
    
    /**
     * 测试数据源是否有效
     *
     * @param vo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/testDataSource")
    public ReturnResult testDataSource(@ModelAttribute DataSourceManageVO vo) throws Exception {
        ReturnResult result = ReturnResult.get();
        Connection conn = null;
        try {
            Class.forName(vo.getSourceDriver());
            conn = DriverManager.getConnection(vo.getSourceUrl(), vo.getUserName(), vo.getUserPwd());
            result.setStatus(StatusCode.SUCCESS).setData(1);
        } catch (Exception e) {
            result.setStatus(StatusCode.SUCCESS).setData(0);
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }
    
    /**
     * 保存数据源
     * @param vo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public ReturnResult saveByAjax(DataSourceManageVO vo) {
        ReturnResult result = ReturnResult.get();
        vo.setSourceUrl(StringEscapeUtils.unescapeHtml4(vo.getSourceUrl()));
        if (StringUtils.isNotBlank(vo.getId())) {
            DataSourceManageVO dataSourceVo = dataSourceManageServiceImpl.get(vo.getId());
            BeanUtils.copyProperties(vo, dataSourceVo, true);
            dataSource.put(dataSourceVo.getName(), getDruidDataSource(dataSourceVo));
            dataSourceManageServiceImpl.update(dataSourceVo);
            result.setStatus(StatusCode.SUCCESS).setData(1);
        } else {
            if (dataSourceManageServiceImpl.queryDataSourceByName(vo.getName()) == null) {
                vo.setDomain("remote");
                vo.setCreateTime(new Date(System.currentTimeMillis()));
                vo.setLastModifyTime(vo.getCreateTime());
                vo.setLastModifier(ControllerHelper.getUserId().toString());
                vo.setMaximumActiveTime(60000);
                vo.setSimultaneousBuildThrottle(6);
                dataSourceManageServiceImpl.save(vo);
                dataSource.put(vo.getName(), getDruidDataSource(vo));
                result.setStatus(StatusCode.SUCCESS).setData(1);
            } else {
                result.setStatus(StatusCode.SUCCESS).setData(0);
            }
        }
        return result;
    }
    
    private DruidDataSource getDruidDataSource(@ModelAttribute DataSourceManageVO vo) {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUrl(vo.getSourceUrl());
        druidDataSource.setDriverClassName(vo.getSourceDriver());
        druidDataSource.setUsername(vo.getUserName());
        druidDataSource.setPassword(vo.getUserPwd());
        druidDataSource.setInitialSize(vo.getPrototypeCount());
        druidDataSource.setMaxActive(vo.getMaximumConnectionCount());
        druidDataSource.setMinIdle(vo.getMinimumConnectionCount());
        return druidDataSource;
    }
    
    /**
     * 根据用户输入的数据库连接信息，获取该数据库的所有表名，并返回给用户，同时应该保存一份用户输入的数据库连接信息
     *
     * @return
     */
    @RequestMapping(value = "/listTables")
    public ModelAndView listTables() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("metadesigner/dataSource/dataSource_tmp_metamodel_listTables");
        try {
            Connection conn = jdbcTemplate.getDataSource().getConnection();
            DatabaseMetaData metaData = conn.getMetaData();
            ResultSet tableRet = metaData.getTables(null, "%", "%", new String[] { "TABLE" });
            List<String> tableNames = new ArrayList<>();
            while (tableRet.next()) {
                tableNames.add(tableRet.getString("TABLE_NAME"));
            }
            mv.addObject("tables", tableNames);
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        return mv;
    }
    
}
