package cn.org.awcp.unit.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.Criteria;
import cn.org.awcp.core.utils.Security;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.metadesigner.application.MetaModelOperateService;
import cn.org.awcp.unit.service.PunPositionService;
import cn.org.awcp.unit.service.PunRoleInfoService;
import cn.org.awcp.unit.service.PunUserBaseInfoService;
import cn.org.awcp.unit.service.PunUserRoleService;
import cn.org.awcp.unit.utils.EncryptUtils;
import cn.org.awcp.unit.vo.PunAJAXStatusVO;
import cn.org.awcp.unit.vo.PunPositionVO;
import cn.org.awcp.unit.vo.PunRoleInfoVO;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.unit.vo.PunUserBaseInfoVO;
import cn.org.awcp.unit.vo.PunUserRoleVO;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户管理Controller
 */
@Controller
@RequestMapping("/unit")
@Api(value = "用户接口")
public class PunUserBaseInfoController extends BaseController {

	@Autowired
	@Qualifier("punUserBaseInfoServiceImpl")
	private PunUserBaseInfoService userService;// 用户Service

	@Autowired
	@Qualifier("punUserRoleServiceImpl")
	private PunUserRoleService userRoleService;// 用户角色关联Service

	@Autowired
	@Qualifier("punRoleInfoServiceImpl")
	private PunRoleInfoService roleService;// 角色Service

	@Resource(name = "punPositionServiceImpl")
	private PunPositionService punPositionService;// 职位Service
	
	@Autowired
	private MetaModelOperateService metaModelOperateServiceImpl;// 元数据操作Service

	/**
	 * 进入新增用户界面
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/intoPunUserBaseInfo", method = RequestMethod.GET)
	public ModelAndView intoPunUserBaseInfo(Model model) {
		try {
			List<PunRoleInfoVO> roleVos = queryRoles();					//获取当前系统的角色
			List<PunPositionVO> posiVos = punPositionService.findAll();	//获取当前系统的职位
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("roleVos", roleVos);
			params.put("posiVos", posiVos);
			return new ModelAndView("/unit/punUserBaseInfo-edit", params);
		} catch (Exception e) {
			logger.info("ERROR", e);
			return new ModelAndView("redirect:/logout.do");
		}
	}

	//获取当前系统角色
	private List<PunRoleInfoVO> queryRoles() {
		PunSystemVO vo = (PunSystemVO) SessionUtils.getObjectFromSession(SessionContants.CURRENT_SYSTEM);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sysId", vo.getSysId());
		List<PunRoleInfoVO> roleVos = roleService.queryResult("eqQueryList", params);
		return roleVos;
	}
	
	/**
	 * 保存
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/punUserBaseInfoSave", method = RequestMethod.POST)
	public ReturnResult punUserBaseInfoSave(@ModelAttribute("vo") PunUserBaseInfoVO vo) {
		ReturnResult result=ReturnResult.get();
		try {
			if(StringUtils.isBlank(vo.getUserIdCardNumber())) {
				vo.setUserIdCardNumber(vo.getUserName());
			}
			String msg = validate(vo);
			if (msg == null) {
				// 密码加密
				vo.setUserPwd(EncryptUtils.encrypt(vo.getUserPwd()));
				boolean isNew = vo.getUserId() == null ? true : false;
				String userHeadImg = StringUtils.defaultString(vo.getUserHeadImg(),
						ControllerHelper.getBasePath() + "template/AdminLTE/images/user2-160x160.jpg");
				vo.setUserHeadImg(userHeadImg);
				//更新用户时，不允许修改用户账号
				if(!isNew){
					vo.setUserIdCardNumber(null);
				}
				Long userId = userService.addOrUpdateUser(vo);
				// 新增用户时，增加部门和岗位
				if (vo.getPositionGroupId() != null && vo.getPositionId() != null) {
					String insertSql = "insert into p_un_user_group(USER_ID,GROUP_ID,POSITION_ID) values(?,?,?)";
					if (isNew) {
						metaModelOperateServiceImpl.updateBySql(insertSql, userId, vo.getPositionGroupId(),
								vo.getPositionId());
					} else {
						String sql = "delete FROM p_un_user_group WHERE user_id=?";
						metaModelOperateServiceImpl.updateBySql(sql, userId);
						metaModelOperateServiceImpl.updateBySql(insertSql, userId, vo.getPositionGroupId(),
								vo.getPositionId());
					}
				}
				result.setStatus(StatusCode.SUCCESS);
			} else {
				result.setStatus(StatusCode.FAIL).setMessage("校验失败" + msg + "。");
			}
		} catch (Exception e) {
			logger.info("ERROR", e);
			result.setStatus(StatusCode.FAIL).setMessage("系统异常");
		}
		return result;
	}

	/**
	 * 修改用户状态
	 * @param userId		用户ID
	 * @param userStatus	用户状态，1：启用；2：禁用
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateUserStatus")
	public ReturnResult updateUserStatus(Long userId,String userStatus) {
		ReturnResult ret = ReturnResult.get();
		String sql = "update p_un_user_base_info set USER_STATUS=? where USER_ID=?";
		int count = metaModelOperateServiceImpl.updateBySql(sql, userStatus, userId);
		ret.setData(count);
		return ret;
	}
	
	/**
	 * 编辑用户，根据ID查找
	 * @param boxs
	 * @return
	 */
	@RequestMapping(value = "punUserBaseInfoGet", method = RequestMethod.POST)
	public ModelAndView punUserBaseInfoGet(Long boxs) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/unit/punUserBaseInfo-edit");
		try {
			PunUserBaseInfoVO vo = userService.findById(boxs);
			List<PunRoleInfoVO> roleVos = queryRoles();// 获取当前系统角色
			// 根据userId获取用户角色
			List<String> selectedRole = roleService.queryByUser(vo.getUserId());
			String sql = "SELECT group_id groupId,position_id positionId FROM p_un_user_group WHERE user_id=?";
			List<Map<String, Object>> data = metaModelOperateServiceImpl.search(sql, boxs);
			Object groupId = "";
			Object positionId = "";
			if (!data.isEmpty()) {
				Map<String, Object> map = data.get(0);
				groupId = map.get("groupId");
				positionId = map.get("positionId");
			}
			List<PunPositionVO> posiVos = punPositionService.findAll();
			vo.setUserPwd(Security.decryptPassword(vo.getUserPwd()));
			mv.addObject("vo", vo);
			mv.addObject("selectedRole", StringUtils.join(selectedRole.toArray(new String[] {}),","));
			mv.addObject("selectedGroup", groupId);
			mv.addObject("selectedPosition", positionId);
			mv.addObject("roleVos", roleVos);
			mv.addObject("posiVos", posiVos);

		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "异常");
		}
		return mv;
	}

	/**
	 * 用户列表
	 * 
	 * @param vo
	 *            查询参数
	 * @param model
	 * @param currentPage
	 *            当前页数
	 * @param pageSize
	 *            每页记录条数
	 * @return
	 */
	@RequestMapping(value = "punUserBaseInfoList")
	public ModelAndView punUserBaseInfoList(@ModelAttribute PunUserBaseInfoVO vo, Model model,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		try {
			String sortString = "p_un_user_base_info.USER_STATUS,p_un_user_base_info.USER_ID";
			BaseExample example = new BaseExample();
			Criteria criteria = example.createCriteria();
			String query_deptName = vo.getDeptName();// 部门名称
			String query_name = vo.getName();// 姓名
			String query_mobile = vo.getMobile();// 手机号
			String query_userTitle = vo.getUserTitle();// 职称
			if (StringUtils.isNotBlank(query_deptName)) {
				criteria.andLike("p_un_group.GROUP_CH_NAME", "%" + query_deptName + "%");
			}
			if (StringUtils.isNotBlank(query_name)) {
				criteria.andLike("p_un_user_base_info.NAME", "%" + query_name + "%");
			}
			if (StringUtils.isNotBlank(query_mobile)) {
				criteria.andLike("p_un_user_base_info.MOBILE", "%" + query_mobile + "%");
			}
			if (StringUtils.isNotBlank(query_userTitle)) {
				criteria.andLike("d.NAME", "%" + query_userTitle + "%");
			}
			PageList<PunUserBaseInfoVO> vos = userService.selectByExample_UserList(example, currentPage, pageSize,sortString);
			model.addAttribute("currentPage", currentPage);
			model.addAttribute("vos", vos);
			model.addAttribute("vo", vo);
			return new ModelAndView("/unit/punUserBaseInfo-list");
		} catch (Exception e) {
			logger.info("ERROR", e);
			model.addAttribute("result", "系统异常");
			return new ModelAndView("/unit/punUserBaseInfo-list");
		}
	}

	/**
	 * 删除用户，同时删除用户组，用户角色
	 * @param boxs
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "punUserBaseInfoDelete", method = RequestMethod.POST)
	public ReturnResult punUserBaseInfoDelete(Long[] boxs) {
		ReturnResult ret = ReturnResult.get();
		Arrays.stream(boxs).forEach((userId) -> {
			userService.delete(userId);
		});
		return ret;
	}
	
	/**
	 * 查询与角色已关联的用户
	 * 
	 * @param vo
	 * @param roleId
	 *            角色ID
	 * @param currentPage
	 *            当前页
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "roleRelateUserQuery")
	public ModelAndView roleRelateUserQuery(PunUserBaseInfoVO vo,
			@RequestParam(value = "roleId", required = false) Long roleId,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		ModelAndView mv = new ModelAndView("unit/user-relate-role");
		try {
			String sortString = "USER_ID.desc";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("roleId", roleId);
			if (null != vo) {
				if (StringUtils.isNotEmpty(vo.getUserIdCardNumber())) {// 身份证号
					params.put("userIdCardNumber", "%" + vo.getUserIdCardNumber() + "%");
				}
				if (StringUtils.isNotEmpty(vo.getName())) {// 姓名
					params.put("name", "%" + vo.getName() + "%");
				}
			}
			List<PunUserBaseInfoVO> userVOs = userService.queryPagedResult("queryUserRelateRole", params, currentPage,
					pageSize, sortString);
			mv.addObject("roleId", roleId);
			mv.addObject("vos", userVOs);
			mv.addObject("currentPage", currentPage);
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "系统异常");
		}
		return mv;
	}

	/**
	 * 查询与角色未关联的用户
	 * 
	 * @param vo
	 * @param roleId
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "roleNotRelateUserQuery")
	public ModelAndView roleNotRelateUserQuery(PunUserBaseInfoVO vo, Long roleId,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		ModelAndView mv = new ModelAndView("unit/user-not-relate-role");
		try {
			String sortString = "USER_ID.desc";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("roleId", roleId);
			if (null != vo) {
				if (StringUtils.isNotEmpty(vo.getUserIdCardNumber())) {// 身份证号
					params.put("userIdCardNumber", "%" + vo.getUserIdCardNumber() + "%");
				}
				if (StringUtils.isNotEmpty(vo.getName())) {// 姓名
					params.put("name", "%" + vo.getName() + "%");
				}
				if (vo.getGroupId() == null) {
					PunUserBaseInfoVO currentUser = (PunUserBaseInfoVO) SessionUtils
							.getObjectFromSession(SessionContants.CURRENT_USER);
					vo.setGroupId(currentUser.getGroupId());
					params.put("groupId", vo.getGroupId());
				} else {
					params.put("groupId", vo.getGroupId());
				}
			}
			List<PunUserBaseInfoVO> userVOs = userService.queryPagedResult("queryRoleNotRealteUser", params,
					currentPage, pageSize, sortString);
			mv.addObject("roleId", roleId);
			mv.addObject("vos", userVOs);
			mv.addObject("currentPage", currentPage);
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "系统异常");
		}
		return mv;
	}

	/**
	 * 用户关联角色
	 * 
	 * @param roleId
	 *            角色ID
	 * @param boxs
	 *            用户ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "userRelateRole", method = RequestMethod.POST)
	public PunAJAXStatusVO userRelateRole(Long roleId, Long[] boxs) {
		PunAJAXStatusVO vo = new PunAJAXStatusVO();
		try {
			for (Long box : boxs) {
				PunUserRoleVO userRole = new PunUserRoleVO();
				userRole.setRoleId(roleId);
				userRole.setUserId(box);
				userRoleService.addOrUpdate(userRole);
			}
			vo.setMessage("完成" + boxs.length + "个用户的关联操作");
		} catch (Exception e) {
			logger.info("ERROR", e);
			vo.setMessage("关联操作失败");
		}
		return vo;
	}

	/**
	 * 取消角色与用户关联关系
	 * 
	 * @param roleId 角色Id
	 * @param boxs 用户Id
	 * @return
	 */
	@RequestMapping(value = "cancelUserRoleRelation", method = RequestMethod.POST)
	public ModelAndView cancelUserRoleRelation(Long roleId, Long[] boxs) {
		ModelAndView mv = new ModelAndView("redirect:roleRelateUserQuery.do?&roleId=" + roleId);
		try {
			userRoleService.deleteByRoleIdAndUserId(roleId, boxs);
			mv.addObject("result", "删除成功");
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "删除失败");
		}
		return mv;
	}

	//校验必填项
	private String validate(PunUserBaseInfoVO vo) {
		// 新增
		if (null == vo.getUserId()) {
			int count = metaModelOperateServiceImpl.queryOne("select count(*) from p_un_user_base_info "
					+ "where USER_NAME=? or USER_ID_CARD_NUMBER=?",vo.getUserName(),vo.getUserIdCardNumber());
			if(count != 0) {
				return "该用户名已注册过";
			}
			count = metaModelOperateServiceImpl.queryOne("select count(*) from p_un_user_base_info where MOBILE=?",vo.getMobile());
			if(count != 0) {
				return "该手机已注册过";
			}
		} else {
			int count = metaModelOperateServiceImpl.queryOne("select count(*) from p_un_user_base_info "
					+ "where USER_ID<>? and USER_NAME=?",vo.getUserId(),vo.getUserName());
			if(count != 0) {
				return "该用户名已注册过";
			}
			count = metaModelOperateServiceImpl.queryOne("select count(*) from p_un_user_base_info "
					+ "where USER_ID<>? and MOBILE=?",vo.getUserId(),vo.getMobile());
			if(count != 0) {
				return "该手机已注册过";
			}
		}	
		return null;
	}

	/**
	 * 修改密码
	 * 
	 * @param oldPwd
	 *            旧密码
	 * @param newPwd
	 *            新密码
	 */
	@ResponseBody
	@RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
	@ApiOperation(value = "修改密码")
	public ReturnResult updatePwd(String oldPwd, String newPwd) {
		ReturnResult result = ReturnResult.get();
		try {
			PunUserBaseInfoVO user = (PunUserBaseInfoVO) SessionUtils
					.getObjectFromSession(SessionContants.CURRENT_USER);
			if (!oldPwd.equals(EncryptUtils.decript(user.getUserPwd()))) {
				result.setStatus(StatusCode.PARAMETER_ERROR).setMessage("原密码输入错误");
			} else {
				user.setUserPwd(EncryptUtils.encrypt(newPwd));
				userService.updateUser(user);
				Session session = SessionUtils.getCurrentSession();
				session.setAttribute(SessionContants.CURRENT_USER,user);
				result.setStatus(StatusCode.SUCCESS).setMessage("修改成功");
			}
		} catch (Exception e) {
			logger.info("ERROR", e);
			result.setStatus(StatusCode.FAIL).setMessage("修改失败，请重试或联系管理员");
		}
		return result;
	}

	/**
	 * 用户选择框回显用户名字
	 */
	@ResponseBody
	@RequestMapping(value = "getNamesByUserIds", method = RequestMethod.POST)
	public String getNamesByUserIds(String userIds) {
		if (StringUtils.isBlank(userIds)) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		String[] userIdArr = userIds.split(",");
		if (userIdArr != null && userIdArr.length > 0) {
			for (int i = 0; i < userIdArr.length; i++) {
				if ("".equals(userIdArr[i])) {
					continue;
				} else {
					PunUserBaseInfoVO user = userService.findById(Long.parseLong(userIdArr[i]));
					if (user != null) {
						sb.append(user.getName() + ",");
					}
				}
			}
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.lastIndexOf(","));
		}
		return sb.toString();
	}

}
