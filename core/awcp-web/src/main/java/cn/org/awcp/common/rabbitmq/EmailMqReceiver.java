package cn.org.awcp.common.rabbitmq;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;

import cn.org.awcp.venson.util.EmailUtil;

/**
 * 发送email消息队列消费者
 * @author yqtao
 *
 */
@Component
public class EmailMqReceiver implements ChannelAwareMessageListener{

	@Override
	public void onMessage(Message message, Channel channel) throws Exception {
		try{
			String json = new String(message.getBody());
			JSONObject jsonObj = JSON.parseObject(json);
			EmailUtil.sendEmail(EmailUtil.EMailType._QQ, 
					jsonObj.getString("email"), jsonObj.getString("subject"), jsonObj.getString("content"));
			channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        }catch(Exception e){
        	channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true); 
            e.printStackTrace();
        }
	}
	
}