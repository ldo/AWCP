package cn.org.awcp.formdesigner.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.pool.DruidDataSource;

import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.Springfactory;

/**
 * 同一个mysql,将一个数据库同步到其他库
 * @author yqtao
 *
 */
@RequestMapping(value="asynDatabase")
@Controller
public class AsynDatabaseController {

	@Autowired
	private SzcloudJdbcTemplate jdbcTemplate;
	
	/**
	 * 同步
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/asyn")
	public Map<String,Object> asyn(HttpServletRequest request){
		Map<String,Object> ret = new HashMap<String,Object>();
		String message = "";
		String mainDataBaseName = request.getParameter("mainDataBaseName");
		String[] databaseNames = request.getParameterValues("databaseNames");
		if(StringUtils.isNotBlank(mainDataBaseName) && databaseNames!=null) {
			//更新动态页面，组件，数据源，Api			
			String[] tables = request.getParameterValues("tables");
			if(tables != null) {
				if(updateMain(databaseNames, mainDataBaseName, tables)) {
					message += "更新动态页面，组件，数据源，Api成功<br/>";
				} else {
					message += "更新动态页面，组件，数据源，Api失败<br/>";
				}
			} else {
				message += "不需要更新动态页面，组件，数据源，Api<br/>";
			}
			
			//更新打印
			String isUpdatePrint = request.getParameter("isUpdatePrint");
			if("Y".equals(isUpdatePrint)) {
				if(updatePrint(databaseNames, mainDataBaseName)) {
					message += "更新打印配置成功<br/>";
				} else {
					message += "更新打印配置失败<br/>";
				}
			} else {
				message += "不需要更新打印配置<br/>";
			}
			//更新菜单
			String isUpdateMenu	= request.getParameter("isUpdateMenu");
			if("Y".equals(isUpdateMenu)) {
				if(updateMenu(databaseNames, mainDataBaseName)) {
					message += "更新菜单配置成功";
				} else {
					message += "更新菜单配置失败";
				}
			} else {
				message += "不需要更新菜单配置";
			}
		}
		ret.put("message", message);
		return ret;
	}
	
	//更新主要表
	private boolean updateMain(String[] databaseNames,String mainDataBaseName,String[] tables) {	
        try {
        	jdbcTemplate.beginTranstaion();
        	for (String databaseName : databaseNames) {
        		for(String table : tables) {
                	jdbcTemplate.execute("delete from " + databaseName + "." + table);
                	jdbcTemplate.execute("insert into " + databaseName + "." + table + 
                			" select * from " + mainDataBaseName + "." + table);
                }
			}    	
        	jdbcTemplate.commit();
        	return true;
        } catch(Exception e) {
        	e.printStackTrace();
        	jdbcTemplate.rollback();
        	return false;
        }   
	}
		
	//更新打印
	private boolean updatePrint(String[] databaseNames,String mainDataBaseName) {	
		try {
        	jdbcTemplate.beginTranstaion();
        	String sql = "select rsTemplateID,rsSubTemplateID from " + mainDataBaseName + ".p_fm_repservice";
        	String tempIds = getFileIds(jdbcTemplate.queryForList(sql));
        	for(String databaseName : databaseNames) {
        		sql = "select rsTemplateID,rsSubTemplateID from " + databaseName + ".p_fm_repservice";
            	String removeIds = getFileIds(jdbcTemplate.queryForList(sql));
            	jdbcTemplate.update("delete from " + databaseName + ".p_fm_repservice");
            	jdbcTemplate.update("insert into " + databaseName + ".p_fm_repservice "
            			+ "select * from " + mainDataBaseName + ".p_fm_repservice");
            	jdbcTemplate.update("delete from " + databaseName + ".p_fm_attachment where ID in (" + removeIds + ")");
            	jdbcTemplate.update("insert into " + databaseName + ".p_fm_attachment "
            			+ "select * from " + mainDataBaseName + ".p_fm_attachment where ID in (" + tempIds + ")");
        	}
        	jdbcTemplate.commit();
        	return true;
        } catch(Exception e) {
        	e.printStackTrace();
        	jdbcTemplate.rollback();
        	return false;
        }   
	}
	
	private String getFileIds(List<Map<String,Object>> list){
		StringBuffer fileIds = new StringBuffer();
		Set<String> set = new HashSet<String>();
		for(Map<String,Object> map : list) {
			String rsTemplateID = (String) map.get("rsTemplateID");
			set.add(rsTemplateID);
			String rsSubTemplateID = (String) map.get("rsSubTemplateID");
			if(StringUtils.isNotBlank(rsSubTemplateID)) {
				for(String subId : rsSubTemplateID.split(";")) {
					if(StringUtils.isNotBlank(subId)) {
						set.add(subId);
					}
				}
			}
		}
		for(String fileId : set) {
			fileIds.append("'").append(fileId).append("',");
		}
		if(fileIds.length()>1) {
			fileIds.deleteCharAt(fileIds.length()-1);
		}
		return fileIds.toString();
	}
	
	//更新菜单
	private boolean updateMenu(String[] databaseNames,String mainDataBaseName) {
		try {
        	jdbcTemplate.beginTranstaion();
        	for(String databaseName : databaseNames) {
        		jdbcTemplate.execute("delete from " + databaseName + ".p_un_resource");
        		jdbcTemplate.execute("delete from " + databaseName + ".p_un_menu");
        		jdbcTemplate.execute("delete from " + databaseName + ".p_un_role_access");
        		jdbcTemplate.execute("insert into " + databaseName + ".p_un_resource "
        				+ "select * from " + mainDataBaseName + ".p_un_resource");
        		jdbcTemplate.execute("insert into " + databaseName + ".p_un_menu "
        				+ "select * from " + mainDataBaseName + ".p_un_menu");   
        		jdbcTemplate.execute("insert into " + databaseName + ".p_un_role_access "
        				+ "select * from " + mainDataBaseName + ".p_un_role_access");  
        	}
        	jdbcTemplate.commit();
        	return true;
        } catch(Exception e) {
        	e.printStackTrace();
        	jdbcTemplate.rollback();
        	return false;
        } 
	}

	/**
	 * 获取所有数据库
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getDataBases")
	public List<String> getDataBases(){
		List<String> ret = new ArrayList<String>();
		String sql = "select SOURCE_URL from fw_mm_datasourcemanager order by name";
		List<String> urls = jdbcTemplate.queryForList(sql,String.class);
		DruidDataSource dataSource = Springfactory.getBean("masterDataSource");
		String baseUrl = dataSource.getUrl();
		String[] base = getUrlAndDatabase(baseUrl);
		for(String url : urls) {
			String[] temp = getUrlAndDatabase(url);
			if(base[0].equals(temp[0])) {
				ret.add(temp[1]);
			}
		}
		return ret;
	}
	
	private String[] getUrlAndDatabase(String url) {
		int firstIndex = url.indexOf("/");
		int lastIndex = url.indexOf("?");
		String str = url.substring(firstIndex+2,lastIndex);
		return str.split("/");
	}
	
}
