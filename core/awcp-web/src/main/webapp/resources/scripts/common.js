var myFun = {
	//根据行内某个值来显示和隐藏表格行按钮
	showAndHideTableButton : function(index,value,okBtnIndex,noBtnIndex){
		$("#bootstrapTable").children("tbody").find("tr").each(function(){
			var $row = $(this);
			var okBtn = $row.find("button").eq(okBtnIndex);
			var noBtn = $row.find("button").eq(noBtnIndex);
			var txt = $.trim($row.find("td").eq(index).text());
			if(value == txt){
				okBtn.show();
				noBtn.hide();
			} else{
				okBtn.hide();
				noBtn.show();
			}
		});
	},
	
	//检测bootstrap Table选中的记录数为1,table的ID为：bootstrapTable
	checkSelectOne : function(){
		var count = $("#bootstrapTable tr.selected").length;
		if(count == 1){	
			return true;
		} else{
			Comm.alert("请选择一项进行操作");
			return false;
		}
	},
	
	//检测bootstrap Table选中的记录数不为0,table的ID为：bootstrapTable
	checkSelectAtLeastOne : function(){
		var count = $("#bootstrapTable tr.selected").length;
		if(count > 0){	
			return true;
		} else{
			Comm.alert("请至少选择一项进行操作");
			return false;
		}
	},
	
	//主要用于ajax执行一些操作
	ajaxExecute : function(url, params, async){
		params = (params) ? params : {};
		async = async ? true : false;
		var retData;
		$.ajax({
	        type: "POST",
	        url: baseUrl + url,
	        data: params,
	        async: async,
	        dataType: "json",
	        success: function (data) {
	        	retData = data;          
	        },
	        error: function () {
	        	Comm.alert("网络出错，请联系管理员！", "error");
	            throw new Error("网络出错，请联系管理员！");
	        }
	    });
		return retData;
	},
	
	/*
	 * 生成下拉框的options;	
	 * data			数组
	 * valueKey		value键		
	 * nameKey		name键		
	 * needBlank	是否需要空的option
	 */
	generateOptions : function(data,valueKey,nameKey,needBlank){
		var options = "";
		if(data.length > 0){
			if(needBlank){
				options += "<option></option>";
			}
			for(var i=0;i<data.length;i++){
				var obj = data[i];
				options += "<option value='" + obj[valueKey] + "'>" + obj[nameKey] + "</option>";
			}
		}
		return options;
	}
};

/**
 * 关闭当前tab页面
 * @returns
 */
function closePage(){
    parent.closeTabByPageId(parent.$("#tab-menu").find("a.active").attr("data-pageid"));
}

/**
 * 列表页面进行搜索
 * @returns
 */
function search(url,params){
	if(url && params){
		localStorage.setItem(url, JSON.stringify(params));
	}
	$("#groupForm").submit();
}

function toListPage(url,baseUrl){
	var params = null;
	if(baseUrl){
		params = localStorage.getItem(baseUrl);
 	} else{
 		params = localStorage.getItem(url);
 	}
	if(params){
		var arr = JSON.parse(params);
		for(var i=0;i<arr.length;i++){
			if(i==0){
				if(url.indexOf("?") != -1){
					url += "&" + arr[i];
				} else{
					url += "?" + arr[i];
				}
			} else{
				url += "&" + arr[i];
			}
		}
	}
	location.href = url;
}

function initFormValidator(){
	var $form = $("#groupForm");
	$form.bootstrapValidator({
		excluded: [":disabled", ":hidden", ":not(:visible)"]
	});
}

function validateForm(){
	var $form = $("#groupForm");
	var validator = $form.data('bootstrapValidator');
	if(!validator.isValid()){	
		validator.resetForm();
	} 
	validator.validate();
	return validator.isValid();
}

function gotoUrl(obj){
	var params = "";
	for(key in obj){
		params += key + "=" + obj[key] + "&";
	}
	location.href = basePath + "document/view.do?" + params;
}

/**
 * 重置列表页面搜索条件,然后搜索
 * @returns
 */
function resetView(){
	$("#groupForm")[0].reset();
	Comm.clearSearch();
	$("#groupForm").submit();
}

function updateAction(params){
    var str = "";
    var bool = true;
    for(var i=0;i<params.length;i++){
        var key = params[i];
        var val = Comm.getUrlParam(key);
        if(val){
            if(bool){
                str += "?" + key + "=" + val;
                bool = false;
            } else{
                str += "&" + key + "=" + val;
            }
        }
    }
    var action = $("#groupForm").attr("action") + str;
    $("#groupForm").attr("action",action);
}

function updateParamsToGroupForm(params){
    var $groupForm = $("#groupForm");

    for(var i=0;i<params.length;i++){
        var key = params[i];
        var val = Comm.getUrlParam(key);

        if(val){
            var $elem = $groupForm.find("input[name='"+key+"']");

            if($elem.size() == 0){
                var newInputStr = '<input type="hidden" name="'+ key +'" value="' + val + '" id="' +key+ '"/>';
                $groupForm.prepend(newInputStr);
            }
            else{
                $elem.val(val);
            }
        }
    }
}

function newFormPage(dynamicPageId,actId, suffixParams){
    if(!suffixParams){ suffixParams = ''; }
    $("#"+actId).attr("disabled","true");
    location.href = basePath + "document/view.do?dynamicPageId=" + dynamicPageId + suffixParams;
}

/**
 * 表单页面保存后跳转到对应的列表页面
 * @param dynamicPageId		列表页面的动态页面ID
 * @param actId				保存按钮的actID
 * @param suffixParams		其他参数
 * @returns
 */
function saveFormPage(dynamicPageId,actId,suffixParams){
    $("#buttons").find("button").attr("disabled",true);	//禁用所有按钮,防止提交数据时同时进行其他操作
    $("#actId").val(actId);
    if(!suffixParams){ 
    	suffixParams = "";
    }
    $.ajax({
        type: "POST",
        url: basePath + "document/excuteOnly.do",
        data: $("#groupForm").serialize(),
        async: false,
        success: function(data){
            if(data==1){
                Comm.alert("保存成功",function(){
                    location.href = basePath + "document/view.do?dynamicPageId=" + dynamicPageId + suffixParams;
                });
            } else{
            	if(data.message){
            		Comm.alert(data.message,function(){
                        $("#buttons").find("button").attr("disabled",false);
                    });
            	} else{
            		Comm.alert("保存失败",function(){
                        $("#buttons").find("button").attr("disabled",false);
                    });
            	}               
            }
        }
    });
}

/**
 * 弹窗的表单页面保存后关闭该页面
 * @param actId	动作ID
 * @returns
 */
function savePageThenClose(actId){
	$("#buttons").find("button").attr("disabled",true);
	$("#actId").val(actId);
	$.ajax({
	    type: "POST",
	    url: basePath + "document/excuteOnly.do",
	    data:$("#groupForm").serialize(),
	    async : false,
	    success: function(data){
			if(data==1){
				Comm.alert("保存成功",function(){
					top.dialog({id:window.name}).close();
				});
			} else{
				if(data.message){
            		Comm.alert(data.message,function(){
                        $("#buttons").find("button").attr("disabled",false);
                    });
            	} else{
            		Comm.alert("保存失败",function(){
                        $("#buttons").find("button").attr("disabled",false);
                    });
            	}     			
			}
	    }
	});
}

function excuteNormalAct(dynamicPageId,actId){
    var count = $("input[name='_selects']").length;
    if(count != 1){
        Comm.alert("请至选择一项进行操作");
        return false;
    }
    $("#actId").val(actId);
    $.ajax({
        type: "POST",
        url: basePath + "document/excuteOnly.do",
        data:$("#groupForm").serialize(),
        async : false,
        success: function(data){
            if(data==1){
                Comm.alert("执行成功",function(){
                    location.href = basePath + "document/view.do?dynamicPageId=" + dynamicPageId;
                });
            } else{
                Comm.alert(data);
            }
        }
    });
}

function backToViewPage(dynamicPageId,suffixParams){
    if(!suffixParams){ suffixParams = '';}
    location.href = basePath + "document/view.do?dynamicPageId=" + dynamicPageId + suffixParams;
}

function goBack(dynamicPageId){
    if(dynamicPageId){
        location.href = basePath + "document/view.do?dynamicPageId=" + dynamicPageId;
    } else{
        var menuName = parent.$("#tab-menu").find("a.active").text();
        if(menuName=="在办件"){
			location.href = basePath + "venson/inDoingTasks.html";
        } else if(menuName=="待办件"){
			location.href = basePath + "venson/listPersonalTasks.html";
        } else if(menuName=="办结件"){
			location.href = basePath + "venson/listHistoryTasks.html";
        } else{
            history.back();
        }
    }
}



/**
 * 打开新的tab页面
 * @param url	链接地址
 * @param title	页面标题
 * @returns
 */
function openPage(url,title){
    parent.addTabs({
        url: url,
        title: title,
        close:true
    });
}

function editFormPage(dynamicPageId, suffixParams){
    if(!suffixParams){ suffixParams = '';}
    var count = $("input[name='_selects']").length;
    if(count!=1){
        Comm.alert("请选择一项进行操作");
        return false;
    }
    var id = $("input[name='_selects']").val();
    location.href = basePath + "document/view.do?dynamicPageId=" + dynamicPageId + "&id=" + id + suffixParams;
}

function deleteRecord(actId){
    var count = $("input[name='_selects']").length;
    if(count==0){
        Comm.alert("请至少选择一项进行操作");
        return false;
    }
    dialogConfirm("是否确认删除?",function(){
        $("#actId").val(actId);
        $.ajax({
            type: "POST",
            url: basePath + "document/excuteOnly.do",
            data:$("#groupForm").serialize(),
            async : false,
            success: function(data){
                if(data==1){
                    Comm.alert("删除成功",function(){
                        $("#groupForm").submit();
                    });
                } else{
                    Comm.alert("删除失败");
                }
            }
        });
    });
}

function addModal(e){
    var title = e.title?e.title:'提示';
    var url = e.url?e.url:'';
    var content = e.content?e.content:'';
    top.dialog({
        title: title,
        url:url,
        skin:"col-md-4",
        content:content,
        okValue: '确定',
        ok: function () {
            this.title('提交中…');
            return false;
        },
        cancelValue: '取消',
        cancel: function () {}
    }).width(600).showModal();
};

function alertMessage(message){
    $.messager.show(message, {placement: 'top',type:'info',icon:'info-sign'});
};

function dialogAlert(data){
    var d = top.dialog({
        id:'Info' + Comm.uuid(),
        title: '操作提示',
        skin:"col-md-4",
        height:'auto',
        content: data,
        cancelValue: 'Cancel',
        cancel: function () {
            return false;
        },
        cancel: false,
        button: [
            {
                value: 'Close',
            }
        ]
    });
    d.show();
}

function dialogConfirm(msg,okFn,cancelFn){
    var d = top.dialog({
        id:'Confirm' + Comm.uuid(),
        title: '确认提示框',
        skin:"col-md-4",
        height:'auto',
        content: msg,
        cancelValue: 'Cancel',
        cancel: function () {
            return false;
        },
        cancel: false,
        button: [
            {
                value: '确认',
                callback: function (){
                    this.close("Y");
                    this.remove();
                },
            },
            {
                value: '取消',
                callback: function () {
                    this.close("N");
                    this.remove();
                },
            }
        ]
    });
    d.show();
    d.addEventListener('close', function () {
        if(this.returnValue=="Y"){
        	if(okFn){
        		okFn();
        	}         
        } else{
        	if(cancelFn){
        		cancelFn();
        	} 
            return false;
        }
    });
}

//当失去焦点时，去除输入框前后空格
function clearBlank(){
    $("input[type='text']").blur(function(){
        $(this).val($(this).val().replace(/^\s+|\s+$/g,""));
    });
}

$(function(){
    //当失去焦点时，去除输入框前后空格
    clearBlank();
});

function formatNumber(num){
    var bool = false;
    if(num.indexOf(".")!=-1){
        bool = true;
    }
    var arr = num.split(".");
    var before = "";
    var after = "";
    if(arr.length==2){
        before = arr[0];
        after = arr[1];
    }
    else{
        before = arr[0];
    }
    var beforeArrReverse = before.split("").reverse();
    var temp = "";
    for(var i = 0; i < beforeArrReverse.length; i ++ )   {
        temp += beforeArrReverse[i] + ((i + 1) % 3 == 0 && (i + 1) != beforeArrReverse.length ? "," : "");
    }
    var result = "";
    if(after!=""){
        result = temp.split("").reverse().join("") + "." + after;
    }
    else{
        result = temp.split("").reverse().join("") + (bool?".":"");
    }
    return result;
}

function checkIsNumber(obj){
    var value = $.trim($(obj).val());
    value = value.replace(/,/g,'');
    if(value && !$.isNumeric(value)){
        $(obj).focus();
        dialogAlert("请输入一个数字.");
        return false;
    }
    else{
        return true;
    }
}

//定义弹窗的高度和宽度
var dialogHeight = $(top).height()-93;
var dialogWidth = 1024;