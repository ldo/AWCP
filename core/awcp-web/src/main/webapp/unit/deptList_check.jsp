<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>组织用户</title>
	 <%@ include file="/resources/include/common_lte_css.jsp" %>
	 <link rel="stylesheet" href="<%=basePath%>resources/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css">
</head>
<body id="main">					
	<section class="content" >
		<div class="row">
	        <div class="col-md-12">
	            <div class="box box-info" >
	                <div class="box-body">
	                	<input type="hidden" name="userIds" id="userIds"/>
	        			<input type="hidden" name="oldGroupId" id="oldGroupId"/>
				       	<ul id="tree1" class="ztree"></ul>
	                </div>
	            </div>
	        </div>
		</div>
	</section>	
	<%@ include file="../resources/include/common_lte_js.jsp"%>
	<script type="text/javascript" src="<%=basePath%>resources/plugins/zTree_v3/js/jquery.ztree.all-3.5.js"></script>
    <script type="text/javascript">
   		$(function(){
   			var data = null;
   			try {
				var dialog = top.dialog.get(window);
				data = dialog.data;
				if(data.userIds!=null){
					$("#userIds").val(data.userIds);
				}
				if(data.oldGroupId!=null){
					$("#oldGroupId").val(data.oldGroupId);
				}
			} catch (e) {
				return;
			}
			
			var setting = {
	   			view: {
	   				showLine: false,
	   				showIcon: false
	   			},
	   			data: {
	   				simpleData: {
	   					enable: true
	   				}
	   			},
	   			callback:{
        			onClick:selectItem       		
    			}
	   		};
			
			var tNodes1 = "";
	        $(function(){
	        	$.ajax({
	            	type:"post",
	                url:basePath + "queryDeptTreeData.do?type=simple",
	                data:"",
	                async: false,
	                datatype:"json",
	                success:function(json){
	                  	tNodes1=json;
	                }
	            });
		   		$.fn.zTree.init($("#tree1"), setting, tNodes1);
	        });	
   		})
		
   		function selectItem(event, treeId, treeNode) {
            save(treeNode.id);
        } 
        
        function save(groupId){
           	try {
				var dialog = top.dialog.get(window);
			} catch (e) {
				return;
			}		
           	var userIds = $("#userIds").val();
           	var oldGroupId = $("#oldGroupId").val();
           	if(userIds!=""&&groupId!=""){
           		$.ajax({
                	type:"post",
                	url:basePath + "punUserGroupController/userGroupEdit.do?",
                	data:{
                		oldGroupId:oldGroupId,
                		userIds:userIds,
                		groupId:groupId
                	},
                	async: false,
                	datatype:"json",
                	success:function(data){
                		dialog.close();
                	}
            	});
            }
        }
	</script>        
</body>
</html>
