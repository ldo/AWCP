<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>系统菜单编辑</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
	<link rel="stylesheet" href="<%=basePath %>resources/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css">
	<link rel="stylesheet" href="<%=basePath %>template/AdminLTE/css/artDialog/dialog.css" />
	<style>
		ul,ul li {list-style:none;padding:0;margin:0;}
		ul.CloudTreeMenu li .ztree {display: block;}
		.input-group{margin-bottom:10px;}
	</style>
</head>
<body>
	<div class="whole" style="margin:20px;">
		<div class="C_btns">
			<button class="btn btn-default" type="button" onclick="closePage()"><i class="fa fa-close"></i> 关闭</button>
		</div>
		<div style="margin-top:20px;">
	      	<ul class="CloudTreeMenu list-group"></ul>    
		</div>
		<div class="C_btns">
			<button class="btn btn-info" type="button" onclick="addNode()"><i class="fa fa-plus"></i> 新增根节点</button>
		</div>
	</div>
	<script src="<%=basePath %>resources/JqEdition/jquery-1.10.2.js"></script>
	<script src="<%=basePath %>resources/plugins/zTree_v3/js/jquery.ztree.all-3.5.js"></script>
	<script src="<%=basePath %>template/AdminLTE/js/artDialog/dialog-plus.js" charset="utf-8"></script>
	<script>
		function closePage(){
			parent.closeTabByPageId(parent.$("#tab-menu").find("a.active").attr("data-pageid"));
		}
	
		var setting = {
			view: {
				showLine: true,
				showIcon: showIconForTree,
				addHoverDom: addHoverDom,
				removeHoverDom: removeHoverDom,
				selectedMulti: false
			},
			edit: {
				drag:{
					isCopy:false,
					isMove:false
				},
				enable: true,
				editNameSelectAll: true,
				showRemoveBtn: showRemoveBtn,
				showRenameBtn: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				onRemove: zTreeOnRemove,
				beforeClick: zTreeBeforeClick
			}
		};	

		function zTreeOnRemove(event,treeId,treeNode){
			$.ajax({
			   	type: "POST",
			   	url: "<%=basePath%>dev/punMenuAJAXDelete.do",
			   	data: "menuId="+treeNode.id,
			   	success: function(data){
				    if(data.status==0){
				    	var zTree = $.fn.zTree.getZTreeObj(treeId);
				    	zTree.updateNode(treeNode);
				    	if(!treeNode.pId){
				    		$("#"+treeId).parent().remove();
				    	}
				    	alertMessage("删除成功");
				    }else{
				    	alertMessage(data.message);
				    }
			   	}
			});	
		}
		
		function showIconForTree(treeId, treeNode) {
			return !treeNode.isParent;
		}
		
		function showRemoveBtn(treeId, treeNode) {
			return !treeNode.isParent;
		}
		
		function zTreeOnClick(event, treeId, treeNode,clickFlag) {
		
		}
		
		function zTreeBeforeClick(treeId, treeNode, clickFlag) {
		    return false;
		}
		
		var newCount = 1;
		function addHoverDom(treeId, treeNode) {
			var sObj = $("#" + treeNode.tId + "_span");
			if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
			var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='新增菜单' onfocus='this.blur();'></span>"
				+ "<span class='button edit' id='" + treeNode.tId + "_edit"
				+ "' title='修改菜单' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var addBtn = $("#addBtn_"+treeNode.tId);
			var editBtn = $("#"+treeNode.tId+"_edit");
			if(addBtn) addBtn.bind("click",function(){
				top.dialog({ 
					id: 'add-dialog' + Math.ceil(Math.random()*10000),
					title: '新增菜单',
					url:'<%=basePath%>dev/menuEditDialog.do?parentMenuId='+treeNode.id,
					height:400,
					width:1000,
					onclose : function() {
						if (this.returnValue) {
							var result = this.returnValue;
							var zTree = $.fn.zTree.getZTreeObj(treeId);
							zTree.addNodes(treeNode, {id:result.menuId, pId:treeNode.id, name:result.menuName,url:result.menuAddress});
					    	alertMessage("添加成功");
						}
					}
				}).showModal();
				return false;
			});
			if (editBtn) editBtn.bind("click", function(){
				top.dialog({ id: 'edit-dialog' + Math.ceil(Math.random()*10000),
					title: '修改菜单',
					url:'<%=basePath%>dev/punMenuGet.do?boxs='+treeNode.id,
					height:400,
					width:1000,
					onclose : function() {
						if (this.returnValue) {
							var result = this.returnValue;
							var zTree = $.fn.zTree.getZTreeObj(treeId);
							treeNode.name = this.returnValue.menuName;
							treeNode.url = this.returnValue.menuAddress;
					    	zTree.updateNode(treeNode);
							alertMessage("保存成功");
						}
					}
				}).showModal();
			});
		}

		function removeHoverDom(treeId, treeNode) {
			$("#addBtn_"+treeNode.tId).unbind().remove();
			$("#"+treeNode.tId+"_edit").unbind().remove();
		}
		
		function creatMenu(){
			var dataLi="";
			var dataMap = ${menuJson};
			$.each(dataMap,function(i,item){
				dataLi ='<li class="list-group-item"><ul id="tree'+i+'" class="ztree"></ul></li>';
				$(dataLi).appendTo(".CloudTreeMenu");
				$.fn.zTree.init($("#tree"+i),setting,item);
			});
		}
		
		function addNode(){
			top.dialog({ 
				id: 'add-root-dialog' + Math.ceil(Math.random()*10000),
				title: '新增根节点',
				url:'<%=basePath%>dev/menuEditDialog.do?parentMenuId='+0,
				height:450,
				width:1000,
				onclose : function() {
					if (this.returnValue) {
						var i = $(".list-group-item").length+1;
						var dataLi ='<li class="list-group-item"><ul id="tree'+i+'" class="ztree"></ul></li>';
						var result = this.returnValue;
						$(dataLi).appendTo(".CloudTreeMenu");
				    	var item = {"id":result.menuId,"pId":0,"name":result.menuName,"url":result.menuAddress,"target":"main","open":false,"isParent":false};
						$.fn.zTree.init($("#tree"+i),setting,item);
				    	alertMessage("添加成功");
					}
				}
			}).showModal();
			return false;
		}
		
		function alertMessage(message){
			var md = dialog({
			    content: message
			});
			md.show();
			setTimeout(function () {
			    md.close().remove();
			}, 2000);
		}
		
		$(document).ready(function(){
			creatMenu();
		});
	</script>
</body>
</html>