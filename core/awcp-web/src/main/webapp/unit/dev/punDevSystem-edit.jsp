<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>系统编辑页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<div class="content" style="background-color: white;">
		<div class="opeBtnGrop">
			<button type="button" class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
			<button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-remove"></i> 关闭</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form id="groupForm" class="form-horizontal form-condensed bv-form">
						<input type="hidden" name="sysId" value="${vo.sysId}" /> 
						<div class="box-body">
							<div class="form-group">  
								<div class="customGroup">     	
						            <div class="col-md-6">
						            	<label class="control-label required">名称：</label>
						                <input name="sysName" class="form-control" id="sysName" type="text" value="${vo.sysName}" />
						            </div>
					            </div>
					            <div class="customGroup">
						            <div class="col-md-6">
						            	<label class="control-label">简称：</label>
						            	<input name="sysShortName" class="form-control" id="sysShortName" 
						            		type="text" value="${vo.sysShortName}" />
						            </div>	
					            </div>		         
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script type="text/javascript">	
		var basePath = "<%=basePath%>";
		$('document').ready(function(){
			$("#closeBtn").on("click",function(){
				closePage();
			});
			
			initFormValidator();
			
			$("#groupForm").bootstrapValidator('addField', 'sysName',{validators:{
				notEmpty: {
			        message: "名称不能为空"
			    }
			}});
			
			$("#saveBtn").on("click",function(){
				if(validateForm()){
					var data = myFun.ajaxExecute("dev/saveSystem.do",$("#groupForm").serialize());
					if(data.status=="0"){
						Comm.alert("保存成功");
					} else{
						Comm.alert("保存失败");
					}
				}
				return false;
			});
		});
	</script>
</body>
</html>
