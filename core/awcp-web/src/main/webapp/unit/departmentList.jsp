<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>组织用户</title>
	 <%@ include file="/resources/include/common_lte_css.jsp" %>
	 <link rel="stylesheet" href="<%=basePath%>resources/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css">
</head>
<body id="main">					
	<section class="content" >
		<div class="row">
	        <div class="col-md-12">
	            <div class="box box-info" >
	                <div class="box-body">
	                	<div class="col-md-2">
				          	<ul id="tree1" class="ztree" style="height: 400px;"></ul>
				       	</div>   
				        <div class="col-md-10">
				          	<iframe src="<%=basePath%>punUserGroupController/getUsers.do" name="sysEditFrame" id="sysEditFrame" 
				          		scrolling="auto" frameborder="0" style="height:400px;" width="100%"></iframe>		
				       	</div>
	                </div>
	            </div>
	        </div>
		</div>
	</section>	
	<%@ include file="../resources/include/common_lte_js.jsp"%>
	<script type="text/javascript" src="<%=basePath%>resources/plugins/zTree_v3/js/jquery.ztree.all-3.5.js"></script>
    <script type="text/javascript">
    	var setting = {
			view: {
				showLine: false,
				showIcon: false
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};
    
        var tNodes1 = "";
        $(function(){       	
        	$.ajax({
                type:"post",
                url:basePath + "queryDeptTreeData.do?type=manage",
                data:"",
                async: false,
                datatype:"json",
                success:function(json){          
                	tNodes1=json;//先将date转化为json                  
                }
            });
	   		$.fn.zTree.init($("#tree1"), setting, tNodes1);   		
        });		
   </script>
</body>
</html>
