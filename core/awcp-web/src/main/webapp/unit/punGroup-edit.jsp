<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="utf-8">
	<title>组信息编辑</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body>
	<section class="content">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:;">组织信息编辑</a></li>
			<li><a href="<%=basePath%>unit/punGroupEdit.do?boxs=${vo.groupId}">组织树编辑</a></li>
			<li><a href="<%=basePath%>punPositionController/pageList.do?groupId=${vo.groupId}">职务</a></li>
		</ul>
		<div style="margin-top:20px;"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<div class="form-group"><!-- 表单提交按钮区域 -->
				            <div class="ccol-md-12">
				              	<a class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</a>
				            </div>
				        </div>
						<form class="form-horizontal" id="groupForm" method="post">
							<input type="hidden" name="groupId" value="${vo.groupId}"/>
							<div class="form-group">
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label required">组名</label>
						                <input name="groupChName" class="form-control" id="groupChName" 
						                	type="text" value="${vo.groupChName}" />
						            </div> 
								</div>
								<div class="customGroup">	
						            <div class="col-md-4">
						            	<label class="control-label">组简称</label>
						                <input name="groupShortName" class="form-control" id="groupShortName" 
						                	type="text" value="${vo.groupShortName}" />
						            </div> 
								</div>
								<div class="customGroup">	
						            <div class="col-md-4">
						            	<label class="control-label">组名（英文）</label>
						                <input name="groupEnName" class="form-control" id="groupEnName" 
						                	type="text" value="${vo.groupEnName}" />
						            </div>
								</div>
							</div>
							<div class="form-group">   
								<div class="customGroup">
						            <div class="col-md-4">
						            	<label class="control-label">组织机构代码</label>
						                <input name="orgCode" class="form-control" id="orgCode" type="text" value="${vo.orgCode}" />
						            </div> 
								</div>
								<div class="customGroup">
						            <div class="col-md-4">
						            	<label class="control-label">传真</label>
						                <input name="fax" class="form-control" id="fax" type="text" value="${vo.fax}" />
						            </div> 
								</div>
								<div class="customGroup">
						            <div class="col-md-4">
						            	<label class="control-label">地址</label>
						                <input name="groupAddress" class="form-control" id="groupAddress" 
						                	type="text" value="${vo.groupAddress}" />
						            </div> 
								</div>
							</div>
							<div class="form-group">   
								<div class="customGroup">
						            <div class="col-md-4">
						            	<label class="control-label">邮编</label>
						                <input name="zipCode" class="form-control" id="zipCode" 
						                	type="text" value="${vo.zipCode}" />
						            </div> 
								</div>
								<div class="customGroup">
						            <div class="col-md-4">
						            	<label class="control-label">联系电话</label>
						                <input name="contactNumber" class="form-control" id="contactNumber" 
						                	type="text" value="${vo.contactNumber}" />
						            </div>
								</div>
								<div class="customGroup">
						            <div class="col-md-4">
						            	<label class="control-label">业务范围</label>
						                <input name="groupBusinessSphere" class="form-control" id="groupBusinessSphere" 
						                	type="text" placeholder="" value="${vo.groupBusinessSphere}" />
						            </div> 
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="../resources/include/common_lte_js.jsp"%>	
	<script type="text/javascript">
		$(function() {
			var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
                    "groupChName": {
                        validators: {
                        	notEmpty: {
                                message: "组名不能为空"
                            }
                        }                    
        			}           
        	    }
        	});
			
		    $("#saveBtn").on("click",function(){
		    	if(!validateForm()){
	    			return false;
	    		}
		    	var data = myFun.ajaxExecute("unit/punGroupSave.do",$("#groupForm").serialize());
        		if(data.status == "0"){
        			Comm.alert("保存成功");
        		} else{
        			if(data.message){
        				Comm.alert(data.message);
        			} else{
        				Comm.alert("保存失败");
        			}
        		}
		    	
            });
        });
	</script>
</body>
</html>
