<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>角色管理</title>
    <%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body>
	<section class="content">
	    <div class="opeBtnGrop">
	        <a href="#" class="btn btn-primary" id="addBtn"><i class="fa fa-plus"></i> 新增</a>
	        <a href="#" class="btn btn-success" id="accessBtn"><i class="fa fa-server"></i> 授权</a>
	        <a href="#" class="btn btn-info" id="relateUserBtn"><i class="fa fa-gear"></i> 关联用户</a>
	        <a href="#" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</a>
	        <a href="#" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</a>
	    </div>
	    <div class="row">
	        <div class="col-md-12">
	            <div class="box box-info">
	                <div class="box-body">
	                    <form method="post" id="groupForm" action="<%=basePath %>unit/listRolesInSys.do">
	                        <input type="hidden" id="currentPage" name="currentPage"
	                               value="${vos.getPaginator().getPage()}">
	                        <input type="hidden" id="pageSize" name="pageSize" value="${vos.getPaginator().getLimit()}">
	                        <input type="hidden" id="totalCount" name="totalCount"
	                               value="${vos.getPaginator().getTotalCount()}">
	                        <div class="row form-group">
	                            <div class="col-md-4">
	                                <div class="input-group">
	                                    <span class="input-group-addon">角色名</span>
	                                    <input name="roleName" class="form-control" id="roleName" type="text" value="${roleName}" />
	                                    <span class="input-group-btn">
					                      	<button class="btn btn-info" id="searchBtn"><i class="fa fa-search"></i> 搜索</button>
					                    </span>
	                                </div>
	                            </div>
	                        </div>
	                        <table class="table table-hover">
	                            <thead>
	                            <tr>
	                                <th class="hidden"></th>
	                                <th data-width="" data-field="" data-checkbox="true"></th>
	                                <th>名称</th>
	                                <th>备注</th>
	                            </tr>
	                            </thead>
	                            <tbody>
	                            <c:forEach items="${vos}" var="vo">
	                                <tr>
	                                    <td class="hidden"><input type="hidden" value="${vo.roleId}"></td>
	                                    <td></td>
	                                    <td><a href="<%=basePath%>unit/editRoleInSys.do?id=${vo.roleId}">${vo.roleName}</a></td>
	                                    <td>${vo.dictRemark}</td>
	                                </tr>
	                            </c:forEach>
	                            </tbody>
	                        </table>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<%@ include file="../resources/include/common_lte_js.jsp" %>
	<script type="text/javascript">
	    $(function () {
	        var count = 0;//默认选择行数为0
	        $(".table").bootstrapTable({
	            pageSize: parseInt($("#pageSize").val()),
	            pageNumber: parseInt($("#currentPage").val()),
	            totalRows: parseInt($("#totalCount").val()),
	            sidePagination: "server",
	            pagination: true,
	            onPageChange: function (number, size) {
	                $("#pageSize").val(size);
	                $("#currentPage").val(number);
	                customSearch();
	            },
	            onClickRow: function (row, $element, field) {
	                var $checkbox = $element.find(":checkbox").eq(0);
	                if ($checkbox.get(0).checked) {
	                    $checkbox.get(0).checked = false;
	                    $element.find("input[type='hidden']").removeAttr("name", "boxs");
	                } else {
	                    $checkbox.get(0).checked = true;
	                    $element.find("input[type='hidden']").attr("name", "boxs");
	                }
	                count = $("input[name='boxs']").length;
	            },
	            onCheck: function (row, $element) {
	                $element.closest("tr").find("input[type='hidden']").attr("name", "boxs");
	                count = $("input[name='boxs']").length;
	            },
	            onUncheck: function (row, $element) {
	                $element.closest("tr").find("input[type='hidden']").removeAttr("name");
	                count = $("input[name='boxs']").length;
	            }
	        });
	        
	        function customSearch(){
	        	var arr = ["pageSize=" + $("#pageSize").val(), 
	        		"currentPage=" + $("#currentPage").val(), "roleName=" + $("#roleName").val()]
	        	search(basePath + "unit/listRolesInSys.do",arr);
	        }
	        
	        //关闭
	        $("#closeBtn").on("click",function(){
	        	closePage();
	        });
	        
	        $("#searchBtn").on("click",function(){
	        	customSearch();
	        });
	        
	        $("#roleName").keyup(function(event) {
		    	if (event.keyCode == 13) {
		    		customSearch();
		    	}
		    });
	        
	        //新增
	        $("#addBtn").click(function () {
	            location.href = basePath + "unit/intoPunRoleInfo.do";
	            return false;
	        });
		
	        //删除
	        $("#deleteBtn").click(function () {
	            if (count < 1) {
	                Comm.alert("请至少选择一项进行操作");
	                return false;
	            }
	            Comm.confirm("确定删除？",function(){
	            	var data = myFun.ajaxExecute("api/unit/delRoleInSys",$("#groupForm").serialize());
	            	if(data.status == "0"){
	            		$("#groupForm").submit();
	        		} else{
	        			if(data.message){
	        				Comm.alert(data.message);
	        			} else{
	        				Comm.alert("删除失败");
	        			}
	        		}
	            });
	            return false;
	        });
	
	        //授权
	        $("#accessBtn").click(function () {
	            if (count == 1) {
	                $("#groupForm").attr("action", basePath + "unit/punRoleMenuAccessEdit.do").submit();
	            } else {
	                Comm.alert("请选择一项进行操作");
	            }
	            return false;
	        });
	        
	        //关联用户
	        $("#relateUserBtn").click(function () {
	            if (count == 1) {
	                location.href = basePath + "unit/roleRelateUserQuery.do?roleId="+ $("input[name='boxs']").val();
	            } else {
	                Comm.alert("请选择一项进行操作");
	            }
	            return false;
	        });
	    })
	</script>
</body>
</html>
