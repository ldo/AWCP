<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <title>用户详细信息编辑</title>
    <%@ include file="/resources/include/common_lte_css.jsp" %>
    <link rel="stylesheet" href="<%=basePath%>resources/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css">
    <style type="text/css">
        .checkbox-inline {
            display: inline-block;
            min-width: 18%;
            padding-left: 0px;
            margin-bottom: 0;
            font-weight: 400;
            vertical-align: middle;
            cursor: pointer;
            text-align: left;
            float: left;
        }
        .checkbox-inline input[type="checkbox"]{
            position:inherit;
            margin-left:0;
            margin-right:5px;
        }
        .ztree{
            padding-left:0;
        }
        .ztree li span.button.switch{
            width:0;
        }
        .radiodiv label:first-child{
        	margin-left:10px;
        }
    </style>
</head>
<body>
	<section class="content">
	    <div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="undoBtn"></i><i class="fa fa-reply"></i> 取消</button>
    	</div>
    	<form class="form-horizontal" id="groupForm" method="post">
		    <div class="row">
		    	<div class="col-md-12">
		    		<div class="box box-info">
		    			<div class="box-header with-border">
							<h3 class="box-title">用户主要信息</h3>
						</div>
		    			<div class="box-body">
				            <input type="hidden" name="userId" value="${vo.userId}" />
				            <input type="hidden" name="userIdCardNumber" value="${vo.userIdCardNumber}" />
				            <input type="hidden" name="groupId" value="${vo.groupId}" />
				            <input type="hidden" name="orgCode" value="${vo.orgCode}" />
				            <input type="hidden" id="updatePassword" name="updatePassword" value="yes"/>
				            <div class="form-group">
				            	<div class="customGroup">	
					                <div class="col-md-3">
					                	<label class="control-label required">用户名</label>
					                    <input name="userName" class="form-control" id="userName" 
					                    	type="text" value="${vo.userName}" />
					                </div>
				            	</div>
				            	<div class="customGroup">	
					                <div class="col-md-3">
					                	<label class="control-label required">姓名</label>
					                    <input name="name" class="form-control" id="name" type="text" value="${vo.name}" />
					                </div>
				            	</div>
				            	<div class="customGroup">	
					                <div class="col-md-3">
					                	<label class="control-label required">密码</label>
					                    <input name="userPwd" class="form-control password_userPwd" id="userPwd" 
					                    	type="text" value="${vo.userPwd}" />
					                </div>
				            	</div>
				            	<div class="customGroup">	
					                <div class="col-md-3">
					                	<label class="control-label required">移动电话</label>
					                    <input name="mobile" class="form-control" id="mobile" 
					                    	type="text" value="${vo.mobile}" />
					                </div>
				            	</div>
				            </div>
				            <div class="form-group">
				            	<div class="customGroup">
					                <label class="col-md-1 col-sm-1 control-label">角色</label>
					                <div class="col-md-11 col-sm-11 radiodiv">
					                    <c:forEach items="${roleVos}" var="vo">
					                        <c:choose>
					                            <c:when test="${(fn:contains(selectedRole,vo.roleId)&&vo.roleId!=1)}">
					                                <label class="checkbox-inline">
					                                    <input name="roleList" type="checkbox" 
					                                    	value="${vo.roleId}" checked="checked">${vo.roleName}
					                                </label>
					                            </c:when>
					                            <c:otherwise>
					                                <label class="checkbox-inline">
					                                    <input name="roleList" type="checkbox" value="${vo.roleId}">${vo.roleName}
					                                </label>
					                            </c:otherwise>
					                        </c:choose>
					                    </c:forEach>
					                </div>
				                </div>
				            </div>
				            <div class="form-group">
				            	<div class="customGroup">	
					                <label class="col-md-1 col-sm-1 control-label">职务</label>
					                <div class="col-md-11 col-sm-11 radiodiv">
					                    <c:forEach items="${posiVos}" var="vo">
					                        <c:choose>
					                            <c:when test="${selectedPosition==vo.positionId}">
					                                <label class="checkbox-inline">
					                                    <input name="positionId"  type="radio" 
					                                    	value="${vo.positionId}" checked="checked">${vo.name}
					                                </label>
					                            </c:when>
					                            <c:otherwise>
					                                <label class="checkbox-inline">
					                                    <input name="positionId" type="radio" value="${vo.positionId}"> ${vo.name}
					                                </label>
					                            </c:otherwise>
					                        </c:choose>
					                    </c:forEach>
					                </div>
				                </div>
				            </div>
				            <div class="form-group">
				            	<div class="customGroup">	
					                <label class="col-md-1 col-sm-1 control-label">组织</label>
					                <div class="col-md-11 col-sm-11 radiodiv" style="padding-left: 54px;">
					                    <input type="hidden" value="${selectedGroup}" id="positionGroupId" name="positionGroupId"/>
					                    <div id="groups" class="ztree"></div>
					                </div>
				                </div>
				            </div>				           
		   				</div>
		   			</div>
		   		</div>
		   	</div>		   	
		   	<div class="row">
		    	<div class="col-md-12">
		    		<div class="box box-info">
		    			<div class="box-header with-border">
							<h3 class="box-title">用户其他信息</h3>
						</div>
		    			<div class="box-body">
		    				<div class="form-group">
				            	<div class="customGroup">	
					                <div class="col-md-3">
					                	<label class="control-label">用户状态</label>
					                	<select name="userStatus" class="form-control" id="userStatus" value="${vo.userStatus}">
					                		<option value="1">正常</option>
					                		<option value="2">禁用</option>
					                	</select>
					                </div>
				            	</div>
				            	<div class="customGroup">	
					            	<div class="col-md-3">
					            	<label class="control-label">户籍地</label>
					                    <input name="userHouseholdRegist" class="form-control" id="userHouseholdRegist" 
					                    	type="text" value="${vo.userHouseholdRegist}" />
					                </div>	
				            	</div>
				                <div class="customGroup">	
					                <div class="col-md-3">
					                	<label class="control-label">居住地</label>
					                    <input name="userDomicile" class="form-control" id="userDomicile" 
					                    	type="text" value="${vo.userDomicile}" />
					                </div>
				                </div>
				                <div class="customGroup">	
					            	<div class="col-md-3">
					            		<label class="control-label">办公电话</label>
					                    <input name="userOfficePhone" class="form-control" id="userOfficePhone" 
					                    	type="text" value="${vo.userOfficePhone}" />
					                </div>
				                </div>
				            </div>
				            <div class="form-group">
				            	<div class="customGroup">
					                <div class="col-md-3">
					                	<label class="control-label">传真</label>
					                    <input name="userFax" class="form-control" id="userFax" 
					                    	type="text" value="${vo.userFax}" />
					                </div>
				            	</div>
				            	<div class="customGroup">
					                <div class="col-md-3">
					                	<label class="control-label">邮箱</label>
					                    <input name="userEmail" class="form-control" id="userEmail" 
					                    	type="text" value="${vo.userEmail}" />
					                </div>
				            	</div>
				            	<div class="customGroup">
					                <div class="col-md-3">
					                	<label class="control-label">工号</label>
					                    <input name="employeeId" class="form-control" id="employeeId" 
					                    	type="text" value="${vo.employeeId}" />
					                </div>
				            	</div>
				            	<div class="customGroup">
					                <div class="col-md-3">
					                	<label class="control-label">头衔</label>
					                    <input name="userTitle" class="form-control" id="userTitle" 
					                    	type="text" value="${vo.userTitle}" />
					                </div>
				            	</div>
				            </div>		
		    			</div>
		    		</div>
		    	</div>
		    </div>
		</form>
	</section>
	<%@ include file="../resources/include/common_lte_js.jsp"%>
	<script src="<%=basePath%>venson/js/tree.js"></script>
	<script src="<%=basePath%>resources/plugins/zTree_v3/js/jquery.ztree.all-3.5.js"></script>
	<script type="text/javascript">
	    var zTreeObj;
	    $.get(basePath + "unit/listGroup.do", function (data) {
	        var setting = {
	            view: {
	                selectedMulti: false,
	                showIcon: false
	            },
	            check: {
	                enable: true,
	                chkStyle: "radio",
	                chkboxType: {"Y": "", "N": ""},
	                radioType: "all"
	            },
	            data: {
	                key: {
	                    name: "groupChName"
	                },
	                simpleData: {
	                    enable: true,
	                    idKey: "groupId",
	                    pIdKey: "parentGroupId",
	                    rootPId: 0
	                }
	            },
	            callback: {
	                onCheck: zTreeOnCheck,
	            }
	        }
	
	        zTreeObj = $.fn.zTree.init($("#groups"), setting, data);
	        var groupId = $("#positionGroupId").val();
	        var node = zTreeObj.getNodeByParam("groupId", groupId, null);
	        //选中节点并展开
	        if (node) {
	            zTreeObj.checkNode(node, true, true);
	        } 
	        zTreeObj.expandAll(true);
	        $(":radio[value='${selectedGroup}']").prop("checked", true);
	    }, "json");
	
	    function zTreeOnCheck(event, treeId, treeNode) {
	        $("#positionGroupId").val(treeNode.checked ? treeNode.groupId : "1")
	    }

	    $(function () {
	    	$("#undoBtn").on("click",function(){
				toListPage(basePath + "unit/punUserBaseInfoList.do");
			});
	    	
	    	var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
                    "userName": {
                        validators: {
                        	notEmpty: {
                                message: "用户名不能为空"
                            }
                        }                    
        			},  
        			"name": {
                        validators: {
                        	notEmpty: {
                                message: "用户姓名不能为空"
                            }
                        }                    
        			},  
        			"userPwd": {
                        validators: {
                        	notEmpty: {
                                message: "用户密码不能为空"
                            }
                        }                    
        			},  
        			"mobile": {
                        validators: {
                        	notEmpty: {
                                message: "用户手机不能为空"
                            },
                            regexp: {
                            	regexp: /^1[34578]\d{9}$/,
                            	message: '请输入正确的手机号码'
                            },
                        }                    
        			},
        			"roleList": {
                        validators: {
                        	notEmpty: {
                                message: "请选择用户角色"
                            }
                        }                    
        			}, 
        			"positionId": {
                        validators: {
                        	notEmpty: {
                                message: "请选择用户岗位"
                            }
                        }                    
        			}, 
        			"positionGroupId": {
                        validators: {
                        	notEmpty: {
                                message: "请选择用户部门"
                            }
                        }                    
        			}
        	    }
        	});
	    	
        	
	        $("#saveBtn").on("click", function () {
	        	if(!validateForm()){
	    			return false;
	    		}
	            $.ajax({
	         	   	type: "POST",
	         	   	url: basePath + "unit/punUserBaseInfoSave.do",
	         	   	data:$("#groupForm").serialize(),
	         	   	async : false,
	         	   	success: function(data){
	         			if(data.status==0){
	         				toListPage(basePath + "unit/punUserBaseInfoList.do");         				
	         			} else{
	         				Comm.alert(data.message);
	         			}
	         	   	}
	         	})
	        })
	    });
	</script>
</body>
</html>
