<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>元数据新增页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="undoBtn"></i><i class="fa fa-reply"></i> 取消</button>
    	</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">										
						 	<div class="form-group">
						 		<div class="customGroup">						      	
							      	<div class="col-md-4">
							      		<input type="hidden" id="systemId" name="systemId" value="${vo.systemId}" />
							      		<input type="hidden" id="id" name="id" value="${vo.id}">						      	 
							      	 	<label class="control-label required">模型名称(中文)</label>
							         	<input name="modelName" id="modelName" class="form-control" 
							         		type="text" value="${vo.modelName}">
							      	</div>	
							    </div>
						      	<div class="customGroup">	      
							      	<div class="col-md-4">
							      		<label class="control-label required">模型名称(英文)</label>
							         	<input name="modelCode" id="modelCode" class="form-control" 
							         		type="text" value="${vo.modelCode}">
							      	</div>	
							    </div>
						      	<div class="customGroup">			      	
								    <div class="col-md-4">
								      	<label class="control-label required">表名称</label>
								        <input name="tableName" id="tableName" class="form-control" 
								        	type="text" value="${vo.tableName}">
								    </div>
							    </div>
						    </div>
						    <div class="form-group">
								<div class="col-md-12">
							    	<label class="control-label">模型描述</label>
							        <input name="modelDesc" id="modelDesc" class="form-control" 
							        	type="text" value="${vo.modelDesc}">
							   	</div>
							</div>											
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>		
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script type="text/javascript">
        $(document).ready(function(){ 
        	var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
        	    	"modelName": {
                        validators: {
                            notEmpty: {
                                message: "模型名称(中文)不能为空"
                            }
                        }
                    },
                    "modelCode": {
                        validators: {
                        	notEmpty: {
                                message: "模型名称(英文)不能为空"
                            }
                        }
                    },
                    "tableName": {
                        validators: {
                        	notEmpty: {
                                message: "表名称不能为空"
                            }
                        }
                    }
        	    }
        	});
        	
	    	$("#saveBtn").click(function(){   
	    		if(!validateForm()){
	    			return false;
	    		}
	    		var url = "metaModel/save.do";
	    		var id = $("#id").val();
	    		if(id){
	    			url = "metaModel/update.do"
	    		}
	    		var data = myFun.ajaxExecute(url,$("#groupForm").serialize());
	    		if(data.status == "0"){
	    			Comm.alert("保存成功",function(){
	    				location.href = basePath + "metaModel/selectPage.do";
	    			})
	    		} else if(data.message){
	    			Comm.alert(data.message);
	    		} else{
	    			Comm.alert("保存失败");
	    		}
				return false;
			});
	    	
	    	$("#undoBtn").bind("click",function(){
	    		location.href = basePath + "metaModel/selectPage.do";
	    	});
        });
        
        function validateForm(){
        	var $form = $("#groupForm");
        	var validator = $form.data('bootstrapValidator');
        	if(!validator.isValid()){	
        		validator.resetForm();
        	} 
        	validator.validate();
        	return validator.isValid();
        }
	</script>	
</body>
</html>