<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html >
<html>
<head>
	<title>元数据管理</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
    	<div class="opeBtnGrop">
   			<button type="button" class="btn btn-primary" id="addBtn"><i class="fa fa-plus"></i> 新增</button>
   			<button type="button" class="btn btn-success" id="updateBtn"><i class="fa fa-edit"></i> 修改</button>
			<button type="button" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</button>
			<button type="button" class="btn btn-default" id="backBtn"><i class="fa fa-reply"></i> 返回</button>
    	</div>
    	<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form method="post" id="modelMetaItemList">
							<input type="hidden" name="modelId" value="${modelId }" />	
							<div class="row form-group">
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">字段名称</span>
										<input name="itemCode" class="form-control" id="itemCode" type="text"/>
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">字段类型</span>
										<select name="itemType" class="form-control">
											<option value="">--请选择字段类型--</option>
											<option value="int">int</option>
											<option value="bigInt">bigInt</option>
											<option value="varchar">varchar</option>
											<option value="bool">bool</option>
											<option value="boolean">boolean</option>
											<option value="float">float</option>
											<option value="double">double</option>
											<option value="date">date</option>
										</select>
									</div>
								</div>
								<div class="col-md-3 btn-group">
									<button class="btn btn-info" id="searchBtn"><i class="fa fa-search"></i> 搜索</button>									
								</div>	
							</div>	
							<table class="table datatable table-bordered">
								<thead>
									<tr>
										<th class="hidden"></th>
							        	<th data-width="" data-field="" data-checkbox="true"></th>
							            <th>字段名称</th>
							            <th>字段类型</th>
							            <th>长度</th>
							            <th>主键</th>
							            <th>索引</th>
							            <th>允许为空</th>
							            <th>默认值</th>
							            <th>备注</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${list}" var="k">
									<tr>
										<td class="hidden formData"><input type="hidden" value="${k.id}"></td>
							            <td></td>
										<td>${k.itemCode }</td>
										<td>${k.itemType }</td>
										<td>${k.itemLength }</td>
										<td>
											<c:if test="${k.usePrimaryKey==1 }">是</c:if>
											<c:if test="${k.usePrimaryKey==0 or empty k.usePrimaryKey}">否</c:if>
										</td>
										<td>
											<c:if test="${k.useIndex==-1 }">主键</c:if>
											<c:if test="${k.useIndex==1 }">唯一</c:if>
											<c:if test="${k.useIndex==2 }">其它</c:if>
											<c:if test="${k.useIndex==0 or empty k.useIndex}">无</c:if>
										</td>
										<td>
											<c:if test="${k.useNull==1 }">是</c:if>
											<c:if test="${k.useNull==0 or empty k.useNull}">否</c:if>
										</td>
										<td>${k.defaultValue }</td>
										<td>${k.remark }</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>						
						</form>
					</div>				
				</div>				
			</div>
		</div>
    </section>
	<%@ include file="/resources/include/common_lte_js.jsp"%>
	<script type="text/javascript">
		$(function(){
			var count = 0;//默认选择行数为0
			$(".table").bootstrapTable({
				sidePagination:"server",
				pagination:false,
				onClickRow:function(row,$element,field){
					var $checkbox=$element.find(":checkbox").eq(0);
					if($checkbox.get(0).checked){
						$checkbox.get(0).checked=false;
						$element.find("input[type='hidden']").removeAttr("name","id");
					}else{
						$checkbox.get(0).checked=true;
						$element.find("input[type='hidden']").attr("name","id");
					}
					count = $("input[name='id']").length;
				},
				onClickCell:function(field,value,row,$element){

				},
				onCheck: function(row,$element){
					$element.closest("tr").find("input[type='hidden']").attr("name","id");
					count = $("input[name='id']").length;
				},
				onUncheck:function(row,$element){
					$element.closest("tr").find("input[type='hidden']").removeAttr("name");
					count = $("input[name='id']").length;
				},
				onCheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","id");
					});
					count = $("input[name='id']").length;
				},
				onUncheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
					});
					count = $("input[name='id']").length;
				}
			});
			
			//搜索
      		$("#searchBtn").click(function(){
      			$("#modelMetaItemList").attr("action","<%=basePath%>metaModelItems/queryPageResult.do").submit();
      			return false;
      		})
			
          	//新增
      		$("#addBtn").click(function(){
      			$("#modelMetaItemList").attr("action","<%=basePath%>metaModelItems/add_toggle.do").submit();
      			return false;
      		})

      		//修改
      		$("#updateBtn").click(function(){
      			if(count == 1){
      				$("#modelMetaItemList").attr("action","<%=basePath%>metaModelItems/update_toggle.do").submit();
      			}else{
      				Comm.alert("请选择一项进行操作");
      			}
      			return false;
      		});
      		
          	//删除
          	$("#deleteBtn").click(function(){
          		if(count < 1){
          			Comm.alert("请至少选择一项进行操作");
          			return false;
          		}
          		Comm.confirm("确定删除？",function(){
          			$("#modelMetaItemList").attr("action","<%=basePath%>metaModelItems/remove.do").submit();
          		})
      			return false;
          	});
          	
          	$("#backBtn").on("click",function(){
          		location.href = "<%=basePath%>metaModel/selectPage.do";
          	});
          	
		});
	</script>
</body>
</html>