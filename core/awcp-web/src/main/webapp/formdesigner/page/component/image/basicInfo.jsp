<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">只允许拍照上传</label>
				<select name="extra" class="form-control" id="extra">
					<option selected="selected" value="0">否</option>
					<option value="1">是</option>
				</select>
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">最大上传数</label>		
				<input name="maxCount" class="form-control" id="maxCount" type="text" value="">
			</div>
		</div>	
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">上传位置</label>
				<select name="uploadType" class="form-control" id="uploadType">
					<option selected="selected" value="0">mongdb</option>
					<option value="1">文件夹</option>
				</select>
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">上传文件路径</label>		
				<input name="filePath" class="form-control" id="filePath" type="text" value="">
			</div>
		</div>	
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">高度</label>
				<input name="imageHeight" class="form-control" id="imageHeight" type="text" value="">
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">宽度</label>		
				<input name="imageWidth" class="form-control" id="imageWidth" type="text" value="">
			</div>
		</div>	
	</div>
</div>
<%@ include file="../common/basicAttr.jsp" %>







