<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 放置校验列表 -->
<div class="text-right">
	<button type="button" class="btn btn-info" id="addConndition"><i class="icon-plus"></i>新增</button>	
</div>
<div class="connditionTable" contenteditable="false" >
	<table class="table table-bordered"  id="vt" align="right">
		<thead>
			<tr>
				<th width="38%">条件参数</th>
				<th width="8" style="text-align: center;">---》</th>
				<th width="38%">值</th>
				<th width="8%">是否<br/>常量</th>
				<th width="8%" style="text-align: center;">删除</th>
			</tr>
		</thead>
		<tbody id="connditionBody"></tbody>
	</table>
</div>