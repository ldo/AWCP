<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">高度</label>
				<input name="imageHeight" class="form-control" id="imageHeight" type="text" value="">
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">宽度</label>		
				<input name="imageWidth" class="form-control" id="imageWidth" type="text" value="100%">
			</div>
		</div>	
	</div>
</div>
<%@ include file="../common/basicAttr.jsp" %>
