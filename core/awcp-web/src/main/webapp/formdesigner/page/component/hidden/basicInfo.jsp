<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
	<input type="hidden" name="pageId" id="pageId"/>
	<input type="hidden" name="componentType" id="componentType">

	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-12">
				<label class="control-label required">名称</label>
				<div class="input-group">					
					<input class="form-control" name="name" id="name" type="text" />
					<div class="input-group-btn">
		             	<button class="btn btn-default" type="button" id="editName">编辑名称</button>
				 	</div>
				</div>
			</div>	
		</div>
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label required">数据源</label>			
				<div class="input-group" id="selectInput">
		            <input type="text" class="form-control sI_input" name="dataItemCode" id="dataItemCode">
		            <div class="input-group-btn">
		              	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
		              		<span class="caret"></span>
		              	</button>
		              	<ul class="dropdown-menu pull-right sI_select"></ul>
		            </div>
		         </div>				
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label required">序号</label>
				<input name="order" class="form-control" id="order" type="text" value="">
			</div>
		</div>
	</div>
</div>