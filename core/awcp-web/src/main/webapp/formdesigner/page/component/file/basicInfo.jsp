<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<div class="form-group">
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">上传位置</label>
				<select name="uploadType" class="form-control" id="uploadType">
					<option value="0">mongdb</option>
					<option value="1">文件夹</option>
				</select>
			</div>	
		</div>
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">上传文件路径</label>
				<input name="filePath" class="form-control" id="filePath" type="text" value="">
			</div>	
		</div>
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">显示方式</label>
				<select name="showType" class="form-control" id="showType">
					<option selected="selected" value="1">普通格式</option>
				</select>
			</div>	
		</div>
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">建立索引</label>
				<select name="isIndex" class="form-control" id="isIndex">
					<option value="0">否</option>
					<option value="1">是</option>
				</select>
			</div>	
		</div>
	</div>

	<div class="form-group">
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">单个大小（M）</label>
				<input name="singleSize" class="form-control" id="singleSize" type="text" value="">
			</div>	
		</div>
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">总大小（M）</label>
				<input name="maxSize" class="form-control" id="maxSize" type="text" value="">
			</div>	
		</div>
	</div>

	<div class="form-group">
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">最大上传数</label>
				<input name="maxCount" class="form-control" id="maxCount" type="text" value="">
			</div>	
		</div>
		<div class="customGroup">
			<div class="col-sm-6 col-md-6">
				<label class="control-label">文件类型（格式:jpg,txt）</label>
				<input name="fileKind" class="form-control" id="fileKind" type="text" value="">
			</div>	
		</div>
	</div>
</div>
<%@ include file="../common/basicAttr.jsp" %>


