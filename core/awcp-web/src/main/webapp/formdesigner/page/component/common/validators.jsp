<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<input type="hidden" id="validatJson" name="validatJson" value="">	
<!-- 放置校验列表 -->
<div class="text-right" style="margin-bottom: 6px;">
	<button class="btn btn-info" id="validatorStore"><i class="fa fa-search"></i> 校验库</button>
	<button class="btn btn-danger" id="delValidatorBtn"><i class="fa fa-trash"></i> 删除</button>
</div>
<div class="validatorTable">
	<table class="table table-bordered" id="vt" align="right">
		<thead>
			<tr>
				<th width="30px"><input type="checkbox" id="checkAll"/></th>
				<th>名称</th>
				<th>校验类型</th>
				<th>描述</th>
			</tr>
		</thead>
		<tbody id="validatort"></tbody>
	</table>
</div>