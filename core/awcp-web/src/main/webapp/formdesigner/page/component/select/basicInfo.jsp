<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-1 col-sm-1">
				<label class="control-label">多选</label>
				<div>
					<input name="supportMulti" id="supportMulti" type="checkbox" value="1"> 是
				</div>
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-5 col-sm-5">
				<label class="control-label">选择数量</label>
				<input name="selectNumber" class="form-control" id="selectNumber" type="text" value="0">
			</div>
		</div>
	</div>
</div>
<%@ include file="../common/basicAttr.jsp" %>


