<%@page import="cn.org.awcp.unit.vo.PunUserGroupVO"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>菜单编辑页面</title>
    <%@ include file="/resources/include/common_lte_css.jsp" %><!-- 注意加载路径 -->
	<link rel="stylesheet" href="<%=basePath%>resources/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css">
	<style>
		.customerBox{
			height:500px;
			font-size: 16px;
		}
		
		.customerHeader{
			height:101px;
		}
		
		.customerBoxBody{
			border-top: 3px solid #d2d6de;
		}
	</style>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-success" id="saveBtn"><i class="fa fa-save"></i> 确定</button>
		    <button class="btn btn-default" id="undoBtn"></i><i class="fa fa-close"></i> 取消</button>
    	</div>
    	<div class="row">		
			<section class="col-md-4 connectedSortable ui-sortable">
				<div class="box box-info customerBox">
					<div class="box-header customerHeader">
						<div id="tab">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1" data-toggle="tab" onClick="searchDept()">部门</a></li>
								<li><a href="#tab1" data-toggle="tab" onClick="searchRole()">角色</a></li>
		    					<li><a href="#tab1" data-toggle="tab" onClick="searchJob()">岗位</a></li>					
							</ul>
						</div>
						<div class="input-group" style="width:224px;margin-top:5px;">
		                	<input type="text" placeholder="请输入姓名" id="wq" class="form-control">
		                    <span class="input-group-btn"> 
		                    <button class="btn btn-default" type="button" id="subeq" onclick="subSearch()">
		                    	<i class="fa fa-search"></i></button> 
		                    </span>
		                </div>
					</div>
					<div class="box-body customerBoxBody">
						<div id="tab1">
				       		<ul id="tree1" class="ztree"></ul>
				       	</div>
					</div>
		       	</div>  
			</section>
			<section class="col-md-4 connectedSortable ui-sortable">
				<div class="box box-info customerBox">
					<div class="box-header customerHeader">
						<div style="height:44px;">
		            		<button class="btn btn-info" id="checkAll">全选</button>
		            		<button class="btn btn-warning" id="unCheckAll">全不选</button>
		            	</div>
		            	<div class="input-group" style="width:224px;margin-top:5px;">
		            		<input type="text" class="form-control" placeholder="请输入姓名" name="memberName" id="memberName"/>
		                	<span class="input-group-btn">
		                		<button class="btn btn-default searchBtn" type="button" onclick="searchMember()">筛选</button>
		                	</span>
		            	</div>
					</div>
					<div class="box-body customerBoxBody" id="searchBoxBody">
						<div class="tip">请点击左侧菜单</div>
					</div>
				</div>
			</section>
			<section class="col-md-4 connectedSortable ui-sortable">
				<div class="box box-info customerBox">
					<div class="box-header customerHeader">
						<div style="height:44px;">
							<button class="btn btn-warning" id="deleteAll" >全删除</button>
						</div>
						<h4>已选择 </h4>
					</div>
					<div class="box-body customerBoxBody" id="selectedBoxBody">
						<div class="tip">请从中间选择框选择用户</div>
					</div>
				</div>
			</section>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>	
	<script type="text/javascript" src="<%=basePath%>resources/plugins/zTree_v3/js/jquery.ztree.all-3.5.js"></script>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/platform.selectPanel.js"></script>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/jsonpath-0.8.0.js"></script>
	<script type="text/javascript">     
		var datas = Comm.getUrlParam("datas");
		var isSingle = Comm.getUrlParam("isSingle");
        $(function(){       
     		try {
 				var dialog = top.dialog.get(window);
 			} catch (e) {
 				return;
 			}
 				
		   	$("#undoBtn").click(function(){//关闭弹窗 取消
			   	dialog.close("");
		   	});
		   
		   	$("#saveBtn").click(function(){//关闭弹窗 提交
			   	dialog.close(item);
		   	});
		   		    	 	
	    	$("#wq").bind("keyup",function(event) {
	        	if (event.keyCode == 13) {
	        		subSearch();
	        	}
	        });
		});	
	</script>	
</body>
</html>
