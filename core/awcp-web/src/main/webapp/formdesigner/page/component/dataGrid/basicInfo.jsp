<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label required">数据源别名</label>
				<input type="text" class="form-control" name="dataAlias" id="dataAlias">
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-3 col-sm-3">
				<label class="control-label">是否分页</label>		
				<select data-placeholder="是否分页" id="hasPager" class="form-control" tabindex="2" name="hasPager">
					<option value="0">否</option>	
					<option value="1">是</option>
				</select>
			</div>
		</div>	
		<div class="customGroup">
			<div class="col-md-3 col-sm-3">
				<label class="control-label">分页条数</label>		
				<input name="pageSize" class="form-control" id="pageSize" type="text" value="10">
			</div>
		</div>	
	</div>
</div>
<%@ include file="../common/basicAttr_novalue.jsp" %>
