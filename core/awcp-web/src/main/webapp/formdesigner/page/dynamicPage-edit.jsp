<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>动态页面编辑</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
	<style type="text/css">
		.compoents{display: inline-block;width:120px;height:30px;}
	</style>
</head>
<body id="main">
	<div class="content" style="background-color: white;">
		<div class="opeBtnGrop">
			<button class="btn btn-primary" id="savePage"><i class="fa fa-save"></i> 保存</button>
			<button class="btn btn-info" id="export"><i class="fa fa-eye"></i> 预览</button>
			<button class="btn btn-success" id="publishBtn"><i class="fa fa-edit"></i> 发布</button>							
			<button class="btn btn-info" id="catTemplate"><i class="fa fa-eye"></i> 查看发布后内容</button>
			<button class="btn btn-success" id="backup"><i class="fa fa-copy"></i> 备份页面</button>
			<button class="btn btn-success" id="searchBackup"><i class="fa fa-search"></i> 查看该页面备份</button>
			<button class="btn btn-info" id="relation"><i class="fa fa-search"></i> 引用关系</button>
			<button class="btn btn-default" id="ret"><i class="fa fa-reply"></i> 返回</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info" style="box-shadow: none;">
					<form class="form-horizontal" id="groupForm">
						<div id="tab">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#basicInfo" data-toggle="tab">基本</a></li>
								<li class=""><a href="#datasource" data-toggle="tab">数据源</a></li>
								<c:if test="${ not empty vo.id }">
									<li class=""><a href="#locLayout" data-toggle="tab">布局</a></li>
									<li class=""><a href="#componentConfig" data-toggle="tab">组件</a></li>
									<li class=""><a href="#actConfig" data-toggle="tab">动作</a></li>
									<li class=""><a href="#workflow" data-toggle="tab">流程</a></li>
									<li class=""><a href="#authority" data-toggle="tab">权限组配置</a></li>
								</c:if>
							</ul>
						</div>			
						<div class="tab-content" style="margin:15px;">
							<div class="tab-pane active" id="basicInfo"><%@ include file="tabs/basicInfo.jsp" %></div>
							<div class="tab-pane" id="datasource"><%@ include file="tabs/datasource.jsp" %></div>
							<c:if test="${ not empty vo.id }">
								<div class="tab-pane" id="locLayout"><%@ include file="tabs/layoutList.jsp" %></div> 
								<div class="tab-pane" id="componentConfig"><%@ include file="tabs/component.jsp" %></div> 
								<div class="tab-pane" id="actConfig"><%@ include file="tabs/act.jsp" %></div>
						    	<div class="tab-pane" id="workflow"><%@ include file="tabs/workflow.jsp" %></div> 
								<div class="tab-pane" id="authority"><%@ include file="tabs/authorityGroupList.jsp" %></div> 
							</c:if>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/map.js"></script>
	<script type="text/javascript" src="<%=basePath%>formdesigner/scripts/dynamicpage.constant.js"></script>
	<script type="text/javascript" src="<%=basePath%>formdesigner/scripts/form.dpcommon.js"></script>
	<script type="text/javascript" src="<%=basePath%>formdesigner/scripts/form.cpcommons.js"></script>	
	<script type="text/javascript" src="<%=basePath%>formdesigner/page/script/data.js"></script>
	<script type="text/javascript" src="<%=basePath%>formdesigner/page/script/act.js"></script>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/layout.js"></script>
	<script type="text/javascript" src="<%=basePath%>formdesigner/page/script/workflow.js"></script>
	<script type="text/javascript" src="<%=basePath%>formdesigner/page/script/authorityGroup.js"></script>
	<script type="text/javascript">
		//表单提交前校验
		function validate(){
			var $form = $("#groupForm");
			var validator = $form.data('bootstrapValidator');
			if(!validator.isValid()){	
				validator.resetForm();
			} 
			validator.validate();
			return validator.isValid();
		}
	
		//表单校验初始化
		function initValidator(){
			var $form = $("#groupForm");
			$form.bootstrapValidator({
				excluded: [":disabled"],
			    fields:{
			    	"name": {
		                validators: {
		                    notEmpty: {
		                        message: "请填写动态页面类型"
		                    }
		                }
		            },
		            "pageType": {
		                validators: {
		                	notEmpty: {
		                        message: "请选择页面类型"
		                    }
		                }
		            },
		            "templateId": {
		                validators: {
		                	notEmpty: {
		                        message: "请选择模板"
		                    }
		                }
		            }
			    }
			});
		}
	
		//显示或隐藏列表页面所需参数
		function showListConfig(){
			var pType = $("#pageType").val();
			if(pType=='1003'){
				$(".listPageConfig").show();
			}else{
				$(".listPageConfig").hide();
			}
		} 
		
		$(document).ready(function() {
			var id = "${vo.id}";
			var tips = "${tips}";
			if(tips){
				Comm.alert(tips);
			}
			
			$("title").text('[' + $("#name").val() + ']动态编辑');
			
			initValidator();
			
			//动态页面所属模块
			var modularID = '${vo.modular}';
			$("#modular").val(modularID);
			$("#modular").select2({width:"100%",placeholder:'请选择',allowClear:!0,language:'zh-CN'});
			
			//动态页面类型
			$("#pageType").select2({width:"100%",placeholder:'请选择',allowClear:!0,language:'zh-CN'});
			$("#pageType").change(function(){
				showListConfig();
			});
			showListConfig();
			
			//动态页面选择的模板
			var templateId = '${vo.templateId}';
			$("#templateId").val(templateId);
			$("#templateId").select2({width:"100%",placeholder:'请选择',allowClear:!0,language:'zh-CN'});
			
			//动态页面选择的样式库
			var styleId = '${vo.styleId}';
			$("#styleId").val(styleId);
			$("#styleId").select2({width:"100%",placeholder:'请选择',allowClear:!0,language:'zh-CN'});
			
			//初始化数据源表格
			initDataSource();
			
			//初始化动作按钮表格
			initAct();
			
			//初始化组件
			var dynamicPageId='${vo.id}';
			$("#typeId").select2({width:"100%",placeholder:'请选择',allowClear:!0,language:'zh-CN'});
			loadComponentTable(dynamicPageId);
			
			//初始化布局
			loadLayoutTable(dynamicPageId);
			
			//保存页面
			$("#savePage").click(function(){
				if(!validate()){
					return false;
				}
				$.ajax({
					type: "POST",
					url: basePath + "fd/save.do",
					data:$("#groupForm").serialize(),
					async : false,
					success: function(data){
						if(data.status == "0"){
							var id = data.data;
							location.href = basePath + "fd/edit.do?_selects=" + id;
						} else{
							if(data.message){
								Comm.alert(data.message);
							} else{
								Comm.alert("保存失败");
							}
						}
					}
				});	
				return false;
			});
			
			//删除组件
			$("#deleteComponent").click(function(){
				var count = $(":checkbox[name='component']:checked").size();
				if(count > 0){
					var dynamicPageId = '${vo.id}';
					edit("component","remove",null,null,dynamicPageId);
					return false;
				}else{
					alert("请选择组件操作");
					return false;
				}
			});
			
			//返回列表页面
			$("#ret").click(function(){	
				location.href = basePath + "formdesigner/page/dynamicPageList.html";
			});
			
			//页面预览
			$("#export").click(function(){	
				top.dialog({ id: 'add-dialog' + Math.ceil(Math.random()*10000),
					title: '页面预览',
					url:'<%=basePath%>document/view.do?dynamicPageId='+'${vo.id}',
					width:1024,
					height:768,
					okValue:'确定',
					ok:function(){},
					cancelValue:'取消',
					cancel:function(){}
				}).showModal();
				return false;
			});
			
			//发布
			$("#publishBtn").click(function(){
				var pageType = $("#pageType").val();
				var datasourcebody = $("#datasourcebody").html();
				if(pageType=="1003" && datasourcebody==""){
					Comm.alert("请填写数据源");
					return false;
				}
				var data = myFun.ajaxExecute("fd/publishOnePage.do",{id:$("#id").val()});
				if(data.status == 0){
					Comm.alert("发布成功");
				} else{
					Comm.alert(data.message);
				}
				return false;
	    	});

			//备份页面列表
			$("#searchBackup").on("click",function(){
				var id = $("#id").val();
				var url = basePath + "formdesigner/page/pageBackupList.html?dynamicpageId=" + id;
				top.addTabs({
			        url: url,
			        title: "查询备份列表",
			        close:true
			    });
			});
			
			//备份页面
			$("#backup").on("click",function(){
				var id = $("#id").val();
				var content = '<div class="form-horizontal"><div class="form-group">' +
								 '<label class="col-md-1 control-label">描述</label>' +
								 '<div class="col-md-11">' + 
								 	'<textarea class="form-control" rows="5" id="backupDesciption"></textarea>' +
								 '</div>' + 
							 '</div></div>';
				var d = dialog({
					title: '备份页面',
					content: content,
					okValue: '确定',
					ok: function() {
						var description = $("#backupDesciption").val();
						$.ajax({
							type:"POST",
							url:"fd/backup.do",
							data:{id:id,description:description},
							async:false,
							dataType:'json',
							success:function(result){
								if(result.status == 0){
									alert("备份成功");
								}else{
									alert(result.message);
								}	   	
							},
					  	 	error: function (XMLHttpRequest, textStatus, errorThrown) { 
			           	    	alert(errorThrown); 
					   	 	}
						});
						return true;
					},
					cancelValue: '取消',
					cancel: function() {}
				});
				d.width(700).showModal();
				return false;			
			});
	    	
	    	//页面发布后的内容
			$("#catTemplate").click(function(){
	    		var id = '${vo.id}';
				top.dialog({ 
					id: 'add-dialog' + Math.ceil(Math.random()*10000),
					title: '页面发布后的模版内容',
					url:'<%=basePath%>fd/catTemplate.do?_select='+id,
					width:800,
					height:600,
					okValue:'确定',
					ok:function(){},
					cancelValue:'取消',
					cancel:function(){}
				}).showModal();
				return false;
			});
	    	
	    	$("#relation").click(function(){
	    		var id = '${vo.id}';
				top.dialog({ id: 'add-dialog' + Math.ceil(Math.random()*10000),
					title: '页面引用关系',
					url:'<%=basePath%>fd/relation.do?_select='+id,
					width:500,
					height:500,
					okValue:'确定',
					ok:function(){},
					cancelValue:'取消',
					cancel:function(){}
				}).showModal();
				return false;
			});
		});
    </script>
</body>
</html>
