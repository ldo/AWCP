<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>配置权限组页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" role="form" method="post" id="authority">
							<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
							<input type="hidden" name="nodeIds" id="nodeIds"/>
							<table id="table" class="table table-bordered"  width="100%">
			               		<thead id="authorityGroup"></thead>
			            	</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>	
	<%@ include file="/resources/include/common_lte_js.jsp" %>	
	<script src="<%=basePath%>resources/scripts/json2.js"></script>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			var data = null;
			try {
				var dialog = top.dialog.get(window);
				dialog.title("权限组配置");
				dialog.width(800);
				dialog.height(500);
				dialog.reset(); 
				data = dialog.data;
				if(data.pageId){
					$("#dynamicPageId").val(data.pageId);
				}
				if(data.nodeIds){
					$("#nodeIds").val(data.nodeIds);
				}				
			} catch (e) {
				return;
			}
			
			//查询数据
			$.ajax({
			   	type: "POST",
			   	url: basePath + "authority/getGroupListByPageId.do",
			   	data:{
				   	dynamicPageId: $("#dynamicPageId").val(),
				   	nodes: $("#nodeIds").val()
			   	},
			   	success: function(data){
					if(data != null){
						$("#authorityGroup").empty();
						$.each(data,function(index,item){
							var check=item.bakInfo;
						 	var show=new RegExp('check');
						 	var str="";
						 	if(show.test(check)){
						 		str="<tr><td><input type='checkbox' id='"+item.id+"' onclick='saveAuthority(\""+item.id+"\")' checked='checked'>"+item.name+"</td></tr>"
						 	}else{
						 		str="<tr><td><input type='checkbox' id='"+item.id+"' onclick='saveAuthority(\""+item.id+"\")'>"+item.name+"</td></tr>"
						 	}
						 	$("#authorityGroup").append(str);
						});
				    }
				}
			});	
		});
	
		function saveAuthority(value){
			var status = 1;
			if($("#"+value).is(':checked')==false){
				status=0;
			}
			$.ajax({
			    type: "POST",
				url: basePath + "authority/addWorkflowNodeAuthority.do",
				data: {
					authorityGroupId: value,
					status: status,
					nodes: $("#nodeIds").val(),
					dynamicPageId: $("#dynamicPageId").val()
				},
				success: function(data){
					
				}
			});	
		}
	</script>
</body>
</html>
