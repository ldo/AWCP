<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-group">	
	<div class="col-md-12">
		<label class="control-label">校验</label><br/>
		<c:choose>
			<c:when test="${act.chooseValidate == 'true' }">
				<label class="radio-inline"> 
					<input name="chooseValidate" type="radio" checked="checked" value="1">是
				</label>
				<label class="radio-inline"> 
					<input name="chooseValidate" type="radio" value="0">否
				</label>
			</c:when>
			<c:otherwise>
				<label class="radio-inline"> 
					<input name="chooseValidate" type="radio" value="1">是
				</label>
				<label class="radio-inline"> 
					<input name="chooseValidate" type="radio" checked="checked" value="0">否
				</label>
			</c:otherwise>
		</c:choose>
	</div>								
</div>
<div class="form-group">
	<div class="col-md-6">
		<label class="control-label">服务端脚本</label>
		<textarea name='serverScript' id='serverScript' rows='10' class='form-control' style="resize: vertical;">${act.serverScript }</textarea>
	</div>
	
	<div class="col-md-6">
		<label class="control-label">客户端脚本</label> 
		<textarea name='clientScript' id='clientScript' rows='10' class='form-control' style="resize: vertical;">${act.clientScript }</textarea>
	</div>
</div>
<div class="form-group">
	<div class="col-md-6">
		<label class="control-label">隐藏脚本</label>
		<textarea name='hiddenScript' id='hiddenScript' rows='4' class='form-control' style="resize: vertical;">${act.hiddenScript }</textarea>
	</div>
	<div class="col-md-6">
		<label class="control-label">描述</label>
		<textarea name='description' id='description' rows='4' class='form-control' style="resize: vertical;">${act.description }</textarea>
	</div>
</div>

