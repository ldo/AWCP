<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>权限组新增页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveAuthority"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">	
							<input type="hidden" name="authorityId" id="authorityId"/>
							<div class="form-group">  
			  					<div class="customGroup">	
									<div class="col-md-6">
										<label class="control-label required">名称</label>
										<input id="name" type="text" class="form-control" name="name" value="" />
									</div>
			  					</div>
			  				</div>
			  				<div class="form-group">  
			  					<div class="customGroup">	
									<div class="col-md-6">
										<label class="control-label required">序号</label>
										<input id="order" type="text" class="form-control" name="order" value="" />
									</div>
			  					</div>
			  				</div>
			  				<div class="form-group">  
			  					<div class="customGroup">	
									<div class="col-md-6">
										<label class="control-label">描述</label>
										<input id="destry" type="text" class="form-control" name="destry" value="" />
									</div>
			  					</div>
			  				</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script type="text/javascript">			
		var data = null;	
		$(function(){
			try {
				var dialog = top.dialog.get(window);
				dialog.title("权限组配置");
				dialog.width(500);
				dialog.height(400);
				dialog.reset(); 
				data = dialog.data;
				if(data.id){
					$("#authorityId").val(data.id);
				}
			} catch (e) {
				return;
			}
			
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			var $form = $("#groupForm");
			$form.bootstrapValidator({
				excluded: [":disabled"],
			    fields:{
		            "name": {
		                validators: {
		                	notEmpty: {
		                        message: "请输入名称"
		                    }
		                }
		            },
		            "order": {
		                validators: {
		                    notEmpty: {
		                        message: "请输入序号"
		                    },
                            regexp: {
                                regexp: /^[1-9]\d*$/,
                                message: '请输入正整数'
                            }
		                }
		            }
			    }
			});
			
			$.ajax({
			   	type: "POST",
			   	url: basePath + "authority/findAuthorityById.do",
			  	data: {
			  		id:  $("#authorityId").val()
			  	},
			   	success: function(data){
					if(data != null){	
				     	$("#order").val(data.order);
				     	$("#destry").val(data.description);
					 	$("#name").val(data.name);
			    	}
			  	}
			});	
			
			$("#saveAuthority").click(function(){
				if(!validateForm()){
					return false;
				}
				$.ajax({
					type: "POST",
					url: basePath + "authority/authorityGroupUpdate.do",
					data: {
						id: $("#authorityId").val(),
						order: $("#order").val(),
						destry: $("#destry").val(),
						name : $("#name").val()
					},
					success: function(data){
						if(data != null){	
							if(dialog != null){
								dialog.close(data);
								dialog.remove();
							}
						} else{
							alert("保存失敗");
						}
					}
				});	
			})
		})
	</script>
</body>
</html>
