<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>数据源新增页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<div class="content" style="background-color: white;">
		<div class="opeBtnGrop">
			<button type="button" class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
			<button type="button" class="btn btn-default" id="cancelBtn"><i class="fa fa-remove"></i> 关闭</button>
		</div>
		<div style="background-color: #eff;margin-bottom:10px;padding:2px;">
			<span>提示：</span><br>
			<span>列表页面(分页)，请选择多行数据，分页；并在动态页面配置的基本tab中的分页选项，选择是，然后填写每页记录数</span><br>
			<span>列表页面(不分页)，请选择多行数据，不分页，请保证固定记录数大于数据库的记录总数</span><br>
			<span>表单页面，请选择单行数据</span></br>
			<span>请不要在sql语句中写入limit等分页字样</span><br>
		 	<c:if test="${result!=null&&''!=result}">
				<span style="color: red">(${result})</span>
			</c:if>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">	
					<div class="row" id="dataform">	
						<form id="datasourceForm" class="form-horizontal" style="margin:0px 15px;padding:0 15px;">
							<input type="hidden" name="id" value="" id="id"/>
							<div class="form-group">
								<div class="customGroup">
									<div class="col-md-3 col-sm-3">
										<label class="control-label required">别名</label>
										<input class="form-control" name="name" id="name" type="text" value="" />
									</div>
								</div>
								<div class="customGroup">
									<div class="col-md-3 col-sm-3">
										<label class="control-label">数据记录</label>		
										<select class="form-control" name="isSingle" id="isSingle">
											<option value="0">单行数据</option>
											<option value="1">多行数据</option>
										</select>
									</div>
								</div>	
								<div class="customGroup">
									<div class="col-md-3 col-sm-3">
										<label class="control-label">分页</label>
										<select class="form-control" name="isPage" id = "isPage">
											<option value="0">不分页</option>
											<option value="1">分页</option>
										</select>
									</div>
								</div>
								<div class="customGroup">
									<div class="col-md-3 col-sm-3">
										<label class="control-label">固定记录</label>		
										<input type='text' name='limitCount' id='limitCount' value="0" class='form-control' />
									</div>
								</div>	
							</div>

							<div class="form-group">
								<div class="customGroup">
									<div class="col-md-12 col-sm-12">
										<label class="control-label">SQL语句</label>
										<textarea rows="8" name="sqlScript" id="sqlScript" class='form-control'></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="customGroup">
									<div class="col-md-12 col-sm-12">
										<label class="control-label">DELETE SQL语句</label>
										<textarea rows="3" name="deleteSql" id="deleteSql" class='form-control'></textarea>
									</div>
								</div>
							</div>
					        <div class="form-group" id="modelSelect" >
					        	<div class="customGroup">
					        		<div class="col-md-4 col-sm-4">
										<label class="control-label">选择模型</label>
										<select id="modelCode" class="form-control" name="modelCode">
											<option value="">-----------请选择模型------------</option>
										</select>
									</div>
								</div>
								<div class="customGroup">
									<div class="col-md-8 col-sm-8">
										<label class="control-label">描述</label>
										<input name='description' id='description' class='form-control'/>
									</div>
								</div>
							</div>	
																		
					    	<table class="table table-bordered" style="padding-top:15px;">
					            <thead>
						        	<tr>
						                <th width="30"><input type="checkbox" id="checkAllItem"/></th>
						                <th>名称（中文）</th>
						                <th>名称（英文）</th>
						                <th width="90">类型</th>
						                <th width="90">长度</th>
						                <th width="90">默认值</th>
						                <th width="90">不允许空</th>
						            </tr>
					            </thead>
					            <tbody id="modelitemtable"></tbody>
					    	</table>				    				        
						</form>
					</div>
			   	</div>
			</div>
		</div>
	</div>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/json2.js"></script>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>resources/scripts/map.js"></script>
	<script type="text/javascript" src="<%=basePath%>formdesigner/page/script/data.js"></script>
	<script type="text/javascript">
		var models = new Map();
		
		//勾选模型属性时触发
		function changeItemMap(ched){
			if($(ched).prop("checked")){
				itemMap.put($(ched).val(),true);
			}else{
				itemMap.remove($(ched).val());
			}
			freshSQL();
		}
		
		//初始化一行模型属性数据
		function initItemRow(item, flag){
			var html = "<tr>";
			//是否为选择状态
			html += "<td><input type='checkbox' " + (flag?"checked='checked'":"") + 
					" data-name='item' onclick='changeItemMap(this)' " + "value='" + item.itemCode + "'></td>";
			html += "<td>" + (item.itemName?item.itemName:"") + "</td>";
			html += "<td>" + (item.itemCode?item.itemCode:"") + "</td>";
			html += "<td>" + (item.itemType?item.itemType:"") + "</td>";
			html += "<td>" + (item.itemLength?item.itemLength:"") + "</td>";
			html += "<td>" + (item.defaultValue?item.defaultValue:"") + "</td>";
			html += "<td>" + (item.itemLength?"是":"") + "</td>";
			html += "</tr>";
			$("#modelitemtable").append(html);
		}
		
		try {
			var dialog = top.dialog.get(window);
			dialog.height($(document).height());
			dialog.reset();
		} catch (e) {
			console.log(e);
		}

		//初始化模型属性
		function initModelItem(obj){
			if($("#modelCode").val()){
				$.ajax({
					type : "POST",
					url : "<%=basePath%>metaModel/queryModelItemByModel.do",
					data : {"modelCode" : $("#modelCode").val()}, 
					success : function(data){
						$("#modelitemtable").empty();
						itemMap.clear();
						var json = eval(data);
						var modelItemCodes = new Array();
						if(!empty(obj)){
							if(obj.modelItemCodes) {
								modelItemCodes = obj.modelItemCodes.split(",");
							}
						}
						$.each(json,function(idx,item){
							if($.inArray(item.itemCode, modelItemCodes) > -1){
								itemMap.put(item.itemCode, true);
								initItemRow(item, true);
							} else {
								initItemRow(item, false);
							}
						});
					}
				});
			}
		}
		
		/**
		 * 初始化选择模型列表，如果是编辑，则将标记指定的模型为选择状态
		 * 页面打开时只加载一次，后续可能会加上分页和搜索的功能
		 */
		function initModel(modelCode){
			$.ajax({
				type:"GET",
				async:false,
				url:"<%=basePath%>metaModel/queryPageByModel.do",
				success:function(data){
					var json = eval(data);
					$.each(json, function(idx, item){
						models.put(item.modelCode,item.tableName);
						if(item.modelCode==modelCode){
							$("#modelCode").append("<option value='" + item.modelCode + "' selected='selected'>"
													+ item.modelName + "</option>");
						}else{
							$("#modelCode").append("<option value='" + item.modelCode + "'>" + item.modelName + "</option>");
						}
					});
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					console.log(errorThrown);
				}
			});
		}		
		
		$(document).ready(function(){
			//通过dialog获取数据，如果有，则是编辑状态，否则是新增状态
			var model = dialog.data;
			if(!empty(model)) {
				var modelCode  = model.modelCode;
				$("#name").val(model.name);
				$("#description").val(model.description);
				$("#id").val(model.id);
				if(model.isPage) {
					$("#isPage").val(model.isPage).trigger("change");
				}				
				if(model.isSingle) {
					$("#isSingle").val(model.isSingle).trigger("change");
				}
				if(model.limitCount) {
					$("#limitCount").val(model.limitCount);
				}
				$("#sqlScript").val(model.sqlScript);
				$("#deleteSql").val(model.deleteSql);
				
				initModel(modelCode);
				//初始化模型属性
				initModelItem(model);				
			} else{ 
				$("#id").val(guidGenerator());
				initModel();
			}
			
			$("#modelCode").select2();
			$("#checkAllItem").click(function(){
				if($(this).prop("checked")){
					$(":checkbox[data-name='item']").prop("checked",true);
					$(":checkbox[data-name='item']").each(function(idx,item){
						itemMap.put($(item).val(),true);
					});
				}else{
					$(":checkbox[data-name='item']").prop("checked",false);
					$(":checkbox[data-name='item']").each(function(idx,item){
						itemMap.remove($(item).val());
					});
				}
				freshSQL();
			});

			//数据源模型改变
			$("#modelCode").change(function(){
				itemMap.clear();
				$("#modelitemtable").empty();
				sqlTableName = models.get($("#modelCode").val());
				initModelItem(model);
				if(dialog){
					dialog.height($(document).height());
					dialog.reset();
				}
			});
			
			//保存
			$("#saveBtn").click(function(){
				var formData = $("#datasourceForm").serializeJSON();
				var codes=[];
				$(":checkbox[data-name='item']").each(function(){
					if($(this).prop("checked")){
						codes.push($(this).val());
					}
				});
				if(codes.length > 0){
					formData.modelItemCodes = codes.join(",");
					dialog.close(formData);
					dialog.remove();
				} else {
					Comm.alert("请选择模型属性！");
				}
			});
			
			//取消
			$("#cancelBtn").click(function(){
				dialog.remove();
			});
        });
	</script>	
</body>
</html>
