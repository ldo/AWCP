<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head><base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>校验编辑页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body>
	<section class="content">
		<div class="opeBtnGrop">
			<button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
			<button class="btn btn-default" id="undoBtn"><i class="fa fa-reply"></i> 取消</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">
							<input type="hidden" name="pageId" id="id" value="${vo.pageId }" />
							<div class="form-group">
								<div class="customGroup">
									<div class="col-sm-4 col-md-4">
										<label class="control-label required">校验名称</label>
										<input class="form-control" name="name" id="name" type="text" value="${vo.name}" />
									</div>
								</div>
								<div class="customGroup">
									<div class="col-sm-4 col-md-4">
										<label class="control-label required">校验类型</label>
										<select class="form-control" name="type" id="type" type="text" value="${vo.type}" >
											<option value="stringLength">字段长度校验</option>
											<option value="regexp">正则表达式校验</option>
											<option value="callback">回调函数校验</option>
										</select>
									</div>
								</div>
								<div class="customGroup">
									<div class="col-sm-4 col-md-4">
										<label class="control-label required">校验描述</label>
										<input class="form-control" name="description" id="description" type="text" 
											value="${vo.description}" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="customGroup">
									<div class="col-sm-4 col-md-4">
										<label class="control-label required">字段最小长度</label>
										<input class="form-control" name="min" id="min" type="text" value="${vo.min}" />
									</div>
								</div>
								<div class="customGroup">
									<div class="col-sm-4 col-md-4">
										<label class="control-label required">字段最大长度</label>
										<input class="form-control" name="max" id="max" type="text" value="${vo.max}" />
									</div>
								</div>
								<div class="customGroup">
									<div class="col-sm-8 col-md-8">
										<label class="control-label required">正则表达式</label>
										<input class="form-control" name="regexp" id="regexp" type="text" value="${vo.regexp}" />
									</div>
								</div>
								<div class="customGroup">
									<div class="col-sm-8 col-md-8">
										<label class="control-label required">回调函数</label>
										<textarea class="form-control" name="callback" id="callback" style="resize: vertical;" 
											rows="10">${vo.callback}</textarea>
									</div>
								</div>
								<div class="customGroup">
									<div class="col-sm-4 col-md-4">
										<label class="control-label required">错误提示</label>
										<input class="form-control" name="onError" id="onError" type="text" value="${vo.onError}" />
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<%@ include file="/resources/include/common_lte_js.jsp"%>	
	<script type="text/javascript">
		//初始化校验
		function initFormValidator(){
			var $form = $("#groupForm");
			$form.bootstrapValidator({
				excluded: [":disabled", ":hidden", ":not(:visible)"],
			    fields:{
			    	"name": {
	                    validators: {
	                        notEmpty: {
	                            message: "校验名称不能为空"
	                        }
	                    }
	                },
	                "description": {
	                    validators: {
	                    	notEmpty: {
	                            message: "校验描述不能为空"
	                        }
	                    }
	                },
	                "onError": {
	                    validators: {
	                    	notEmpty: {
	                            message: "错误提示不能为空"
	                        }
	                    }
	                },
	                "min": {
	                    validators: {
	                    	notEmpty: {
	                            message: "字段最小长度不能为空"
	                        },
	                        regexp: {
	                        	regexp: /^\d+$/,
	                        	message: '序号请输入数字'
	                    	}
	                    }
	                },	 
	                "max": {
	                    validators: {
	                    	notEmpty: {
	                            message: "字段最大长度不能为空"
	                        },
	                        regexp: {
	                        	regexp: /^\d+$/,
	                        	message: '序号请输入数字'
	                    	},
	                    	callback:{
	                    		callback:function(value, validator,$field){
	                    			var min = $("#min").val() - 0;
	                    			var max = $("#max").val() - 0;
	                    			if(min > max){
	                    				return false;
	                    			} else{
	                    				return true;
	                    			}
	                    		},
	                    		message: "最大长度必须大于最小长度"	                    		
	                    	}
	                    }
	                },
	                "regexp": {
	                    validators: {
	                    	notEmpty: {
	                            message: "正则表达式不能为空"
	                        }
	                    }
	                },
	                "callback": {
	                    validators: {
	                    	notEmpty: {
	                            message: "回调函数不能为空"
	                        }
	                    }
	                },
			    }
	    	});
		}
	
		//显示和隐藏
		function showAndHide(){
			var type = $("#type").attr("value");
			if(type){
				$("#type").val(type);
			}
			type = $("#type").val();
			if(type=="stringLength"){
				$("#regexp,#callback").parent().hide();
				$("#regexp,#callback").val("");
				$("#max,#min").parent().show();
			} else if(type=="regexp"){
				$("#max,#min,#callback").parent().hide();
				$("#max,#min,#callback").val("");
				$("#regexp").parent().show();
			} else if(type=="callback"){
				$("#max,#min,#regexp").parent().hide();
				$("#max,#min,#regexp").val("");
				$("#callback").parent().show();
			}
		}
	
		$(function(){
			showAndHide();
			
			initFormValidator();
			
			$("#type").on("change",function(){
				showAndHide();
			});
			
			$("#undoBtn").on("click",function(){
				location.href = basePath + "fd/validator/list.do";
			});
			
			$("#saveBtn").on("click",function(){
				if(validateForm()){
					var data = myFun.ajaxExecute("fd/validator/saveValidator.do",$("#groupForm").serialize());
					if(data.status==0){
						Comm.alert("保存成功",function(){
							location.href = basePath + "fd/validator/list.do";
						});
					} else{
						if(data.message){
							Comm.alert(data.message);
						} else{
							Comm.alert("保存失败");
						}
					}
				}			
			});
		});
	</script>	
</body>
</html>
