<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>样式编辑页面</title>
	<base href="<%=basePath%>">
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
			<div class="row">
				<div class="opeBtnGrop">
					<button type="button" class="btn btn-success" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
					<button type="button" class="btn btn-default" id="backBtn"><i class="fa fa-reply"></i> 返回</button>
				</div>
				<div class="row">
					<div class="col-md-12">
					    <div class="box box-info">
					        <div class="box-body">
								<form class="form-horizontal" id="groupForm" method="post">
									<input type="hidden" name="pageId" value="${vo.pageId}" />	
			  						<input type="hidden" name="code" value="${vo.code}" />
			  						<div class="form-group">  
			  							<div class="customGroup">	
								            <div class="col-md-6">
								            	<label class="control-label required">样式名称</label>
								                <input name="name" class="form-control" id="name" type="text" value="${vo.name}" />
								            </div>
			  							</div>
			  							<div class="customGroup">	
								            <div class="col-md-4">
								            	<label class="control-label required">样式描述</label>
								                <input name="description" class="form-control" id="description" 
								                	type="text" value="${vo.description}" />
								            </div> 
			  							</div>     	
							 	    </div>
									<div class="form-group">    
										<div class="customGroup">	   	
								            <div class="col-md-12">
								            	<label class="control-label required">样式脚本</label>
								                <textarea name="script" class="form-control" id="script" 
								                	rows="4" style="resize: vertical;">${vo.script}</textarea>
								            </div>
							            </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script type="text/javascript">	
		$(document).ready(function() {
			var $form = $("#groupForm");
			$form.bootstrapValidator({
				excluded: [":disabled"],
			    fields:{
		            "name": {
		                validators: {
		                	notEmpty: {
		                        message: "请输入样式名称"
		                    }
		                }
		            },
		            "description": {
		                validators: {
		                    notEmpty: {
		                        message: "请输入样式描述"
		                    }
		                }
		            },
		            "script": {
		                validators: {
		                    notEmpty: {
		                        message: "请输入样式代码"
		                    }
		                }
		            }
			    }
			});
			
			//保存
			$("#saveBtn").click(function() {
				if(!validateForm()){
					return false;
				}
				var data = $("#groupForm").serializeJSON();
		        var ret = myFun.ajaxExecute("fd/style/saveByAjax.do",data);
		        if(ret.status == "0"){
		        	Comm.alert("保存成功",function(){
		        		location.href = basePath + "fd/style/list.do";
		        	})
		        } else{
		        	Comm.alert("保存失败");
		        }
		        return false;
			});
			
			$("#backBtn").on("click",function(){
				location.href = basePath + "fd/style/list.do";
			});
		});
	</script>
</body>
</html>