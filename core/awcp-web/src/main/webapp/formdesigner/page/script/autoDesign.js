/**
 * 动态页面可视化表单设计js
 */
var pageId = $("#id").val();	//动态页面ID
var $newComp = null;			//新增的组件
//组件类型Map
var compMap = {
	"1001"	:	{type:"1001",name:"单行文本框",icon:""},
	"1002"	:	{type:"1002",name:"日期文本框",icon:""},
	"1003"	:	{type:"1003",name:"多选复选框",icon:""},
	"1004"	:	{type:"1004",name:"单选按钮",icon:""},
	"1005"	:	{type:"1005",name:"多行输入框",icon:""},
	"1006"	:	{type:"1006",name:"下拉选项框",icon:""},
	"1008"	:	{type:"1008",name:"列框",icon:""},
	"1009"	:	{type:"1009",name:"标签",icon:""},
	"1010"	:	{type:"1010",name:"隐藏框",icon:""},
	"1011"	:	{type:"1011",name:"文件上传框",icon:""},
	"1012"	: 	{type:"1012",name:"包含组件框",icon:""},
	"1016"	:	{type:"1016",name:"图片上传框",icon:""},
	"1017"	:	{type:"1017",name:"表格组件",icon:""},
	"1019"	:	{type:"1019",name:"富文本框",icon:""},
	"1020"	:	{type:"1020",name:"用户选择框",icon:""},
	"1033"	:	{type:"1033",name:"级联下拉框",icon:""},
	"1036"	:	{type:"1036",name:"搜索条件",icon:""},
	"1043"	:	{type:"1043",name:"流程意见组件",icon:""}
}

/**
 * 初始化设计器
 */
function initAutoDesign(){
	var pageType = $("#pageType").val();
	//清空数据
	$(".headLi").nextAll(".sidebar-li").remove();
	$("#content-wrapper").empty();
	$("#commPage").hide();
	//初始化组件
	initComponent(pageType);
	if(pageType=="1002"){//表单页面
		initLayout();	
		$("#content-wrapper").empty();
		$("#content-wrapper").html("<div class='form-group' id='hiddenDiv'></div>");
		$(".sidebar-ul").eq(0).show();
		$(".headLi").on("click",function(){
	    	$(".headLi").not($(this)).nextAll(".sidebar-li").hide();
	    	$(this).nextAll(".sidebar-li").show();
	    });
	} else if(pageType=="1003"){//列表页面
		addListPageContianer();
		$(".sidebar-ul").eq(0).hide();
		$(".headLi").eq(1).nextAll().show();	
	}
}

//初始化组件
function initComponent(pageType){
	var compArr = [];
	if(pageType=="1002"){//表单页面
		for(type in compMap){
			if(type!="1008" && type!="1036"){
				compArr.push(compMap[type]);
			}
		}		
	} else if(pageType=="1003"){//列表页面
	    for(type in compMap){
			if(type=="1008" || type=="1010"	|| type=="1036"){
				compArr.push(compMap[type]);
			}
		}
	}   
    var html = "";
    for(var i=0;i<compArr.length;i++){
    	html += '<li class="sidebar-li" style="display:none;">' +
       			'<input type="text" readonly="readonly" value="' + compArr[i].name + 
       			'" data-type="' + compArr[i].type + '"/>' +
       			'<button class="btn btn-default btn-xs" draggable="true" ondragstart="Trag.dragComp(event)">' + 
       			'<i class="fa fa-arrows"></i> 拖动</button>' +
       		 	'</li>';            	
    } 
    $("#component").append(html);
}

//列表页面拖拽组件的存储容器，分为隐藏框，搜索条件，列框三类组件
function addListPageContianer(){
	$("#content-wrapper").empty();
	var html = '<label class="control-label">隐藏框</label><div ondrop="Trag.dropListComp(event)" ' + 
			   'ondragover="Trag.allowDrop(event)" class="form-group listPageContianer" id="hiddenDiv"></div>' + 
			   '<label class="control-label">搜索条件</label><div ondrop="Trag.dropListComp(event)" ' + 
			   'ondragover="Trag.allowDrop(event)" class="form-group listPageContianer" id="searchDiv"></div>' + 
			   '<label class="control-label">列框</label><div ondrop="Trag.dropListComp(event)" ' + 
			   'ondragover="Trag.allowDrop(event)" class="form-group listPageContianer" ' + 
			   'style="overflow-x: scroll;" id="columnDiv"></div>';
	$("#content-wrapper").html(html);
}

//初始化表单页面布局
function initLayout(){
	var arr = ["12","6,6","4,8","3,9","2,10","4,4,4","3,3,6","3,3,3,3","4,2,4,2"];
	var html = "";
    for(var i=0;i<arr.length;i++){
    	html += '<li class="sidebar-li">' +
       			'<input type="text" readonly="readonly" value="' + arr[i] + '" />' +
       			'<button class="btn btn-default btn-xs" draggable="true" ondragstart="Trag.dragLayout(event)">' + 
       			'<i class="fa fa-arrows"></i> 拖动</button>' +
       		 	'</li>';            	
    } 
    html += '<li class="sidebar-li">' +
   			'<input type="text" placeholder="自定义布局" value="" />' +
   			'<button class="btn btn-default btn-xs" draggable="true" ondragstart="Trag.dragLayout(event)">' + 
   			'<i class="fa fa-arrows"></i> 拖动</button>' +
   		 	'</li>';
    $("#layout").append(html);
}

/**
 * 初始化页面的布局和组件数据
 */
function initPageLayoutAndComp(){
	var data = Comm.getData("auto/getLayoutAndComponent.do",{pageId:pageId});
	if(data.comps.length>0 || data.rows.length>0 || data.cols.length>0){
		$("#pageType").attr("disabled",true);
	}
	var pageType = $("#pageType").val();
	if(pageType=="1002"){//表单页面
		initFormPageComps(data.rows,data.cols,data.comps)
	} else if(pageType=="1003"){//列表页面
		initListPageComps(data.comps);
	}
	console.log(data);
}

/******		列表页面操作	 ******/
//列表页面组件操作按钮
var listPageCompbuttons = '<button class="btn btn-info btn-xs" draggable="true" ondragstart="Trag.dragListComp(event)">' +
			  			  '<i class="fa fa-arrows"></i></button>' +
			  			  '<button class="btn btn-success btn-xs compEditBtn" style="margin-left:5px;">' + 
			  			  '<i class="fa fa-edit"></i></button>' +
			  			  '<button class="btn btn-danger btn-xs compDeleteBtn" style="margin-left:5px;">' + 
			  			  '<i class="fa fa-trash"></i></button>';

//搜索条件组件操作按钮
var searchCompButtons = '<button class="btn btn-success btn-xs compEditBtn searchComp" style="margin-left:5px;">' + 
						'<i class="fa fa-edit"></i></button>' +
						'<button class="btn btn-danger btn-xs compDeleteBtn searchComp" style="margin-left:5px;">' + 
						'<i class="fa fa-trash"></i></button>';

//隐藏框组件操作按钮
var hiddenCompButtons = '<button class="btn btn-success btn-xs compEditBtn" style="margin-left:5px;">' + 
						'<i class="fa fa-edit"></i></button>' +
						'<button class="btn btn-danger btn-xs compDeleteBtn" style="margin-left:5px;">' + 
						'<i class="fa fa-trash"></i></button>';

//初始化列表页面的组件
function initListPageComps(comps){
	var hiddens = "";	//隐藏框
	var cols = "";		//列框
	var searchs = "";	//搜索条件
	for(var i=0;i<comps.length;i++){
		var comp = comps[i];
		var id = comp.ID;
		var content = comp.CONTENT;
		var dataItemCode = content.dataItemCode;
		if(dataItemCode && dataItemCode.indexOf(".")>-1){			
			dataItemCode = dataItemCode.split(".")[1];
		}
		if(content.componentType=="1010"){
			hiddens += getHiddenHtml(id,dataItemCode);
		} else if(content.componentType=="1008"){
			var columnName = content.columnName?content.columnName:"";
			cols += getColHtml(id,dataItemCode,columnName);
		} else if(content.componentType=="1036"){
			searchs = getSearchHtml(content,id);
		} 
	}
	cols = "<table class='table'><thead><tr>" + cols + "</tr></thead></table>" 
	$("#hiddenDiv").html(hiddens);
	$("#columnDiv").html(cols);
	$("#searchDiv").html(searchs);
	if(searchs){
		$("#searchDiv").before("<div style='float:right;'>" + searchCompButtons + "</div>");
	}
}

//获取隐藏框Html
function getHiddenHtml(id,dataItemCode){
	return '<div class="col-md-3 col-sm-3" data-type="1010" data-id="' + id + '">' +
		   '<div style="float:right;">' + hiddenCompButtons + '</div>' +
		   '<input type="text" value="' + dataItemCode + '" class="form-control" readonly="true" />' +
		   '</div>';
}

//获取列框Html
function getColHtml(id,dataItemCode,columnName){
	return '<th class="colTh" style="padding:0px;" data-type="1008" data-id="' + id + '">' + 
		   '<div style="float:right;">' + listPageCompbuttons + '</div>' +
		   '<input type="text" class="form-control thInput" readonly="true" value="' + columnName + '" />' + 
		   '<input type="text" class="form-control thInput" readonly="true" value="' + dataItemCode + '" /></th>';
}

//获取搜索Html
function getSearchHtml(content,id){
	var html = "<div data-id='" + id + "' data-type='1036'>";
	var labels = ["textLabel","dateSelectLabel","selectLabel","checkboxLabel","radioLabel"]
	var names = ["textName","dateSelectName","selectName","checkboxName","radioName"];
	var types = ["文本框","日期框","下拉框","复选框","单选框"];
	if(content){
		for(var i=0;i<labels.length;i++){
			html += getSearchHtmlByType(content[labels[i]],content[names[i]],types[i]);
		}
	}
	html += "</div>";
	return html;
}

//获取某类搜索的Html
function getSearchHtmlByType(label,name,type){
	var html = "";
	if(label && name){
		var labelArr = label.split("@");
		var nameArr = name.split("@");
		for(var i=0; i<labelArr.length && i<nameArr.length;i++){
			html += '<div class="col-md-3 col-sm-3">' +
					'<label class="control-label">' + labelArr[i] + '</label>' +
					'<label class="control-label" style="font-size:12px;float:right;">' + type + '</label>' +
					'<input class="form-control" readonly="true" value="' + nameArr[i] + '" /></div>'; 
		}
	}
	return html;
}

//新增列框，或拖动列框时：插入列框html代码
function insertColHtml(x,$obj){
	var index = 0;
	var count = $(".colTh").length;
	if(count==0){
		$("#columnDiv").find("tr").append($obj);
	} else{
		var lastThX = $(".colTh").last().find("button").eq(0).offset().left;
		if(x > lastThX){
			$(".colTh").last().after($obj);
			index = $(".colTh").length;
		} else{
			var temp = -100;
			$(".colTh").each(function(i){
				var left = $(this).find("button").eq(0).offset().left;
				if(x>temp && x<left){
					$(this).before($obj);
					index = i;
					return false;
				}else{
					temp = left;
				}
			});
		}
	}	
	return index;
}

//列表页面修改组件的顺序
function updateListCompOrder(){
	var arr = [];
	$("#content-wrapper").find("[data-id]").each(function(){
		arr.push($(this).attr("data-id"));
	});	
	myFun.ajaxExecute("auto/updateListCompOrder.do",{ids:arr.join()},true);
}

/******		表单页面操作	 ******/
//初始化表单页面的组件
function initFormPageComps(rows,cols,comps){
	for(var i=0;i<rows.length;i++){
		var row = rows[i];
		$("#content-wrapper").append(getRow(row.ID)+"</div>");
	}
	for(var i=0;i<cols.length;i++){
		var col = cols[i];
		var id = col.ID;
		var parentId = col.CONTENT.parentId;
		var colWidth = col.CONTENT.proportion;
		var name = col.CONTENT.name;
		$("[data-id='" + parentId + "']").append(getCol(id,colWidth,name));
	}
	for(var i=0;i<comps.length;i++){
		var comp = comps[i];
		var id = comp.ID;
		var dataItemCode = comp.CONTENT.dataItemCode;
		if(dataItemCode && dataItemCode.indexOf(".")>-1){
			dataItemCode = dataItemCode.split(".")[1];
		}
		var componentType = comp.CONTENT.componentType;
		if(componentType != "1010"){
			var layoutId = comp.CONTENT.layoutId;
			var title = comp.CONTENT.title;
			var compName = compMap[componentType].name;
			$("[data-id='" + layoutId + "']").append(getCompContent(compName,componentType,id,title,dataItemCode));
		} else{
			$("#hiddenDiv").append(getHiddenHtml(id,dataItemCode));
		}
	}
}

//根据布局生成Html片段
function getLayout(cols){
	var colArr = cols.split(",");
	var html = getRow("");
	for(var i=0;i<colArr.length;i++){
		html += getCol("",colArr[i],"");
	}
	html += "</div>";
	return html;
}

//获取行Html
function getRow(id){
	id = id?id:"";
	return "<div class='form-group layoutRow' data-id='" + id + "' ondrop='Trag.dropRow(event)' " + 
		   "ondragover='Trag.allowDrop(event)'><span class='rowSpan'>Row</span>" +
	   	   "<button class='btn btn-info btn-xs rowDragBtn' draggable='true' ondragstart='Trag.dragRow(event)'>" + 
	   	   "<i class='fa fa-arrows'></i></button>" +
	   	   "<button class='btn btn-danger btn-xs rowDelBtn'>" + 
	   	   "<i class='fa fa-trash'></i></button>";
}

//获取列Html
function getCol(id,col,name){
	id = id?id:"";
	return "<div class='col-md-" + col + "'>" +   				
		   "<div class='layoutCol' data-colWidth='" + col + "' data-id='" + id + "' data-name='" + name + 
		   "' ondrop='Trag.dropCol(event)' ondragover='Trag.allowDrop(event)'><span class='colSpan'>Col</span></div>"+
		   "<button class='btn btn-info btn-xs colDragBtn' draggable='true' ondragstart='Trag.dragCol(event)'>" + 
		   "<i class='fa fa-arrows'></i></button>" +
		   "<button class='btn btn-danger btn-xs colDelBtn'>" + 
		   "<i class='fa fa-trash'></i></button>" + 
		   "</div>";
}

var formPageCompBtn = "<button class='btn btn-info btn-xs' draggable='true' ondragstart='Trag.dragFormComp(event)'>" + 
					  "<i class='fa fa-arrows'></i></button>" +
					  "<button class='btn btn-success btn-xs compEditBtn' style='margin-left:5px;'>" + 
					  "<i class='fa fa-edit'></i></button>" + 
					  "<button class='btn btn-danger btn-xs compDeleteBtn' style='margin-left:5px;'>" + 
					  "<i class='fa fa-trash'></i></button>";

//获取组件的Html
function getCompContent(compName,compType,id,title,dataItemCode){
	id = id?id:"";	
	title = title?title:"";
	dataItemCode = dataItemCode?dataItemCode:"";
	var html = "<div class='compDiv' data-id='" + id + "' data-type='" + compType + "'>" + 
			   "<span class='colSpan'>" + compName + "</span>" +
			   "<div class='colBtnDiv'>" + formPageCompBtn + "</div>" + 
	    	   "<input class='form-control compInput' value='" + title + "' readonly='true' />" +
	    	   "<input class='form-control compInput' value='" + dataItemCode + "' readonly='true' /></div>";
	return html;
}

function setRowHeight($row){
	var height = 0;
	$row.children("div").each(function(){
		var divHeight = $(this).height();
		if(height < divHeight){
			height = divHeight;
		}
	});
	$row.height(height);
}

/*
 * @param componentType		组件类型
 * @param dynamicPageId		动态页面ID
 * @param componentId		组件ID
 * @param colId				布局ID
 * @param colName			布局名称
 * @param order				顺序
 * @returns
 */
function getUrl(componentType, dynamicPageId, componentId, colId, colName, order){
	var url = basePath + "component/toedit.do?dynamicPageId=" + dynamicPageId + "&componentType=" + componentType;
	if(componentId){
		url += "&componentId=" + componentId;
	}
	if(colId){
		url += "&colId=" + colId;
	}
	if(colName){
		url += "&colName=" + colName;
	}
	if(order){
		url += "&order=" + order;
	}
	return url;
}

//保存或修改布局数据
function saveAndUpdateLayout(){
	var rows = [];
	$(".layoutRow").each(function(i){
		var rowId = $(this).attr("data-id");
		var rowIndex = i+1;
		var cols = [];
		$(this).find(".layoutCol").each(function(j){
			var colId = $(this).attr("data-id");
    		var colIndex = j+1;
    		var colWidth = $(this).attr("data-colWidth") - 0;
    		cols.push({
    			colId:colId?colId:"",
    			colIndex:colIndex,
    			colWidth:colWidth
    		});
		});
		rows.push({
			rowId:rowId?rowId:"",
			rowIndex:rowIndex,
			cols:cols
		});
	});
	var data = Comm.getData("auto/saveAndUpdateLayout.do",{dynamicPageId:pageId,json:JSON.stringify(rows)});
	if(data.length > 0){
		$(".layoutRow").each(function(i){
    		$(this).attr("data-id",data[i].rowId);
    		$(this).find(".layoutCol").each(function(j){
    			$(this).attr("data-id",data[i].cols[j].colId);
    			$(this).attr("data-name",data[i].cols[j].colName);
    		});
    	});
	}    	
}

//修改组件所在的布局
function updateCompLayout(compId,layoutId,layoutName){
	Comm.getData("auto/updateCompLayout.do",{compId:compId,layoutId:layoutId,layoutName:layoutName});
}

function initEvent(){
	//动态页面类型改变事件
	$("#pageType").change(function(){
		initAutoDesign();
	});
	
	//组件编辑按钮
	$("#content-wrapper").on("click",".compEditBtn",function(){
		var id;
		var compType;	
		if(compType=="1017"){
			parent.$(".right-sidebar").css("width","800px");
		} else{
			parent.$(".right-sidebar").css("width","560px");
		}
		if($(this).hasClass("searchComp")){//搜索条件组件
			id = $(this).parent().next().find("[data-id]").attr("data-id");
			compType = $(this).parent().next().find("[data-type]").attr("data-type");
		} else{
			id = $(this).closest("[data-id]").attr("data-id");
			compType = $(this).closest("[data-type]").attr("data-type");
		}
		if(id && compType && $newComp==null){
			var url = getUrl(compType,pageId,id,"","",""); 
			$("#editPage").attr("src",url);  
			$("#commPage").show(100);
		}
	});
	
	//组件删除按钮
	$("#content-wrapper").on("click",".compDeleteBtn",function(){
		var id;
		var $currentComp;
		if($(this).hasClass("searchComp")){//搜索条件组件
			$currentComp = $(this).parent().next().find("[data-id]");
			id = $currentComp.attr("data-id");
			isSerachComp = true;
		} else{
			$currentComp = $(this).closest("[data-id]");
			id = $currentComp.attr("data-id");			
		}
		if(id && $newComp==null){
			Comm.confirm("确认删除？",function(){
				var data = myFun.ajaxExecute("auto/deleteStore.do",{id:id});
				if(data.status==0){
					$currentComp.remove();
					if($(this).hasClass("searchComp")){
						$(this).parent().remove();
					}
					var pageType = $("#pageType").val();
					if(pageType=="1003"){
						updateListCompOrder();
					}
				} else{
					alert("删除失败");
				}
			});		
		}
	});
	
	//删除行布局
	$("#content-wrapper").on("click",".rowDelBtn",function(){
		var $row = $(this).parent();
		var count = $row.find(".compDiv").length;
		if(count > 0){
			Comm.alert("布局中有组件，不能删除");
			return false;
		}
		Comm.confirm("确认删除？",function(){
			$row.remove();
	    	saveAndUpdateLayout();
		});   	
    });
	
	//删除列布局
	$("#content-wrapper").on("click",".colDelBtn",function(){
		var $col = $(this).parent();
		var count = $col.find(".compDiv").length;
		if(count > 0){
			Comm.alert("布局中有组件，不能删除");
			return false;
		}
		Comm.confirm("确认删除？",function(){
			$col.remove();
	    	saveAndUpdateLayout();
		});   	
    });
}

//拖拽
var Trag = {
	allowDrop : function(ev){
		ev.preventDefault();
	},
	
	//拖动组件
	dragComp : function(ev){
		var $target = $(ev.target);
		var compType = $target.prev().attr("data-type");
		var compName = $target.prev().val();
		var obj = {
			type : "dragComp",
			compType : compType,
			compName : compName
		}
		ev.dataTransfer.setData("obj",JSON.stringify(obj));
	},
	
	/**
	 * 列表页面拖拽
	 */
	//拖拽列框
	dragListComp : function(ev){
		var $target = $(ev.target);
		var val = $target.parent().parent().attr("data-id");
		var obj = {
			type : "dragComp",
			compType : "1008",
			compName : "列框",
			compId : val
		}
		ev.dataTransfer.setData("obj",JSON.stringify(obj));
	},
	
	//放入容器
	dropListComp : function(ev){
		ev.preventDefault();
	    var obj = JSON.parse(ev.dataTransfer.getData("obj"));
	    var $target = $(ev.currentTarget);
	    var divId = $target.attr("id");
	    var type = obj.type;
	    var compType = obj.compType;
	    if(type == "dragComp"){
	    	if(divId=="columnDiv" && compType=="1008"){    		
	    		var compId = obj.compId;
	    		var x = ev.pageX;    			
	    		if(compId){//列框调整位置
	    			var $th = $("th[data-id='" + compId + "']");   			
	    			insertColHtml(x,$th);
	    			updateListCompOrder();
	    		} else{//新增列框
	    			if($newComp == null){
		    			var thHtml = getColHtml("","","");
		    			$newComp = $(thHtml);
		    			var index = insertColHtml(x,$newComp);
		    			var order = $("#hiddenDiv").find("[data-id]").length + 1 + index + 1;
		    			var url = getUrl(compType,pageId,"","","",order); 
		    	    	$("#editPage").attr("src",url);  
		    	    	$("#commPage").show();
	    			}
	    		}		    	
	    	} else if(divId=="hiddenDiv" && compType=="1010"){
	    		if($newComp == null){
	    			var hiddenHtml = getHiddenHtml("","");
		    		$newComp = $(hiddenHtml);
		    		var order = $("#hiddenDiv").find("[data-id]").length + 1;
		    		var url = getUrl(compType,pageId,"","","",order); 
	    			$("#hiddenDiv").append($newComp);
	    	    	$("#editPage").attr("src",url);  
	    	    	$("#commPage").show();
	    		}
	    	} else if(divId=="searchDiv" && compType=="1036"){
	    		if($newComp == null){
	    			if($("#searchDiv").html() == ""){
	    				var searchHtml = getSearchHtml("","");
			    		$newComp = $(searchHtml);
			    		var order = $("#hiddenDiv").find("[data-id]").length + 1;
			    		var url = getUrl(compType,pageId,"","","",order); 
		    			$("#searchDiv").append($newComp);
		    	    	$("#editPage").attr("src",url);  
		    	    	$("#commPage").show();
	    			}    			
	    		}
	    	}	       	
	    } 
	},
	
	/**
	 * 表单页面拖拽
	 */	
	//拖动布局	
	dragLayout : function(ev){
		var $target = $(ev.target);
		var val = $target.prev().val();
		var obj = {
			type : "dragLayout",
			cols : val
		}
		ev.dataTransfer.setData("obj",JSON.stringify(obj));
	},
	
	//拖动行
	dragRow : function(ev){
		var $target = $(ev.target);
		var val = $target.parent().attr("data-id");
		var obj = {
			type : "dragRow",
			rowId : val
		}
		ev.dataTransfer.setData("obj",JSON.stringify(obj));
	},
	
	//拖动列
	dragCol : function(ev){
		var $target = $(ev.target);
		var val = $target.prev().attr("data-id"); 
		var obj = {
			type : "dragCol",
			colId : val
		}
		ev.dataTransfer.setData("obj",JSON.stringify(obj));
	},

	//拖拽表单组件
	dragFormComp : function(ev){
		var $target = $(ev.target);
		var id = $target.parent().parent().attr("data-id");
		var compType = $target.parent().parent().attr("data-type");
		var obj = {
			type : "dragFormComp",
			compType : compType,
			compName : compMap[compType],
			compId : id
		}
		ev.dataTransfer.setData("obj",JSON.stringify(obj));
	},
	
	//拖拽到主容器
	dropMain : function(ev){
	    ev.preventDefault();
	    var obj = JSON.parse(ev.dataTransfer.getData("obj"));
	    if(obj.type == "dragLayout"){
	    	var cols = obj.cols;
	    	if(cols){
	        	var html = getLayout(cols);
	            $("#content-wrapper").append(html);
	            saveAndUpdateLayout();
	        } 
	    } else if(obj.type == "dragRow"){
	    	var rowId = obj.rowId;
	    	var $obj = $("[data-id='" + rowId + "']");
	    	var y = ev.pageY;
	    	var temp = -100;
	    	var lastRowY = $(".layoutRow").last().offset().top;
	    	if(y > lastRowY){
	    		$(".layoutRow").last().after($obj);
	    	} else{
	    		$(".layoutRow").each(function(){
	        		var top = $(this).offset().top;
	        		if(y<=top && y>temp){
	        			$(this).before($obj);
	        			return false;
	        		}else{
	        			temp = top;
	        		}     		
	        	}); 
	    	}    
	    	saveAndUpdateLayout();
	    } else if(obj.type=="dragComp"){
	    	var compType = obj.compType;
	    	if(compType=="1010"){//隐藏框
	    		if($newComp == null){    			
	    			var hiddenHtml = getHiddenHtml("","");
		    		$newComp = $(hiddenHtml);
		    		var order = $("#hiddenDiv").find("[data-id]").length + 1;
		    		var url = getUrl(compType,pageId,"","","",order); 
	    			$("#hiddenDiv").append($newComp);
	    	    	$("#editPage").attr("src",url);  
	    	    	$("#commPage").show();
	    		}
	    	}
	    }        
	},
	
	//拖拽到行内
	dropRow : function(ev){
	    ev.preventDefault();
	    var obj = JSON.parse(ev.dataTransfer.getData("obj"));
	    if(obj.type == "dragCol"){
	    	var colId = obj.colId;
	    	var $obj = $("[data-id='" + colId + "']").parent();
	    	var x = ev.pageX;
	    	var temp = -100;
	    	var $cols = $obj.parent().find(".layoutCol");
	    	if($cols.length != 0){
	    		var lastColX = $cols.last().offset().left;
		    	if(x > lastColX){
		    		$cols.last().parent().after($obj);
		    	} else{
		    		$cols.each(function(){
		        		var left = $(this).offset().left;
		        		if(x<=left && x>temp){
		        			$(this).parent().before($obj);
		        			return false;
		        		}else{
		        			temp = left;
		        		}     		
		        	}); 
		    	}
	    	} 	    
	    	saveAndUpdateLayout();
	    }          
	},
	 	
	//拖拽到列中
	dropCol : function(ev){
	    ev.preventDefault();
	    var obj = JSON.parse(ev.dataTransfer.getData("obj"));
	    var $target = $(ev.currentTarget);
	    var colId = $target.attr("data-id");
    	var colName = $target.attr("data-name");
	    if(obj.type == "dragComp" && obj.compType!="1010"){
	    	if($newComp == null){
	    		var compName = obj.compName;
		    	var compType = obj.compType;
		    	if(compType=="1017"){
					parent.$(".right-sidebar").css("width","800px");
				} else{
					parent.$(".right-sidebar").css("width","560px");
				}
		    	var compHtml = getCompContent(compName,compType);
		    	$newComp = $(compHtml);
		    	$target.append($newComp);   
		    	var url = getUrl(compType,pageId,null,colId,colName);        	
		    	$("#editPage").attr("src",url);  
		    	$("#commPage").show(100);
	    	}
	    } else if(obj.type == "dragFormComp"){
	    	var compId = obj.compId;
	    	var $comp = $("[data-id='" + compId + "']");
	    	var $compRow = $comp.parent().parent().parent();
	    	$target.append($comp);
	    	setRowHeight($compRow);
	    	updateCompLayout(compId,colId,colName);
	    }
	    var $row = $target.parent().parent();
    	setRowHeight($row);
	},
};