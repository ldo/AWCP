$(function(){
	var dynamicPageId = $("#dynamicPageId").val();
	loadPowerTable(dynamicPageId);
	
	//新增权限组
	$("#addAuthorityGroup").click(function(){
		addAuthorityGroup(dynamicPageId);
		return false;
	});
	
	//删除权限组
	$("#deleteAuthority").click(function(){
		var count = $("#powerTable").find("tr.selected").length;
		if(count > 0){
			removeAuthority(dynamicPageId);
			return false;
		}else{
			alert("请选择权限组操作");
			return false;
		}
	});
	
	//权限配置
	$("#addAuthorityValue").click(function(){
		var count = $("#powerTable").find("tr.selected").length;
		if(count==1){	  
			var componentId = $("tr.selected").attr("data-id");
			addAuthorityValue(dynamicPageId,componentId);
			return false;
		}else if(count>1){
			alert("请选择一组权限组操作");
			return false;
		}else{
			alert("请选择权限组操作");
			return false;
		}
	});
});

//加载权限组
function loadPowerTable(dynamicPageId) {
	$.ajax({
		type : "GET",
		url : basePath + "authority/getGroupListByPageId.do",
		data : "dynamicPageId=" + dynamicPageId + "&pageSize=9999",
		async : false,
		success : function(data) {
			var html = "";
			$.each(data, function(idx, item) {
				var id = item.id;
				var name = item.name;
				var lastTime = item.lastupdateTime;
				var time = item.createTime;
				var bakInfo = item.bakInfo;
				var order = item.order;
				var lastupdateTime = new Date(lastTime);
				var createTime = new Date(time);
				var description = item.description;
				description = description?description:"";
				html += "<tr data-id='" + id + "'>" +
					"<td></td>" +
					"<td><a href='javascript:void(0);' onclick='updateAuthorityGroup(\"" + id + "\")'>" + name + 
					"</a></td>" +
					"<td>" + bakInfo + "</td>" +
					"<td>" + createTime.Format("yyyy-MM-dd hh:mm") + "</td>" +
					"<td>" + lastupdateTime.Format("yyyy-MM-dd hh:mm") + "</td>" +
					"<td>" + description + "</td>" +
					"<td width='50'>" + order + "</td>" +
					"</tr>";
				
			});
			$("#powerTable").bootstrapTable(("destroy"));
			$("#powerTable tbody").html(html);	
			$("#powerTable").bootstrapTable({});
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});
}

//新增权限组
function addAuthorityGroup(dynamicPageId){	
	var urlStr = "formdesigner/page/authgroup/addAuthorith.jsp";		
	var postData = {};	
	postData.dynamicPageId = dynamicPageId;
	top.dialog({
		id: 'add-dialog' + Math.ceil(Math.random()*10000),
		title: '载入中...',
		url: urlStr,
		data:postData,
		onclose: function () {
			if (this.returnValue) {
				var ret = this.returnValue;
				loadPowerTable(dynamicPageId);			
			}
		}		
	}).showModal();
	return false;
}

//修改权限组
function updateAuthorityGroup(id){		
	var urlStr = "formdesigner/page/authgroup/updateAuthorith.jsp";
	var dynamicPageId = $("#dynamicPageId").val();	
	var postData = {};	
	postData.id = id;	
	top.dialog({
		id: 'add-dialog' + Math.ceil(Math.random()*10000),
		title: '载入中...',
		url: urlStr,
		data:postData,
		onclose: function () {
			if (this.returnValue) {
				var ret = this.returnValue;
				loadPowerTable(dynamicPageId);		
			}
		}		
	}).showModal();
	return false;
}
	
//删除权限组
function removeAuthority(dynamicPageId){
	var _selects = new Array();
	$("tr.selected").each(function(){
		var value = $(this).attr("data-id");
		_selects.push(value);
	});
	$.ajax({
		url:"authority/deleteByAjax.do",
		type:"POST",
		async:false,
		data:{_selects:_selects.join(",")},
		success:function(ret){
			if("1"==ret){
				loadPowerTable(dynamicPageId);
			}else{
				alert("删除失败！");
			}			
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) { 
              alert(errorThrown); 
	    }
	});
	return false;
}
	
//配置权限组权限
function addAuthorityValue(dynamicPageId,pageId){		
	var urlStr="formdesigner/page/authgroup/addAuthorityValue.jsp";	
	var postData={};	
	postData.dynamicPageId = dynamicPageId;
	postData.componentId = pageId;	
	top.dialog({
		id: 'add-dialog' + Math.ceil(Math.random()*10000),
		title: '载入中...',
		url: urlStr,
		height:500,
		width:800,
		data:postData,
		onclose: function () {
			if (this.returnValue) {
				var ret = this.returnValue;
				loadPowerTable(dynamicPageId);	
			}
		}		
	}).showModal();
	return false;
}	
	
//格式化日期
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
