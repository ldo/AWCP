<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="btn-group" style="margin-bottom:10px;">	
	<input type="hidden" value="${vo.id}" id="dynamicPageId"/>
	<button class="btn btn-sm btn-primary" id="addAuthorityGroup">
		<i class="fa fa-plus"></i> 添加
	</button>
	<button class="btn btn-sm btn-danger" id="deleteAuthority">
		<i class="fa fa-remove"></i> 删除
	</button>
	<button class="btn btn-sm btn-info" id="addAuthorityValue">
		<i class="fa fa-edit"></i>权限配置
	</button>
</div>
<table class="table table-hover" id="powerTable">
	<thead>
		<tr>
			<th data-width="30" data-checkbox="true"></th>
			<th>名称</th>
			<th>创建人</th>
			<th>创建时间</th>
			<th>最后更新时间</th>
			<th>描述</th>
			<th width="50">序号</th>
		</tr>
	</thead>
	<tbody id="powerTable_body">
	</tbody>
</table>

