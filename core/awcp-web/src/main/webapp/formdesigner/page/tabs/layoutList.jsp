<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="btn-group">
	<button type="button" class="btn btn-sm btn-primary" id="addLayoutBtn" onclick="addLayout(this);">
		<i class="fa fa-plus"></i> 新增
	</button>
	<button class="btn btn-sm btn-danger" id="deleteLayout">
		<i class="fa fa-remove"></i> 删除
	</button>
	<button type="button" class="btn btn-sm btn-primary" id="addNewLayoutBtn" onclick="addChildLayout(this);">
		<i class="fa fa-plus"></i> 新增子布局
	</button>
	<button type="button" class="btn btn-sm btn-success" id="addQuickLayoutBtn" onclick="addQuickLayout(this);">
		<i class="fa fa-plus"></i> 快捷新增
	</button>
	<button type="button" class="btn btn-sm btn-info" id="merageLayout">
		<i class="fa fa-navicon"></i> 合并布局
	</button>
	<button type="button" class="btn btn-sm btn-success" id="copyLayout">
		<i class="fa fa-copy"></i> 复制布局
	</button>
	<button type="button" class="btn btn-sm btn-warning" id="refreshLayoutOrder">
		<i class="fa fa-gear"></i> 重置序号
	</button>
	<button type="button" class="btn btn-sm btn-success" id="batchModifyProportionBtn" onclick="batchModifyProportion(this);">
		<i class="fa fa-edit"></i>批量修改布局占比
	</button>
</div>

<div class="componentTable" contenteditable="false" style="margin-top:10px;">
	<table class="table table-bordered" id="layoutTable" align="left">
		<thead>
			<tr>
				<th><input type="checkbox" id="checkAllLayout" /></th>
				<th>名称</th>
				<th>类型</th>
				<th>占比</th>
				<th>排序</th>
			</tr>
		</thead>
		<tbody id="layoutt">
		</tbody>
	</table>
</div>
