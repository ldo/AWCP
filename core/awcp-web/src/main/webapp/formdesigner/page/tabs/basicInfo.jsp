<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<input type="hidden" value="${vo.dataJson}" name="dataJson" id="modelJsonArray" >		
<input type="hidden" name="id" value="${vo.id}" id="id">
<div class="form-group">
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label required">名称</label>
			<input type='text' name='name' id='name' value="${vo.name}"	class='form-control' />
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label required">类型</label>		
			<select id="pageType" class="form-control" tabindex="2" name="pageType">
				<c:choose>
	       	 		<c:when test="${vo.pageType == '1002' }">
						<option value="1002" selected="selected">表单页面</option>
						<option value="1003">列表页面</option>
	       	 		</c:when>
	       	 		<c:otherwise>
						<option value="1002">表单页面</option>
						<option value="1003" selected="selected">列表页面</option>
	       	 		</c:otherwise>
	       	 	</c:choose>
			</select>
		</div>
	</div>	
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">所属模块</label>
			<select name='modular' id='modular' class='form-control'>
	    		<c:forEach var="modular" items="${modulars }">
	    			<option value="${modular.ID }">${modular.modularName }</option>
	    		</c:forEach>
	    	</select> 
		</div>
	</div>
</div>

<div class="form-group listPageConfig" style="display: none;">
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">分页</label>
			<div class="input-group">
				<c:choose>
					<c:when test="${vo.isLimitPage == '1'}">
						<label class="radio-inline" style="width: 50px;"><input name="isLimitPage" type="radio" checked="checked" value="1">是</label>
						<label class="radio-inline" style="width: 50px;"><input name="isLimitPage" type="radio"  value="0">否</label>
					</c:when>
					<c:otherwise>
						<label class="radio-inline" style="width: 50px;"> <input name="isLimitPage" type="radio" value="1">是</label>
						<label class="radio-inline" style="width: 50px;"><input name="isLimitPage" type="radio" checked="checked" value="0">否</label>
					</c:otherwise>
				</c:choose>
				<input type='text' name='pageSize' id='pageSize' value="${vo.pageSize}"	style="width:150px;"/> 条记录/页
			</div>
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">显示序号</label>	
			<div class="input-group">	
				<c:choose>
					<c:when test="${vo.showReverseNum == '1'}">
						<label class="radio-inline" style="width: 50px;"><input name="showReverseNum" type="radio" checked="checked" value="1">是</label>
						<label class="radio-inline" style="width: 50px;"><input name="showReverseNum" type="radio" value="0">否</label>
					</c:when>
					<c:otherwise>
						<label class="radio-inline" style="width: 50px;"><input name="showReverseNum" type="radio" value="1">是</label>
						<label class="radio-inline" style="width: 50px;"><input name="showReverseNum" type="radio" checked="checked" value="0">否</label>
					</c:otherwise>
				</c:choose>
				<input type='text' name='reverseNumMode' id='reverseNumMode' value="${vo.reverseNumMode}" style="width:200px;"
					placeholder="序号格式化模式（如：0000）"/>
			</div>
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">序号排序方式</label>
			<div class="input-group">
				<c:choose>
					<c:when test="${vo.reverseSortord == '1'}">
						<label class="radio-inline"><input name="reverseSortord" type="radio" checked="checked" value="1">降序</label>
						<label class="radio-inline"><input name="reverseSortord" type="radio" value="0">升序</label>
					</c:when>
					<c:otherwise>
						<label class="radio-inline"><input name="reverseSortord" type="radio" value="1">降序</label>
						<label class="radio-inline"><input name="reverseSortord" type="radio" checked="checked" value="0">升序</label>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label required">模板库</label>
			<select name='templateId' id='templateId' class='form-control'>
	    		<c:forEach var="template" items="${templates }">
	    			<option value="${template.id }">${template.file_name }</option>
	    		</c:forEach>
	    	</select>
		</div>
	</div>
	
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">样式库</label>
			<select name='styleId' id='styleId' class='form-control'>
	    		<c:forEach var="style" items="${styles }">
	    			<option value="${style.ID }">${style.NAME }</option>
	    		</c:forEach>
	    	</select>	
				
		</div>
	</div>
	
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">记录操作</label>
			<div class="input-group">
				<c:choose>
		     	 	<c:when test="${vo.isLog=='1'}">
						<label class="radio-inline"> <input name="isLog" type="radio" checked="checked" value="1" >是</label>
						<label class="radio-inline"><input name="isLog" type="radio"  value="0" >否</label>
		     	 	</c:when>
		     	 	<c:otherwise>
						<label class="radio-inline"> <input name="isLog" type="radio" value="1"  >是</label>
						<label class="radio-inline"><input name="isLog" type="radio"  checked="checked" value="0" >否</label>
		     	 	</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</div>	

<div class="form-group">
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">只读脚本</label>
			<textarea name='readonlyScript' id='readonlyScript' rows='4' class='form-control' style="resize: vertical;">${vo.readonlyScript}</textarea>
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">禁用脚本</label>
			<textarea name='disabledScript' id='disabledScript' rows='4' class='form-control' style="resize: vertical;">${vo.disabledScript}</textarea>
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">隐藏脚本</label>		
			<textarea name='hiddenScript' id='hiddenScript' rows='4' class='form-control' style="resize: vertical;">${vo.hiddenScript}</textarea>
		</div>
	</div>		
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">加载前脚本</label>
			<textarea name='preLoadScript' id='preLoadScript' rows='4' class='form-control' style="resize: vertical;">${vo.preLoadScript}</textarea>
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">加载后脚本</label>
			<textarea name='afterLoadScript' id='afterLoadScript' rows='4' class='form-control' style="resize: vertical;">${vo.afterLoadScript}</textarea>
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-4 col-sm-4">
			<label class="control-label">描述</label>		
			<textarea name='description' id='description' rows='4' class='form-control' style="resize: vertical;">${vo.description}</textarea>
		</div>
	</div>		
</div>



