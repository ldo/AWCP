<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style type="text/css">
   	.sidebar-ul{
   		padding-top: 16px;
   		padding-left: 16px;
   	}
   	.sidebar-li{
   		list-style: none;
   		color:white;
   		padding-bottom: 6px;
   	}
   	.sidebar-li input{
	    height: 22px;
	    width: 120px;
	    border-radius: 4px;
	    color: black;
	    background-color: #ecf0f5;
   	}
   	
   	.sidebar-li button{
   		margin-left: 12px;
   	}
   	
   	.layoutRow{
   		border: 1px solid #ccc;
   		padding: 29px 9px 9px 9px;min-height: 90px;
   		background-color: #ecf0f5;  
   		position: relative;		
   	}
   	
   	.layoutCol{
		border: 1px solid #ccc;
		min-height: 50px;
		background-color: white;
		position: relative;		
		padding: 24px 0px 0px 6px;
	}
	
	.rowSpan{
		position: absolute;
		left: 0px;
		top: 0px;
	}
	
	.colSpan{
		position: absolute;
		left: 0px;
		top: 0px;
		line-height: 28px;
    	font-size: 12px;
	}
	
	.rowDragBtn{
		position: absolute;
		right: 30px;
		top: 0px;
	}
	
	.rowDelBtn{
		position: absolute;
		right: 0px;
		top: 0px;
	}
	
	.colDragBtn{
		position: absolute;
		right: 45px;
		top: 0px;
	}
	
	.colDelBtn{
		position: absolute;
		right: 15px;
		top: 0px;
	}
	
	.colBtnDiv{
		float: right;
    	position: absolute;
    	right: 0;
    	top: 0;
	}
	
	.compDiv{
		position: relative;
	    width: 210px;
	    height: 90px;
	    padding-top: 26px;
	    display: inline-block;
    	margin-right: 2px;
    	max-width: 98%;
	}
	
	.compInput{
		padding:2px;
		height:28px;
	}
	
	.right-sidebar{
		top: 0;
		right: 0;
		padding-top: 50px;
		min-height: 100%;
		width: 560px; 
		z-index: 810;
		position:fixed;
		display: none;
	}
	
	.listPageContianer{
		min-height: 80px;
   		border: 1px #ccc solid;
   		padding: 10px 0px;
	}
	
	.thInput{
		padding:2px;
		min-width:100px;
		font-size:12px;
	}
</style>
<div class="wrapper">
   	<!-- Mian Sidebar -->
    <aside class="main-sidebar" style="padding-top:0px;">
        <section class="sidebar" style="background-color: darkseagreen;min-height:600px;">				
            <ul class="sidebar-ul" id="layout">
	       		<li class="sidebar-li headLi"><i class="fa fa-plus-square"></i> 布局设置</li>
	       	</ul>
	       	<ul class="sidebar-ul" id="component">
	       		<li class="sidebar-li headLi"><i class="fa fa-plus-square"></i> 组件</li>
	       	</ul>
        </section>
    </aside>

    <!-- Content Wrapper  -->
    <div class="content-wrapper" id="content-wrapper" style="min-height: 600px;padding: 10px 30px;" 
    	ondrop="Trag.dropMain(event)" ondragover="Trag.allowDrop(event)">    	    		
    </div>
    
    <aside id="commPage" class="right-sidebar">
    	<section class="sidebar" >	
    		<div class="row" style="margin-bottom: 10px;min-height: 720px;">						
				<div class="col-md-12 col-lg-12">
					<iframe src="" id="editPage" name="editPage" allowtransparency="yes" scrolling="auto" 
						style="display: inline;border: 1px solid #f4f4f4;" width="100%" height="720px;" frameborder="no"></iframe>
				</div>
			</div>		    	
    	</section>
    </aside>	
</div>
