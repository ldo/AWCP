package cn.org.awcp.formdesigner.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.core.common.exception.MRTException;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.Criteria;
import cn.org.awcp.core.domain.QueryChannelService;
import cn.org.awcp.core.utils.BeanUtils;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.formdesigner.application.service.FormdesignerService;
import cn.org.awcp.formdesigner.application.service.StoreService;
import cn.org.awcp.formdesigner.application.vo.DynamicPageVO;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.formdesigner.core.domain.DynamicPage;
import cn.org.awcp.formdesigner.core.domain.PfmTemplate;
import cn.org.awcp.formdesigner.core.domain.Store;
import cn.org.awcp.formdesigner.core.domain.design.context.act.PageAct;
import cn.org.awcp.formdesigner.core.engine.FreeMarkers;
import cn.org.awcp.formdesigner.core.parse.bean.PageActBeanWorker;
import cn.org.awcp.unit.service.PunResourceService;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.unit.vo.PunUserBaseInfoVO;

@Service
public class FormdesignerServiceImpl implements FormdesignerService {
	private static final Logger logger = LoggerFactory.getLogger(FormdesignerService.class);

	@Autowired
	private QueryChannelService queryChannel;

	@Autowired
	@Qualifier("punResourceServiceImpl")
	private PunResourceService punResourceService;

	@Autowired
	@Qualifier("storeServiceImpl")
	private StoreService storeService;

	@Override
	public DynamicPageVO findById(String id) throws MRTException {
		DynamicPage dp = DynamicPage.get(DynamicPage.class, id);
		DynamicPageVO vo = BeanUtils.getNewInstance(dp, DynamicPageVO.class);
		return vo;
	}

	@Override
	public void saveOrUpdate(DynamicPageVO dpvo) throws MRTException {
		DynamicPage dp = BeanUtils.getNewInstance(dpvo, DynamicPage.class);
		Object obj = SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER);
		Date now = new Date();
		if (obj instanceof PunUserBaseInfoVO) {
			PunUserBaseInfoVO user = (PunUserBaseInfoVO) obj;
			if (StringUtils.isBlank(dpvo.getId())) { // 新增
				dp.setCreatedUser(user.getName());
				dp.setCreated(now);
			}
			dp.setUpdatedUser(user.getName());
		}
		dp.setUpdated(now);
		dp.save();
		dpvo.setId(dp.getId());
	}

	@Override
	public void updateWorkflowInfo(DynamicPageVO vo) throws MRTException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", vo.getId());
		params.put("workflowNodeInfo", vo.getWorkflowNodeInfo());

		// queryChannel.excuteMethod(DynamicPage.class, "updateWorkflowInfo",
		// params);
		DynamicPage.getRepository().executeUpdate("updateWorkflowInfo", params, DynamicPage.class);

	}

	@Override
	public void updateModelInfo(DynamicPageVO vo) throws MRTException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", vo.getId());
		params.put("dataJson", vo.getDataJson());
		DynamicPage.getRepository().executeUpdate("updateModelInfo", params, DynamicPage.class);
	}

	@Override
	public List<DynamicPageVO> findAll() {
		BaseExample example = new BaseExample();
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			example.createCriteria().andEqualTo("SYSTEM_ID", system.getSysId());
		}
		PageList<DynamicPage> list = queryChannel.selectPagedByExample(DynamicPage.class, example, 1, Integer.MAX_VALUE,
				null);
		PageList<DynamicPageVO> resultVo = new PageList<DynamicPageVO>(list.getPaginator());
		for (DynamicPage mm : list) {
			resultVo.add(BeanUtils.getNewInstance(mm, DynamicPageVO.class));
		}
		return resultVo;
	}

	@Override
	public PageList<DynamicPageVO> queryPagedResult(String queryStr, Map<String, Object> params, int currentPage,
			int pageSize, String sortString) {
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			params.put("systemId", system.getSysId());
		}
		PageList<DynamicPage> groups = queryChannel.queryPagedResult(DynamicPage.class, queryStr, params, currentPage,
				pageSize, sortString);
		List<DynamicPageVO> tmp = new ArrayList<DynamicPageVO>();
		for (DynamicPage group : groups) {
			tmp.add(BeanUtils.getNewInstance(group, DynamicPageVO.class));
		}
		PageList<DynamicPageVO> vos = new PageList<DynamicPageVO>(tmp, groups.getPaginator());
		groups.clear();
		return vos;
	}

	@Override
	public PageList<DynamicPageVO> selectPagedByExample(BaseExample example, int currentPage, int pageSize,
			String sortString) {
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			if (example.getOredCriteria().size() > 0) {
				example.getOredCriteria().get(0).andEqualTo("SYSTEM_ID", system.getSysId());
			} else {
				example.createCriteria().andEqualTo("SYSTEM_ID", system.getSysId());
			}
		}
		PageList<DynamicPage> list = queryChannel.selectPagedByExample(DynamicPage.class, example, currentPage,
				pageSize, sortString);
		PageList<DynamicPageVO> vos = new PageList<DynamicPageVO>(list.getPaginator());
		for (DynamicPage dp : list) {
			vos.add(BeanUtils.getNewInstance(dp, DynamicPageVO.class));
		}
		list.clear();
		return vos;
	}

	public void delete(List<String> ids) throws MRTException {
		for (String id : ids) {
			DynamicPage dp = DynamicPage.get(DynamicPage.class, id);
			dp.remove();
		}
	}

	@Override
	public List<DynamicPageVO> queryResult(String queryStr, Map<String, Object> params) {
		List<DynamicPage> result = queryChannel.queryResult(DynamicPage.class, queryStr, params);
		List<DynamicPageVO> resultVO = new ArrayList<DynamicPageVO>();
		for (DynamicPage dd : result) {
			resultVO.add(BeanUtils.getNewInstance(dd, DynamicPageVO.class));
		}
		result.clear();
		return resultVO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.szcloud.framework.formdesigner.application.service.
	 * FormdesignerService#getWfByStartNode(java.lang.Long)
	 */
	@Override
	public String getWfByStartNode(String pageId) {
		// [{workId,workName}]
		JSONArray rtn = new JSONArray();
		try {
			DynamicPage page = DynamicPage.get(DynamicPage.class, pageId);
			if (null != page) {
				String jsonStr = page.getWorkflowNodeInfo();
				if (!StringUtils.isEmpty(jsonStr)) {
					JSONObject nodes = JSON.parseObject(jsonStr);
					Collection<Object> objects = nodes.values();
					for (Object n : objects) {
						JSONObject tmp = (JSONObject) n;
						if (tmp.getIntValue("priority") == 1) {
							JSONObject workflow = new JSONObject();
							workflow.put("workId", tmp.getString("workflowId"));
							workflow.put("workName", tmp.getString("workflowName"));
							rtn.add(workflow);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rtn.toJSONString();
	}

	/**
	 * 发布页面
	 */
	public DynamicPageVO publish(DynamicPageVO vo) {
		String disStr = generateDisTemplateByPageId(vo);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("templateContext", disStr);
		params.put("id", vo.getId());
		queryChannel.excuteMethod(DynamicPage.class, "updateTemplateContext", params);
		return findById(vo.getId());
	}

	private String generateDisTemplateByPageId(DynamicPageVO vo) {
		logger.debug("generateDisTemplateByPageId {} [id : {}]", vo.getName(), vo.getId());
		Map<String, Object> root = new HashMap<String, Object>();
		BaseExample example = new BaseExample();
		example.createCriteria().andEqualTo("dynamicPage_id", vo.getId());
		List<Store> children = Store.selectByExample(Store.class, example);						//该页面所有组件
		List<JSONObject> mainLayouts = new ArrayList<JSONObject>();	
		List<JSONObject> hiddenComs = new ArrayList<JSONObject>();								//隱藏框組件
		List<JSONObject> coms = new ArrayList<JSONObject>();									//列表页面组件	
		Map<String, List<JSONObject>> components = new HashMap<String, List<JSONObject>>();		//表单页面组件	
		List<PageAct> pageActsList = new ArrayList<PageAct>();					//按钮
		boolean hasRowButton = false;											//是否有表格行按钮
		boolean hasSearchComponent = false;										//是否有搜索组件
		Map<String, JSONObject> layoutMap = new HashMap<String, JSONObject>();	//布局
		Map<String, Long> pageActPermission = new HashMap<String, Long>();		//按钮权限
		// key:componentId value:对应的校验list<JSONObject>
		Map<String, List<JSONObject>> valdatorsMap = new HashMap<String, List<JSONObject>>();	//组件校验
		
		if (children != null && children.size() > 0) {
			for (int i = 0; i < children.size(); i++) {
				Store o = children.get(i);
				if (o.getCode().indexOf(StoreService.LAYOUT_CODE) != -1) {//布局
					JSONObject c = JSON.parseObject(o.getContent());
					layoutMap.put(c.getString("pageId"), c);
				} else if (o.getCode().indexOf(StoreService.COMPONENT_CODE) != -1) {//组件
					JSONObject c = JSONObject.parseObject(o.getContent()); 
					if (c != null) {
						logger.debug("start format component {} [id : {}] ", c.getString("name"),c.getString("pageId"));
						//对于包含页面组件，特殊处理，根据包含的页面，解析后得到要发布的模板string，修改数据源dataItemCode 以及 组件id，组件name
						if ("1012".equals(c.getString("componentType"))) {//包含组件框
							String relatePageId = c.getString("relatePageId");
							logger.debug("start contain component {} [id : {}]  relatedPageId is {}",
									c.getString("name"), c.getString("pageId"), relatePageId);
							DynamicPageVO relaPageVO = findById(relatePageId);
							String relaPageStr = generateDisTemplateByPageId(relaPageVO);
							JSONArray relComArray = c.getJSONArray("configures");
							if (relComArray != null && relComArray.size() > 0) {
								for (int k = 0; k < relComArray.size(); k++) { // 遍历更改包含页面中组件的dataItemCode，pageId，name
									JSONObject relCom = (JSONObject) relComArray.get(k);
									String relComId = relCom.getString("pageId");
									Store relComStore = Store.get(Store.class, relComId);
									if (relComStore != null) {
										JSONObject relComJson = JSON.parseObject(relComStore.getContent());
										String oraignDataCode = relComJson.getString("dataItemCode");
										String newDataCode = relCom.getString("dataItemCode");
										if (newDataCode == null || newDataCode == null || relaPageStr == null) {
											logger.debug(c.getString("pageId") + "name :  " + c.getString("name")
													+ "oraignDataCode   :" + oraignDataCode + "newDataCode:   "
													+ newDataCode + "componentId + " + relComStore.getId());
										}
										// 如果原片段页面未配置数据源，则执行添加操作，如果原片段页面配置了数据源，则执行替换操作
										relaPageStr = relaPageStr.replace(oraignDataCode, newDataCode);
										// 包含页面的组件pageId加前缀
										relaPageStr = relaPageStr.replace(relCom.getString("pageId"),
												c.getString("name") + "_" + relCom.getString("pageId")); 
										// 包含页面的组件name加前缀
										relaPageStr = relaPageStr.replace(relComJson.getString("name"),
												c.getString("name") + "_" + relComJson.getString("name"));
									}
								}
							}
							if (relaPageStr.indexOf("<--MYLayoutAndCom-->") == -1) {
								logger.debug(relaPageVO.getName() + " 模版没有选择正确");
							}
							String layoutAndCom = relaPageStr.substring(
									relaPageStr.indexOf("<--MYLayoutAndCom-->") + "<--MYLayoutAndCom-->".length(),
									relaPageStr.indexOf("<--MYLayoutAndCom/-->"));
							String pageJScript = relaPageStr.substring(
									relaPageStr.indexOf("<--MYpageJScript-->") + "<--MYpageJScript-->".length(),
									relaPageStr.indexOf("<--MYpageJScript/-->"));
							String pageActScript = relaPageStr.substring(
									relaPageStr.indexOf("<--MYpageActScript-->") + "<--MYpageActScript-->".length(),
									relaPageStr.indexOf("<--MYpageActScript/-->"));
							String pageValidate = relaPageStr.substring(
									relaPageStr.indexOf("<--MYvalidate-->") + "<--MYvalidate-->".length(),
									relaPageStr.indexOf("<--MYvalidate/-->"));
							c.put("layoutAndCom", layoutAndCom);
							c.put("pageJScript", pageJScript);
							c.put("pageActScript", pageActScript);
							c.put("pageValidate", pageValidate);
						} else if("1010".equals(c.getString("componentType"))){
							hiddenComs.add(c);
						} else if("1036".equals(c.getString("componentType"))){
							hasSearchComponent = true;
						}
						String layoutId = c.getString("layoutId");					
						// 存入components中
						if(layoutId != null){
							if (components.get(layoutId) != null) {
								components.get(layoutId).add(c);
							} else {
								List<JSONObject> list = new ArrayList<JSONObject>();
								list.add(c);
								components.put(layoutId, list);
							}
						}
						coms.add(c);
						logger.debug("组件名称：" + c.getString("name") + "===组件layoutID" + c.getString("layoutId"));
						// 存组件对应的校验
						String validatJson = c.getString("validatJson");
						if (StringUtils.isNotBlank(validatJson)) {
							List<JSONObject> valdators = new ArrayList<JSONObject>();
							String[] validateArray = validatJson.split(",");
							for (int k = 0; k < validateArray.length; k++) {
								Store v = Store.get(Store.class, validateArray[k]);
								if (v != null) {
									JSONObject tmp = JSONObject.parseObject(v.getContent());
									logger.debug(tmp.getString("clientScript"));
									tmp.put("clientScript",StringEscapeUtils.unescapeHtml4(tmp.getString("clientScript")));
									tmp.put("callback",StringEscapeUtils.unescapeHtml4(tmp.getString("callback")));
									valdators.add(tmp);
								}
							}
							valdatorsMap.put(c.getString("name"), valdators);
						}
						logger.debug("end format component {} [id : {}] ", c.getString("name"), c.getString("pageId"));
					}
				} else if (o.getCode().indexOf(StoreService.PAGEACT_CODE) != -1) {//按钮
					PageAct c = PageActBeanWorker.convertConfToAct(o.getContent());
					if (c != null) {
						pageActsList.add(c);
						if("1".equals(c.getPlace())){
							hasRowButton = true;
						}
						Long resourceId = punResourceService.getResourceIdByRelateId(c.getPageId(), "3");
						pageActPermission.put(c.getPageId(), resourceId);
					}
				}
			}
		}
		
		for (Iterator<String> it = layoutMap.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			JSONObject v = layoutMap.get(key);
			if (!StringUtils.isEmpty((String) v.getString("parentId"))) {
				JSONObject p = layoutMap.get((String) v.getString("parentId"));
				if (p != null) {
					if (p.getJSONArray("childLayouts") == null) {
						List<JSONObject> tmp = new ArrayList<JSONObject>();
						p.put("childLayouts", tmp);
					}
					List<Object> list = p.getJSONArray("childLayouts");
					list.add(v);
					p.put("childLayouts", list);
				}
			} else {
				mainLayouts.add(v);
			}
		}
		root.put("layouts", mainLayouts);
		if (vo.getPageType() == 1002) {	//表单页面
			root.put("components", components);
		} else {	//列表页面
			root.put("components", coms); 
		}
		
		//数据源
		List<String> dataAlias = new ArrayList<String>();
		String dataSource = StringEscapeUtils.unescapeHtml4(vo.getDataJson());
		JSONArray o = JSON.parseArray(dataSource);	
		if (o != null && o.size() > 0) {
			for (int i = 0; i < o.size(); i++) {
				JSONObject jo = (JSONObject) o.get(i);
				dataAlias.add(jo.getString("name"));
			}
		}
		
		//加载后脚本，加载前脚本，样式
		vo.setAfterLoadScript(StringEscapeUtils.unescapeHtml4(vo.getAfterLoadScript()));
		vo.setPreLoadScript(StringEscapeUtils.unescapeHtml4(vo.getPreLoadScript()));
		String styleId = vo.getStyleId();
		if(StringUtils.isNotBlank(styleId)){
			StoreVO storeVO = storeService.findById(vo.getStyleId());
			vo.setStyleId(JSON.parseObject(storeVO.getContent()).getString("script"));
		}	
		
		root.put("dataAlias", dataAlias);
		root.put("pageActs", pageActsList);
		root.put("hasRowButton", hasRowButton);
		root.put("hasSearchComponent", hasSearchComponent);
		root.put("page", vo);
		root.put("valdatorsMap", valdatorsMap);
		root.put("pageActPermission", pageActPermission);
		root.put("hiddenComs", hiddenComs);
		String templateStr = PfmTemplate.get(PfmTemplate.class, vo.getTemplateId()).getContent();
		logger.debug("--------------此处超强分割线-------------------------------------页面组件-------------------------------");
		logger.debug("要解析的页面 ：　　" + vo.getName() + "\t页面ID为" + vo.getId());
		logger.debug("页面中组件有    ：　　" + vo.getName());
		for (int i = 0; i < coms.size(); i++) {
			JSONObject com = coms.get(i);
			logger.debug("组件ID: " + com.getString("pageId") + "\t组件名称： " + com.getString("name") + "\t组件数据源： "
					+ com.getString("dataItemCode") + "\t组件布局: " + com.getString("layoutName"));
		}
		logger.debug("--------------此处超强分割线-------------------------------------页面动作-------------------------------");
		for (int i = 0; i < pageActsList.size(); i++) {
			PageAct c = pageActsList.get(i);
			if("1".equals(c.getPlace())){
				root.put("hasRowButton", true);
			}
			logger.debug("动作ID: " + c.getPageId() + "\t动作名称: " + c.getName());
			logger.debug("动作客户端脚本: " + c.getClientScript());
			logger.debug("动作服务端脚本: " + c.getServerScript());
		}
		logger.debug("--------------此处超强分割线-------------------------------------END-------------------------------");
		String disStr = FreeMarkers.renderString(templateStr, root);
		return disStr;
	}

	@Deprecated
	public String generateTemplateByPageId(JSONObject parent, DynamicPageVO vo) {
		// 根据需求 加上父组件的name 和 id
		Map<String, Object> root = new HashMap<String, Object>();
		BaseExample example = new BaseExample();
		example.createCriteria().andEqualTo("dynamicPage_id", vo.getId());
		List<Store> children = Store.selectByExample(Store.class, example);

		JSONArray array = parent.getJSONArray("configures");
		Map<String, JSONObject> configMap = new HashMap<String, JSONObject>();
		if (array != null) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject o = array.getJSONObject(i);
				configMap.put(o.getString("id"), o);
			}
		}

		List<JSONObject> mainLayouts = new ArrayList<JSONObject>();
		List<JSONObject> coms = new ArrayList<JSONObject>();
		Map<String, PageAct> pageActs = new HashMap<String, PageAct>();
		List<PageAct> pageActsList = new ArrayList<PageAct>();

		// 布局组件
		Map<String, JSONObject> map = new HashMap<String, JSONObject>();

		Map<String, List<JSONObject>> components = new HashMap<String, List<JSONObject>>();

		Map<String, Long> pageActPermission = new HashMap<String, Long>();
		// key:componentId value:对应的校验list<JSONObject>
		Map<String, List<JSONObject>> valdatorsMap = new HashMap<String, List<JSONObject>>();
		if (children != null && children.size() > 0) {
			for (int i = 0; i < children.size(); i++) {
				Store o = children.get(i);
				String name = o.getName();
				if (StringUtils.isNotBlank(parent.getString("name"))) {
					name = parent.getString("name") + "_" + name;
				}
				o.setName(name);
				if (o.getCode().indexOf(StoreService.LAYOUT_CODE) != -1) {
					// SimpleComponent c = JSON.parseObject(o.getContent(),
					// SimpleComponent.class);
					JSONObject c = JSON.parseObject(o.getContent());
					c.put("name", name);
					map.put(c.getString("pageId"), c);
				} else if (o.getCode().indexOf(StoreService.COMPONENT_CODE) != -1) {
					JSONObject c = JSONObject.parseObject(o.getContent()); // 取组件
					if (c != null) {
						c.put("name", name);
						if (configMap.containsKey(o.getId())) {
							c.put("dataItemCode", configMap.get(o.getId()).getString("dataItemCode"));
							// c.put("name", name);
						}
						// 对于包含页面组件，特殊处理，根据包含的页面，解析后得到要发布的模板string，修改
						// 数据源dataItemCode 以及 组件id，组件name
						if ("1012".equals(c.getString("componentType"))
								|| "1013".equals(c.getString("componentType"))) {
							// c为包含组件
							String relatePageId = c.getString("relatePageId");
							DynamicPageVO relaPageVO = findById(relatePageId);

							String relaPageStr = generateTemplateByPageId(c, relaPageVO);
							// JSONArray relComArray =
							// c.getJSONArray("configures");
							// if(relComArray != null && relComArray.size()>0){
							// for(int k =0; k<relComArray.size();k++){
							// //遍历更改包含页面中组件的dataItemCode，pageId，name
							// JSONObject relCom = (JSONObject)
							// relComArray.get(k);
							// String relComId = relCom.getString("pageId");
							// Store relComStore = Store.get(relComId);
							// if(relComStore != null){
							// JSONObject relComJson =
							// JSON.parseObject(relComStore.getContent());
							// String oraignDataCode =
							// relComJson.getString("dataItemCode");
							// String newDataCode =
							// relCom.getString("dataItemCode");
							// relaPageStr = relaPageStr.replace(oraignDataCode,
							// newDataCode);
							//
							// relaPageStr =
							// relaPageStr.replace(relCom.getString("pageId"),
							// c.getString("name")+"_"+relCom.getString("pageId"));
							// //包含页面的组件pageId加前缀
							// relaPageStr =
							// relaPageStr.replace(relComJson.getString("name"),
							// c.getString("name")+"_"+relComJson.getString("name"));
							// //包含页面的组件name加前缀
							// }
							// }
							// }
							// logger.debug("relaPageStr" + relaPageStr);
							String layoutAndCom = relaPageStr.substring(
									relaPageStr.indexOf("<--MYLayoutAndCom-->") + "<--MYLayoutAndCom-->".length(),
									relaPageStr.indexOf("<--MYLayoutAndCom/-->"));
							String pageJScript = relaPageStr.substring(
									relaPageStr.indexOf("<--MYpageJScript-->") + "<--MYpageJScript-->".length(),
									relaPageStr.indexOf("<--MYpageJScript/-->"));
							String pageActScript = relaPageStr.substring(
									relaPageStr.indexOf("<--MYpageActScript-->") + "<--MYpageActScript-->".length(),
									relaPageStr.indexOf("<--MYpageActScript/-->"));
							String pageValidate = relaPageStr.substring(
									relaPageStr.indexOf("<--MYvalidate-->") + "<--MYvalidate-->".length(),
									relaPageStr.indexOf("<--MYvalidate/-->"));

							c.put("layoutAndCom", layoutAndCom);
							c.put("pageJScript", pageJScript);
							c.put("pageActScript", pageActScript);
							c.put("pageValidate", pageValidate);

						}
						// 如果包含组件不能将name传递，则需要冒过包含组件后更新name
						// if(!configMap.isEmpty()) {
						// c.put("name", name);
						// }

						String layoutId = c.getString("layoutId"); // 存入components中
						if (components.get(layoutId) != null) {
							components.get(layoutId).add(c);
						} else {
							List<JSONObject> list = new ArrayList<JSONObject>();
							list.add(c);
							components.put(layoutId, list);
						}
						coms.add(c);

						// 存组件对应的校验
						String validatJson = c.getString("validatJson");
						if (StringUtils.isNotBlank(validatJson)) {
							List<JSONObject> valdators = new ArrayList<JSONObject>();
							String[] validateArray = validatJson.split(",");
							for (int k = 0; k < validateArray.length; k++) {
								Store v = Store.get(Store.class, validateArray[k]);
								if (v != null) {
									valdators.add(JSONObject.parseObject(v.getContent()));
								}
							}
							valdatorsMap.put(c.getString("pageId"), valdators);
						}
					}

				} else if (o.getCode().indexOf(StoreService.PAGEACT_CODE) != -1) {
					PageAct c = PageActBeanWorker.convertConfToAct(o.getContent());
					if (c != null) {
						pageActs.put(c.getPageId(), c);
						pageActsList.add(c);

						Long resourceId = punResourceService.getResourceIdByRelateId(c.getPageId(), "3");
						pageActPermission.put(c.getPageId(), resourceId);
					}
				}
			}
		}
		for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			JSONObject v = map.get(key);
			if (!StringUtils.isEmpty((String) v.getString("parentId"))) {
				JSONObject p = map.get((String) v.getString("parentId"));
				if (p != null) {

					if (p.getJSONArray("childLayouts") == null) {
						List<JSONObject> tmp = new ArrayList<JSONObject>();
						p.put("childLayouts", tmp);
					}
					List<Object> list = p.getJSONArray("childLayouts");
					list.add(v);
					p.put("childLayouts", list);

				}
			} else {
				mainLayouts.add(v);
			}
		}
		// if(vo.getId() == 198) {
		// logger.debug(111);
		// }
		root.put("layouts", mainLayouts);
		if (vo.getPageType() == 1002) { // 表单页面
			root.put("components", components);
			root.put("allComs", coms);
		} else {
			root.put("components", coms); // 列表页面
		}
		// 数据源
		String dataSource = StringEscapeUtils.unescapeHtml4(vo.getDataJson());

		JSONArray o = JSON.parseArray(dataSource);
		List<String> dataAlias = new ArrayList<String>();
		if (o != null && o.size() > 0) {
			for (int i = 0; i < o.size(); i++) {
				JSONObject jo = (JSONObject) o.get(i);
				dataAlias.add(jo.getString("name"));
			}
		}
		vo.setAfterLoadScript(StringEscapeUtils.unescapeHtml4(vo.getAfterLoadScript()));
		vo.setPreLoadScript(StringEscapeUtils.unescapeHtml4(vo.getPreLoadScript()));
		root.put("dataAlias", dataAlias);
		// root.put("pageActs", pageActs);
		root.put("pageActs", pageActsList);
		root.put("page", vo);
		root.put("valdatorsMap", valdatorsMap);
		root.put("pageActPermission", pageActPermission);
		String templateStr = PfmTemplate.get(PfmTemplate.class, vo.getTemplateId()).getContent();
		// logger.debug(templateStr);
		String disStr = FreeMarkers.renderString(templateStr, root);
		logger.debug("disStr : " + disStr);
		return disStr;
	}

	@Override
	public DynamicPageVO publish(String id) {
		DynamicPageVO vo = findById(id);
		return publish(vo);
	}

	/**
	 * 签出,将页面修改为checkOut状态，以及修改checkOutUser，同时修改store表中的页面元素
	 */
	@Override
	public DynamicPageVO checkOut(String id) {
		Map<String, Object> params = new HashMap<String, Object>();
		Object obj = SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER);
		if (obj instanceof PunUserBaseInfoVO) {
			params.put("checkOutUser", ((PunUserBaseInfoVO) obj).getName());
		}
		params.put("id", id);
		queryChannel.excuteMethod(DynamicPage.class, "checkOutPage", params);
		queryChannel.excuteMethod(Store.class, "checkOutStore", params);
		return null;
	}

	/**
	 * 签入,将页面修改为checkIn状态，同时修改store表中的页面元素checkIn状态
	 */
	@Override
	public DynamicPageVO checkIn(String id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		queryChannel.excuteMethod(DynamicPage.class, "checkInPage", params);
		queryChannel.excuteMethod(Store.class, "checkInStore", params);
		return null;
	}

	@Override
	public DynamicPageVO copy(String id) {
		DynamicPageVO vo = findById(id);

		vo.setName(vo.getName() + "_" + System.currentTimeMillis());
		vo.setId(null);
		vo.setWorkflowNodeInfo(null);
		saveOrUpdate(vo);

		DynamicPageVO sourceVO = new DynamicPageVO();
		sourceVO.setId(id);

		storeService.copyByDynamicPage(sourceVO, vo);
		return vo;
	}

	@Override
	public DynamicPageVO copyToSystem(String id, String systemId) {
		DynamicPageVO vo = findById(id);

		vo.setName(vo.getName() + "_" + System.currentTimeMillis());
		vo.setId(null);
		vo.setWorkflowNodeInfo(null);
		vo.setSystemId(Long.parseLong(systemId));
		saveOrUpdate(vo);

		DynamicPageVO sourceVO = new DynamicPageVO();
		sourceVO.setId(id);

		storeService.copyByDynamicPage(sourceVO, vo);
		return vo;
	}

	/**
	 * 修改内容：修改查找按钮方式，由发送多条sql改为使用in(?,..,?)发送一条sql 修改人：wuhengguan 修改日期：2015.3.26
	 */
	@Override
	public Map<String, List<StoreVO>> getSystemActs(List<String> dynamicPageIds) {
		BaseExample baseExample = new BaseExample();
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			baseExample.createCriteria().andEqualTo("SYSTEM_ID", system.getSysId());
		}
		List<DynamicPageVO> pageVos = selectPagedByExample(baseExample, 1, Integer.MAX_VALUE, null);
		Map<String, List<StoreVO>> ret = new HashMap<String, List<StoreVO>>();
		if (pageVos != null) {
			List<String> ids = new ArrayList<>();
			for (DynamicPageVO pageVo : pageVos) {
				ids.add(pageVo.getId());
			}
			BaseExample example = new BaseExample();
			Criteria criteria = example.createCriteria();
			criteria.addCriterion("code like", StoreService.PAGEACT_CODE + "%", "code");
			if (dynamicPageIds != null&&!dynamicPageIds.isEmpty()) {
				criteria.addCriterion("DYNAMICPAGE_ID in", dynamicPageIds, "DYNAMICPAGE_ID");
			}
			List<StoreVO> stores = storeService.selectPagedByExample(example, 1, Integer.MAX_VALUE, null);
			for (StoreVO store : stores) {
				String pageName = null;
				for (DynamicPageVO pageVo : pageVos) {
					if (store.getDynamicPageId().equals(pageVo.getId())) {
						pageName = pageVo.getName();
					}
				}
				if (ret.containsKey(pageName)) {
					List<StoreVO> newStore = ret.get(pageName);
					newStore.add(store);
				} else {
					List<StoreVO> newStore = new ArrayList<StoreVO>();
					newStore.add(store);
					ret.put(pageName, newStore);
				}
			}
		}

		return ret;
	}

	@Override
	public List<JSONObject> getComponentByContainer(JSONObject container) {
		List<JSONObject> retVal = new ArrayList<JSONObject>();
		String name = container.getString("name");
		String dynamicPageId = container.getString("relatePageId");

		DynamicPageVO pageVO = findById(dynamicPageId);
		// 只查询表单和普通页面
		if (pageVO.getPageType() != 1003) {
			String componentType = container.getString("componentType");
			String configures = container.getString("configures");
			if (StringUtils.isNotBlank(configures)) {
				JSONArray array = JSON.parseArray(configures);
				for (int i = 0; i < array.size(); i++) {
					JSONObject tmp = array.getJSONObject(i);
					if (tmp != null) {
						StoreVO vo = storeService.findById(tmp.getString("pageId"));
						JSONObject o = JSON.parseObject(vo.getContent());
						if (o == null) {
							logger.debug(111 + "");
						}
						o.put("dataItemCode", tmp.getString("dataItemCode"));
						String nameTmp = StringUtils.isNotBlank(name) ? name + "_" + o.getString("name")
								: o.getString("name");
						o.put("name", nameTmp);
						retVal.add(o);
					}
				}
			}

			BaseExample example = new BaseExample();
			example.createCriteria().andEqualTo("dynamicPage_id", dynamicPageId).andLike("code",
					StoreService.COMPONENT_CODE + "%");
			List<StoreVO> stores = storeService.selectPagedByExample(example, 1, Integer.MAX_VALUE, null);
			for (StoreVO vo : stores) {
				JSONObject object = JSON.parseObject(vo.getContent());
				String type = object.getString("componentType");
				if (type.equalsIgnoreCase("1012")) {

					// String nameTmp = StringUtils.isNotBlank(name) ? name +
					// "_" + object.getString("name") :
					// object.getString("name");
					// object.put("name", nameTmp);

					retVal.addAll(getComponentByContainer(object));
				} else if (type.equalsIgnoreCase("1025")) {
					JSONObject content = JSONObject.parseObject(vo.getContent());
					JSONArray tags = content.getJSONArray("tags");
					for (int i = 0; i < tags.size(); i++) {

						JSONObject tag = tags.getJSONObject(i);
						Integer relatePageId = tag.getInteger("relatePageId");
						if (relatePageId != null) {
							JSONObject jcon = new JSONObject();
							jcon.put("relatePageId", relatePageId);
							jcon.put("componentType", "");
							retVal.addAll(getComponentByContainer(jcon));
						}
					}
				} else if (type.equalsIgnoreCase("1009") || type.equalsIgnoreCase("1014")) {

				} else {
					if (!componentType.equalsIgnoreCase("1012")) {
						String nameTmp = StringUtils.isNotBlank(name) ? name + "_" + object.getString("name")
								: object.getString("name");
						object.put("name", nameTmp);
						retVal.add(object);
					}
				}
			}
		}
		return retVal;
	}

	@Override
	public List<JSONObject> getComponentByContainerWithColumn(JSONObject container) {
		List<JSONObject> retVal = new ArrayList<JSONObject>();
		String name = container.getString("name");
		String dynamicPageId = container.getString("relatePageId");

		// DynamicPageVO pageVO = findById(Long.valueOf(dynamicPageId));

		String componentType = container.getString("componentType");
		String configures = container.getString("configures");
		if (StringUtils.isNotBlank(configures)) {
			JSONArray array = JSON.parseArray(configures);
			for (int i = 0; i < array.size(); i++) {
				JSONObject tmp = array.getJSONObject(i);
				if (tmp != null) {
					StoreVO vo = storeService.findById(tmp.getString("pageId"));
					JSONObject o = JSON.parseObject(vo.getContent());
					if (o == null) {
						String parentId = container.getString("dynamicPageId");
						if (StringUtils.isNotBlank(parentId)) {
							DynamicPageVO pageVO = findById(parentId);
							logger.debug(pageVO.getName() + " 动态页面中的[ " + name + " ]组件需要重新刷新下");
						} else {
							DynamicPageVO pageVO = findById(dynamicPageId);
							logger.debug(pageVO.getName() + " 动态页面中的[ " + name + " ]组件需要重新刷新下");
						}
					}
					o.put("dataItemCode", tmp.getString("dataItemCode"));
					String nameTmp = StringUtils.isNotBlank(name) ? name + "_" + o.getString("name")
							: o.getString("name");
					o.put("name", nameTmp);
					retVal.add(o);
				}
			}
		}

		BaseExample example = new BaseExample();
		example.createCriteria().andEqualTo("dynamicPage_id", dynamicPageId).andLike("code",
				StoreService.COMPONENT_CODE + "%");
		List<StoreVO> stores = storeService.selectPagedByExample(example, 1, Integer.MAX_VALUE, null);
		for (StoreVO vo : stores) {
			JSONObject object = JSON.parseObject(vo.getContent());
			String type = object.getString("componentType");
			if (type.equalsIgnoreCase("1012") || type.equalsIgnoreCase("1013")) { // 包含组件和搜索组件，将包含页面下的其他组件也一并加上
				retVal.add(object);
				retVal.addAll(getComponentByContainerWithColumn(object));
			}
			// else if(type.equalsIgnoreCase("1009") ||
			// type.equalsIgnoreCase("1014")) {
			//
			// }
			else {
				if (!componentType.equalsIgnoreCase("1012")) {
					if (componentType.equalsIgnoreCase("1013")) { // 对于搜索组件，不加前缀，
																	// 包含组件，则需要加上前缀
						object.put("name", object.getString("name"));
						retVal.add(object);
					} else {
						String nameTmp = StringUtils.isNotBlank(name) ? name + "_" + object.getString("name")
								: object.getString("name");
						object.put("name", nameTmp);
						retVal.add(object);
					}

				}
			}
		}

		return retVal;
	}

	public List<String> getChildListPages(String dynamicPageId) {
		List<String> list = new ArrayList<>();
		DynamicPageVO pageVO = findById(dynamicPageId);
		if (pageVO.getPageType() == 1003) {// 列表页面，则返回自己
			list.add(dynamicPageId);
		} else {// 表单页面，则查询包含的列表子页面
			BaseExample example = new BaseExample();
			example.createCriteria().andEqualTo("dynamicPage_id", dynamicPageId).andLike("code",
					StoreService.COMPONENT_CODE + "%");
			List<StoreVO> stores = storeService.selectPagedByExample(example, 1, Integer.MAX_VALUE, null);

			if (stores != null && stores.size() > 0) {
				for (int i = 0; i < stores.size(); i++) {
					JSONObject json = JSON.parseObject(((StoreVO) stores.get(i)).getContent());
					if ("1012".equals(json.getString("componentType"))) {
						String relatePageId = json.getString("relatePageId");
						DynamicPageVO relaPageVO = findById(relatePageId);
						if (relaPageVO.getPageType() == 1003) {
							list.add(relaPageVO.getId());
						} else {
							list.addAll(getChildListPages(relaPageVO.getId()));
						}
					}
				}
			}
		}
		return list;
	}

	@Override
	public String getTemplateContext(String id) {
		DynamicPage page = DynamicPage.get(DynamicPage.class, id);
		if (page != null) {
			return page.getTemplateContext();
		}
		return null;
	}

	@Override
	public List<DynamicPageVO> listNameAndIdInSystem(Long systemId, Map<String, Object> params) {
		if (params == null)
			params = new HashMap<String, Object>();
		params.put("systemId", systemId);
		params.put("start", 0);
		params.put("limit", Integer.MAX_VALUE);
		List<DynamicPage> list = queryChannel.queryPagedResult(DynamicPage.class, "listNameAndIdInSystem", params);
		List<DynamicPageVO> resultVo = new ArrayList<DynamicPageVO>();
		for (DynamicPage mm : list) {
			resultVo.add(BeanUtils.getNewInstance(mm, DynamicPageVO.class));
		}
		list.clear();
		return resultVo;
	}

}
