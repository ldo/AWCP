/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_emp` AS select distinct `p_un_user_base_info`.`USER_ID_CARD_NUMBER` AS `No`,`p_un_user_base_info`.`NAME` AS `Name`,`p_un_user_base_info`.`USER_PWD` AS `Pass`,`p_un_user_group`.`GROUP_ID` AS `FK_Dept`,NULL AS `SID`,`p_un_user_base_info`.`MOBILE` AS `Tel`,`p_un_user_base_info`.`USER_EMAIL` AS `Email`,1 AS `NumOfDept`,`p_un_user_base_info`.`NUMBER` AS `Idx`,`p_un_user_base_info`.`USER_NAME` AS `PinYin`,0 AS `SignType`,`p_un_user_group`.`POSITION_ID` AS `FK_Duty`,`p_un_user_group`.`leader` AS `Leader`,NULL AS `StaDesc`,NULL AS `DeptDesc`,`p_un_user_base_info`.`USER_ID` AS `EmpNo` from (`p_un_user_base_info` left join `p_un_user_group` on((`p_un_user_base_info`.`USER_ID` = `p_un_user_group`.`USER_ID`)))$$
DELIMITER ;

DELIMITER $$
CREATE VIEW `port_dept` AS select `p_un_group`.`GROUP_ID` AS `No`,`p_un_group`.`GROUP_CH_NAME` AS `Name`,`p_un_group`.`PARENT_GROUP_ID` AS `ParentNo`,NULL AS `NameOfPath`,NULL AS `TreeNo`,NULL AS `Leader`,`p_un_group`.`CONTACT_NUMBER` AS `Tel`,NULL AS `FK_DeptType`,`p_un_group`.`NUMBER` AS `Idx`,NULL AS `IsDir` from `p_un_group`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_deptduty` AS select `port_emp`.`FK_Duty` AS `FK_Duty`,`port_emp`.`FK_Dept` AS `FK_Dept` from `port_emp`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_deptemp` AS select `p_un_user_group`.`USER_GRUOP_ID` AS `MyPK`,`p_un_user_group`.`USER_ID` AS `FK_Emp`,`p_un_user_group`.`POSITION_ID` AS `FK_Duty`,`p_un_user_group`.`GROUP_ID` AS `FK_Dept`,NULL AS `DutyLevel`,`p_un_user_group`.`leader` AS `Leader` from `p_un_user_group`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_deptempstation` AS select `p_un_user_group`.`USER_GRUOP_ID` AS `MyPK`,`p_un_user_group`.`USER_ID` AS `FK_Emp`,`p_un_user_group`.`GROUP_ID` AS `FK_Dept`,`p_un_user_group`.`POSITION_ID` AS `FK_Station` from `p_un_user_group`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_deptstation` AS select distinct `p_un_user_group`.`GROUP_ID` AS `FK_Dept`,`p_un_user_group`.`POSITION_ID` AS `FK_Station` from `p_un_user_group`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_depttype` AS select NULL AS `No`,NULL AS `Name`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_duty` AS select `p_un_position`.`POSITION_ID` AS `No`,`p_un_position`.`NAME` AS `Name` from `p_un_position`$$
DELIMITER ;


/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_empdept` AS select `port_emp`.`No` AS `FK_Emp`,`port_emp`.`FK_Dept` AS `FK_Dept` from `port_emp`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_empstation` AS select `p_un_user_group`.`USER_ID` AS `FK_Emp`,`p_un_user_group`.`POSITION_ID` AS `FK_Station` from `p_un_user_group`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_inc` AS select `p_un_group`.`GROUP_ID` AS `No`,`p_un_group`.`GROUP_CH_NAME` AS `Name`,`p_un_group`.`PARENT_GROUP_ID` AS `ParentNo` from `p_un_group`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_station` AS select `p_un_position`.`POSITION_ID` AS `No`,`p_un_position`.`NAME` AS `Name`,`p_un_position`.`GRADE` AS `FK_StationType` from `p_un_position`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `port_stationtype` AS select `p_un_position`.`GRADE` AS `Idx`,`p_un_position`.`GRADE` AS `No`,concat('级别',`p_un_position`.`GRADE`) AS `Name` from `p_un_position`$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `v_flowstarter` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_empstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`No` AS `No` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_emp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4))$$
DELIMITER ;

/*  Create View in target  */

DELIMITER $$
CREATE VIEW `wf_empworks` AS select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`IsRead` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`WFState` AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`FK_NodeText` AS `NodeName`,`b`.`FK_Dept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`SDT` AS `SDT`,`b`.`FK_Emp` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SDTOfNode` AS `SDTOfNode`,`b`.`PressTimes` AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TaskSta` AS `TaskSta`,0 AS `ListType`,`a`.`Sender` AS `Sender`,((`b`.`AtPara` + _utf8'') + `a`.`AtPara`) AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_generworkerlist` `b`) where ((`b`.`IsEnable` = 1) and (`b`.`IsPass` = 0) and (`a`.`WorkID` = `b`.`WorkID`) and (`a`.`FK_Node` = `b`.`FK_Node`)) union select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`Sta` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,2 AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`NodeName` AS `NodeName`,`b`.`CCToDept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`RDT` AS `SDT`,`b`.`CCTo` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SDTOfNode` AS `SDTOfNode`,0 AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,0 AS `TaskSta`,1 AS `ListType`,`b`.`Rec` AS `Sender`,(_utf8'@IsCC=1' + `a`.`AtPara`) AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_cclist` `b`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`b`.`Sta` <= 1))$$
DELIMITER ;